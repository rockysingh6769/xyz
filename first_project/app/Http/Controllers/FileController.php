<?php



namespace App\Http\Controllers;



use Illuminate\Http\Request;
use App\productModel;
class FileController extends Controller

{

    /**

     * Show the application dashboard.

     *

     * @return \Illuminate\Http\Response

     */

    public function create()

    {

        return view('products.create');

    }



    /**

     * Show the application dashboard.

     *

     * @return \Illuminate\Http\Response

     */

    public function store(Request $request)
    { 
       if(empty($request->file()))
       {
        $model = new productModel();
        $data['imgpath']='';
        $response=$model->updatedata('product_list','id',$request->product_id,$data);
        return redirect()->back();
       }
       else
       {
           $imageName = time().'.'.$request->file('image_upload')->
           getClientOriginalExtension();
           $request->file('image_upload')->move(
           base_path() . '/uploads/', $imageName
           );
           $model = new productModel();
           $data['imgpath']=$imageName;
           $response=$model->updatedata('product_list','id',$request->product_id,$data);
           return redirect()->back();
       } 

    }
      


}