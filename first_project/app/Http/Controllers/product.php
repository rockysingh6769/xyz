<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\productModel;

class product extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function __construct()
     {
         
     }
     public function index()
     {  
        $model = new productModel();
        $response=$model->getdata('product_list');
        if(count($response) === 0)
        {return view('products.index');}
        else
        {
           foreach ($response as $value) 
            { $values[]=$value;   
            $php_array = json_encode($values);
            $convert= json_decode($php_array,true);
            }
            $list=$convert;
            return view("products.index",compact("list"));
        }
     }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('products.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $data=[ 'product_name'=>$request->product_name,
                'discription'=>$request->discription,
                'status'    =>$request->status,
                'created_at' =>date('Y-m-d H:i:s'),
                'imgpath'    =>'' 
             ];
         if(!empty($request->image_upload))
         {
           $imageName = time().'.'.$request->file('image_upload')->
           getClientOriginalExtension();
           $request->file('image_upload')->move(
           base_path() . '/uploads/', $imageName
           );
           $model = new productModel();
           $data['imgpath']=$imageName;
         }
         $model = new productModel();
         $response=$model->insert('product_list',$data);
         return redirect('product');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model = new productModel();
        $response=$model->view('product_list','id',$id);
        foreach ($response as $value) 
        { $values[]=$value;   
        $php_array = json_encode($values);
        $convert= json_decode($php_array,true);
        }
        $list=$convert;
        return view("products.view",compact("list"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = new productModel();
        $response=$model->view('product_list','id',$id);
        foreach ($response as $value) 
        { $values[]=$value;   
        $php_array = json_encode($values);
        $convert= json_decode($php_array,true);
        }
        $list=$convert;
        return view("products.edit",compact("list"));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $data=[ 'product_name'=>$request->product_name,
                'discription'=>$request->discription,
                'status'    =>$request->status
             ];
         if(!empty($request->image_upload))
         {
           $imageName = time().'.'.$request->file('image_upload')->
           getClientOriginalExtension();
           $request->file('image_upload')->move(
           base_path() . '/uploads/', $imageName
           );
           $model = new productModel();
           $data['imgpath']=$imageName;
         }     
        $model = new productModel();
        $response=$model->updatedata('product_list','id',$id,$data);  
        return redirect('product');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         $model = new productModel();
         $response=$model->deletedata('product_list','id',$id);  
         return redirect('product');
      
    }
}
