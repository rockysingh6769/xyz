<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use DB;

class productModel extends Model
{
    public function getdata($tbl)
    {
       return DB::table($tbl)->get();
    }
    public function view($tbl,$column,$where)
    {  
      return DB::table($tbl)->where($column,'=',$where)->get();
    }
    public function updatedata($tbl,$column,$where,$data)
    {
      return DB::table($tbl)->where($column,'=',$where)->update($data);  
    }
    public function deletedata($tbl,$column,$where)
    {
      return DB::table($tbl)->where($column,'=',$where)->delete();
    } 
    public function insert($tbl,$data)
    { 
      return DB::table($tbl)->insert($data); 
    }
}
