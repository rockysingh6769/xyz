<html lang="en">

<head>
<meta name="csrf-token" content="{{ csrf_token() }}">

  <title>Laravel 5.7 Multiple File Upload Example</title>

  <script src="{{ url('/js/jquery.min.js') }}"></script>

  <link rel="stylesheet" href="{{ url('/js/bootstrap.min.css') }}">

</head>

<body>

<form action="{{url('file')}}"  method="post" id ="form0" enctype="multipart/form-data" >

        {{ csrf_field() }}
        <div class="form-group">
          <input name="fileAjax" id="fileAjax" class="form-control" type="file">test</textarea>  
        </div>

        <div class="form-group">
          <button id ="test" class="btn btn-success upload-image" type="submit" >Upload</button>
        </div>


</form>


<script type="text/javascript">
$.ajax({
     url: "{{url('file')}}",
     method:"POST",
     data: new FormData(this),
     // IF YOU WANT TO USE PROGRESS
     xhr: function() {
          var myXhr = $.ajaxSettings.xhr();
          if(myXhr.upload){
              // FUNCTIONS 
              myXhr.upload.addEventListener('progress', progress, false);
              myXhr.addEventListener("load", complete, false);
              myXhr.addEventListener("error", error, false);
              myXhr.addEventListener("abort", abort, false);
          }
          return myXhr;
      },
     dataType:'JSON',
     contentType: false,
     cache: false,
     processData: false,
     // IF SUCCESS
     success:function(data)
     {
        console.log(data);
     }
  });
                
</script>

</body>
</html>