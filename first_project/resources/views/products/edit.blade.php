<!DOCTYPE html>
<html>
<head>
<title></title>
  <link href="{{ url('/js/bootstrap.min.css') }}" rel="stylesheet"> 
  <link rel="stylesheet" type="text/css" href="{{ url('/js/style.css') }}">  
  <style type="text/css">
  	.error{ color: red !important;}
  </style>
</head>
<body>
<div class="container">
<div class="panel-heading">
   <a href="{{url('product')}}" class="btn btn-info btn-rounded float-right btn-sm m-0"><i class="mdi mdi-plus"></i> Back</a>
</div> 
<br>
  <form action="{{url('product',$list[0]['id']) }}" id="edit_product" method="POST" enctype="multipart/form-data" class="form-horizontal">
           <input type="hidden" name="_method" value="PUT">
	{{csrf_field()}}<br>
            <div class="panel panel-default">
                <input type="file" id="src"  name="image_upload" enctype="multipart/form-data">
                <div class="view_img">
                @if(!empty($list[0]['imgpath']))
               <img id="target"  src="{{url('/uploads/'.$list[0]['imgpath'])}}" height="100px" width="100px">
               @else
               <img  id="target" height="100px" width="100px" style="border-radius: 50%;">
               @endif
                </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" >Product</label>
                        <div class="col-sm-10">
                            <input type="text" name="product_name" id="product_name" class="form-control" value="{{$list[0]['product_name']}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" >Description</label>
                        <div class="col-sm-10">
                            <textarea name="discription" id="description" class="form-control">{{$list[0]['discription']}}</textarea>
                        </div>
                    </div>
                     <div class="form-group">
                        <label class="control-label col-sm-2" >status</label>
                       <div class="col-sm-10" >
                      
                         <select class="form-control" name="status" >
                            <option value="0" {{$list[0]['status'] == '0' ? 'selected' : ''}}>Active</option>
                            <option value="1" {{$list[0]['status'] == '1' ? 'selected' : ''}}>Inactive</option>
                        </select>  
                      
                          
                        </div>
                    </div>
                   
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            {{csrf_field()}}
                            <input type="submit" class="btn btn-default" value="Update" />
                        </div>
                    </div>
                </form>
            </div>

<script src="{{ url('/js/jquery.min.js') }}"></script>
<script src="{{ url('/js/validate.js') }}"></script>
<script type="text/javascript">
$(document).ready(function(){
$('#edit_product').validate(
{
    rules:
    {
        product_name:{   required:true  },
        discription: {   required:true  }
    },
});
});

function showImage(src,target) {
  var fr=new FileReader();
  // when image is loaded, set the src of the image where you want to display it
  fr.onload = function(e) { target.src = this.result; };
  src.addEventListener("change",function() {
    // fill fr with image data    
    fr.readAsDataURL(src.files[0]);
  });
}
var src = document.getElementById("src");
var target = document.getElementById("target");
showImage(src,target);


</script>

</body>
</html>