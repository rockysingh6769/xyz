<!DOCTYPE html>
<html lang="en">
<head>
<title>Products List</title>
  <link href="{{ url('/js/bootstrap.min.css') }}" rel="stylesheet"> 
</head>

<body>
 <div class="container">
  <a href="{{url('product/create')}}" class="btn btn-info btn-rounded float-right btn-sm m-0"><i class="mdi mdi-plus"></i> Add User</a>
 
<table id="myTable" class="table table-striped">
	<thead>
		<tr>
		   <th>SNo.</th>
		   <th>Images</th>
		   <th>Product&nbsp;Name</th>
       <th>Description</th>
		   <th>Status</th>
		   <th>created_at</th>
		   <th>Action</th>
		</tr>
	</thead>
    <tbody>
     @if(!empty($list))
       <?php $a = 1; ?>
       @foreach($list as $value)
       <tr>
       <td>{{$a++}}</td>
        @if(!empty($value['imgpath']))
       <td><img src="{{url('/uploads/'.$value['imgpath'])}}" height="50px" width="50px">
       @else
       <td>...</td>
       @endif
       <td>{{$value['product_name']}}</td>
       <td>{{$value['discription']}}</td>
       <td>{{$value['status'] == '0' ? 'Active' :'Inactive'}}</td>
       <td>{{date('d-M-Y', strtotime($value['created_at']))}}</td>
       <td><a href="{{url('product',$value['id'])}}">View |</a>
        <a href="{{url('product',[$value['id'],'edit'])}}">Edit |</a>
        <form method="POST" id="delete-form" action="{{url('product',$value['id'])}}">
          <input type="hidden" name="_method" value="DELETE">
          <input type="hidden" name="_token" value="{{csrf_token()}}">
          <input type="submit" onclick="return confirmation();" name="submit" value="delete">
        </form> 
       </td> 
       <tr>
       @endforeach    
       @endif

   </tbody>
</table>
</div>
  <script src="{{ url('/js/jquery.min.js') }}"></script>
<script type="text/javascript">
function confirmation()
{
  if(confirm('are you sure?'))
  {$('#de lete-form').submit();}
  else{ return false; }   
}
</script>


</body>
</html>

