<!DOCTYPE html>
<html>
<head>
<title></title>
  <link href="{{ url('/js/bootstrap.min.css') }}" rel="stylesheet"> 
  <link rel="stylesheet" type="text/css" href="{{ url('/js/style.css') }}">  
  <style type="text/css">
  	.error{ color: red !important;}
  </style>
</head>
<body>
<div class="container">
<div class="panel-heading">
   <a href="{{url('product')}}" class="btn btn-info btn-rounded float-right btn-sm m-0"><i class="mdi mdi-plus"></i> Back</a>
</div> 
<br>
  <form method="POST" action="{{url('file')}}" enctype="multipart/form-data">
  {{csrf_field()}}
<div class="files_upload"><br><br>
	<input type="hidden" name="product_id" value="{{$list[0]['id']}}">
	<div class="view_img">
	 @if(!empty($list[0]['imgpath']))
       <img   src="{{url('/uploads/'.$list[0]['imgpath'])}}" height="100px" width="100px">
       @else
       <td>{{$list[0]['imgpath']}}</td>
       @endif
	</div>
	
</div>
</form>
  <form action="{{url('product',$list[0]['id']) }}" id="edit_product" method="POST" enctype="multipart/form-data" class="form-horizontal">
           <input type="hidden" name="_method" value="PUT">
	{{csrf_field()}}<br>
            <div class="panel panel-default">
                    <div class="form-group">
                        <label class="control-label col-sm-2" >Product Name</label>
                        <div class="col-sm-10">
                            <input type="text" disabled name="product_name" id="product_name" class="form-control" value="{{$list[0]['product_name']}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" >Description</label>
                        <div class="col-sm-10">
                            <textarea disabled name="discription" id="description" class="form-control">{{$list[0]['discription']}}</textarea>
                        </div>
                    </div>
                     <div class="form-group">
                        <label class="control-label col-sm-2" >status</label>
                       <div class="col-sm-10" >
                       <select disabled class="form-control" name="status" >
                            <option value="0" {{$list[0]['status'] == '0' ? 'selected' : ''}}>Active</option>
                            <option value="1" {{$list[0]['status'] == '1' ? 'selected' : ''}}>Inactive</option>
                        </select>
                        </div>
                    </div>
                   
                    
                </form>
            </div>

</body>
</html>


             

