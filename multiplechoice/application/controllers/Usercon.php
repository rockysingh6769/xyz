<?php
class Usercon extends CI_Controller
{
		    public function __construct()
	      {
		       parent::__construct();
		       $this->load->library('session');
		       $this->load->model('usermodel/users');
		       $this->load->helper('url');
		       $this->load->library('email');
		       $this->load->library('form_validation');
		    }
        /*start Front functions */
        public function index()
        {
          $this->load->view('index');
        }    
        public function  frontregister()
		    {      
               if(!empty($this->session->userdata('id')))
               return redirect(base_url('usercon/user'));
			         if(empty($_POST))
        	     { $res['list']=$this->users->get_listbyname('country');
               $this->load->view('signup',$res); } 
			         if($_SERVER['REQUEST_METHOD']=="POST")
               {   
               	  if(empty($_POST['firstname']) || empty($_POST['lastname']) ||
               	     empty($_POST['cpassword']) || empty($_POST['mobile_no'])|| 
               	     empty($_POST['password'])  || empty($_POST['email'])    ||
               	     empty($_POST['dob']))
               	  	 {echo 'please'; die;}
	           	      else
                    { 
                      $data=['firstname'   =>$this->input->post('firstname'),
                             'lastname'    =>$this->input->post('lastname'),
                             'email'       =>$this->input->post('email'),
                             'mobile_no'   =>$this->input->post('mobile_no'),
                             'country_id'  =>$this->input->post('country_id'),
                             'dob'         =>$this->input->post('dob'),
                             'status'      =>$this->input->post('status'),
                             'password'    =>$this->input->post('password')
                              ];
                       $response=$this->users->emailexist($this->input->post('email'));
                       if($response){ echo 'already'; die; }
                       else{
                       $response=$this->users->register($data);
                       if($response){ echo 'success'; die;}}
                    } 
               }
        }
        public function frontlogin()
        {
        	    if(!empty($this->session->userdata('id')))
              return redirect(base_url('usercon/user'));
              if(empty($_POST))
        	    $this->load->view('login'); 
        	    if($_SERVER['REQUEST_METHOD']=="POST")
              { 
                  if(empty($_POST['email']) || empty($_POST['password'])) 
                	{echo 'Please'; die;}
	           	    else
                  {   
                      $data=['email'   =>$this->input->post('email'),
                             'password'=>$this->input->post('password')];
                      $response=$this->users->login($data);
                      if(!$response){  echo 'check'; die;   }
                      else{ $check=$this->users->check_user_status($data);
                      if($check < 2)
                      {$this->session->set_userdata('id',$response);
                      echo 'success'; die; }else{ echo 'admin_del'; die; }}
                  } 
              }
        }
        public function forgot()
        {
            if(!empty($_POST['email']))
            {
	            $response=$this->users->emailexist($this->input->post('email'));
	            if(!$response){ echo 'notexist'; die; }
	            else
	            {  $token =rand(10, 30);
	               $code=md5($token);
	               $response=$this->users->generatetoken($this->input->post('email'),$code);
					       if($response){ echo base_url().'usercon/resetpass/?token='.
                 $code; die; }
					    }
            } 
        }
        public function resetpass()
        {   
        	     if(empty($_POST))
        	     $this->load->view('resetform');
               if(!empty($_GET))
               {$response=$this->users->tokenexist($this->input->get('token'));
               $msg='Your Token Is Expired';            
        		   if(!$response){ $this->session->set_flashdata('message', $msg); 
        		   $this->session->flashdata('message'); 
        		   redirect(base_url('usercon/frontlogin'));}}
            	 if($_SERVER['REQUEST_METHOD']=="POST")
               {
                  if(empty($_POST['password']) || empty($_POST['cpassword'])) 
               	  {echo 'Please'; die;}
	           	    else
                  { 
                    $data=['password'=>$this->input->post('password'),
                           'token'   =>$this->input->post('token')  ];
                    $response=$this->users->updatepass($data);
                    if($response){ echo 'success'; die; }
                  } 
               }
        }
        public function front_aboutus()
        {
        	$this->load->view('aboutus');
        }
        public function front_contactus()
        {
        	$this->load->view('contactus');
        } 
        /*End of Front functions */
        /* Start User functions */ 
   	    public function userindex()
   	    {
  	       $i=0; 
           $data1=$this->users->get_listbyname_numofrow('question_list');
           $data2=$this->users->get_numofcolumn('report_list');
           $data3=$this->users->get_listbyname_numofrow('report_list');
           $data4=$this->users->get_listbyname_numofwhere('report_list','your_answer','
           correct_answer');
           foreach($data2 as  $value)
           { $i++; }
            $result1=$this->users->get_numofcolumn('report_list');
            foreach ($result1 as $val) {
              $one=$this->users->get_listbyid('priorities_list',$val['prioritie_id']);
              $score=$this->users->get_listbyname_numofwherebyid('report_list','your_answer','
              correct_answer',$val['prioritie_id']); 
              foreach ($one as  $value2) 
                { $first  =$this->users->get_listbyid('course_list', $value2['course_id']);
                  $secound=$this->users->get_listbyid('chapter_list',$value2['chapter_id']);
                  $third  =$this->users->get_listbyid('subject_list',$value2['subject_id']);
                  $total=[];
                  $total['course_name']= $first[0]['course_name'];
                  $total['chapter_name'] = $secound[0]['chapter_name'];
                  $total['subject_name']  = $third[0]['subject_name'];
                  $total['score']=$score;
                  $get[]=$total;
                }

            }
           $result=[];
           $result['total_question']=$data1;
           $result['total_test']=$i;
           $result['total_score']=$data3;
           $result['out_of_score']=$data4;
           $result['recent_list']=$get;
           $result2['list']=$result;
           $this->load->view('userview/index',$result2);
   	    }
		    public function  userupdate()
		    {   
               if($_SERVER['REQUEST_METHOD']=="POST")
               { 
                  if(empty($_POST['firstname']) || empty($_POST['lastname']) || empty($_POST['mobile_no']) ||  empty($_POST['email']))
                  	{echo 'Please'; die;}
                   else
                   {  
                      $data=['firstname'=>$this->input->post('firstname'),
                             'lastname' =>$this->input->post('lastname'),
                             'mobile_no'=>$this->input->post('mobile_no'),
                             'email'    =>$this->input->post('email'),
                             'status'   =>$this->input->post('status'),
                             'country_id'=>$this->input->post('country_id'),
                             'dob'       =>$this->input->post('dob') ];
                       if(!empty($_POST['password'])) $data['password']=$_POST['password']; 	
                       $id=$this->session->userdata('id');
                       $response=$this->users->useremailexist($this->input->post('email'),$id);
                       if($response){ echo 'already'; die; }
                       else{
                       $response=$this->users->userupdate($data,$id);
                       if($response){  echo 'success'; die; }}
                    }

               }
        }
        public function user()
		    {  

		       if(!empty($_GET))
           {$this->session->set_userdata('id',$_GET['id']);}
           $id=$this->session->userdata('id');
           $res1=$this->users->getdata($id);
           $res2=$this->users->get_listbyname('country');
           if(empty($this->session->userdata('id')))
           { return redirect(base_url('usercon/frontlogin'));} 
           $response['data']=[$res1,$res2];
           $this->load->view('userview/profile',$response);
		    }
		    public  function logout()
        {
              if(!empty($this->session->userdata('id')))
              { session_destroy(); 
              return redirect(base_url('usercon/frontlogin'));
              }else
              return redirect(base_url('usercon/frontlogin'));
        }
        public function fileupload()
        {   
          	if($_SERVER['REQUEST_METHOD']=="POST")
		  	    {   
              	if(!empty($_FILES))
    				    {
          					$config['upload_path']    = FCPATH."/assets/user/images/upload";
          					$config['allowed_types']  = 'gif|jpg|png|pdf|doc|jpeg';
          					$config['max_size']       = 100;
          					$config['max_width']      = 1024;
          					$config['max_height']     = 768;

    					     $this->load->library('upload', $config);
                   if(!$this->upload->do_upload('files'))
    					     {
                     $this->form_validation->set_error_delimiters('<p class="error">', '</p>');
                     $error = array('error' => $this->upload->display_errors());
                     echo 'not'; die;				
    					     }
    					     else
    					     {
  					          $data = array('upload_data' => $this->upload->data());
                      $file=$data['upload_data']['orig_name']; 
                      $id=$this->session->userdata('id');
                      $response=$this->users->imgupload($file,$id);
                      if($response) 
    				          $response=$this->users->selectimgpath($id);	
                      if(!empty($response))
                      $response[0]->imgpath;
                      echo 'success'; die;
    					     }   
    				    }
			        }
        }   
		    public function userlogin()
        {
            echo 'asasasas';
        }
   	    public function usertest_summary()
   	    {
   	    	$this->load->view('userview/test-summary');
   	    }
        public function userstart_test()
        {
        	$this->load->view('userview/start-test');
        }
        public function userreport()
        {   
            $result1=$this->users->get_numofcolumn('report_list');
            foreach($result1 as  $value)
            {   
                $total_question=$value['num_of_time'];
                $prioritie_id=$value['prioritie_id'];
                $result2=$this->users->numof_submitans('report_list',$prioritie_id);   
                $submit_answer=$result2[0]['submit_answer']; 
                $first=[];
                $first['total_question']=$total_question;
                $first['prioritie_id']=$prioritie_id;
                $first['submit_answer']=$submit_answer;
                $result3=$this->users->numof_suggestans('report_list',$prioritie_id);
                if(!empty($result3)){ $first['suggest_answer']=$result3[0]['suggest_answer']; }
                $store[]=$first;
            }
            $list['list']=$store;
            $this->load->view('userview/report',$list);
        }
        public function userquestion_detail()
        {
            $this->load->view('userview/question-detail');
        }
        public function userquestion()
        {
        	 if(empty($_POST))
           {  
              $result1=$this->users->get_countrylist('country');
              $result2=$this->users->get_courselist();
              $result3=$this->users->get_gradelist();
              $result4=$this->users->get_yearlist();
              $response['list']=[$result1,$result2,$result3,$result4];
              $this->load->view('userview/question',$response);
            }
        }
        public function usersidenav()
        {
         	$this->load->view('userview/sidenav');
        }
        public function userdetail()
        {
            $response['list']=$this->users->get_listbyname_id('report_list',$_GET['prioritie_id']);
            $this->load->view('userview/detail',$response);
        }  
        public function userattempt_test()
        { 
          if(empty($_POST))
          {  
              $result1=$this->users->get_countrylist('country');
              $result2=$this->users->get_courselist();
              $result3=$this->users->get_gradelist();
              $result4=$this->users->get_yearlist();
              $result5=$this->users->get_listbynamewithlimit('question_list','11');
              $response['list']=[$result1,$result2,$result3,$result4,$result5];
              $this->load->view('userview/attempt-test',$response);
          }
        }
        public function checkexist_priorities()
        {
             $data=[ 'country_id'=> $this->input->post('country_id'),
                     'course_id' => $this->input->post('course_id'),
                     'grade_id'  => $this->input->post('grade_id'),
                     'year_id'   => $this->input->post('year_id'),
                     'state_id'  => $this->input->post('state_id'),
                     'chapter_id'=> $this->input->post('chapter_id'),
                     'subject_id'=> $this->input->post('subject_id')
                    ];
                   $response=$this->users->checkexist_priorities('priorities_list',$data);
                   if(empty($response)){ echo 'not'; die; }else{ print_r($response[0]['id']); die; }
        }
        public function getlistbyname()
        {
          if(!empty($_POST['id']))
          {
             $response=$this->users->get_listbyname_id('question_list',$_POST['id']);
             header('Content-Type: application/json');
             echo json_encode($response);
             die;    
          }
        }
        public function insert_anwers()
        {
            if(!empty($_POST['question']) && !empty($_POST['answer']) )
            {   $question=$_POST['question'][0]['value'];
                $answer=$_POST['answer'][0]['value'];
                $id=$_POST['id'];
                $res=$this->users->check_question_exist('report_list',$question,$id);
                if($res)
                { if(!empty($_POST['sugg_opt']) && !empty($_POST['sugg_text']))
                  {
                    $data['suggest_option']=$_POST['sugg_opt'];
                    $data['comment']=$_POST['sugg_text'];
                  }
                  $data['your_answer']=$answer;
                  $result=$this->users->update_question_answer('report_list',$_POST['id'],$question,$data); echo 'updated'; die;  }
                else
                {  
                  $list=$this->users->get_quest_list('question_list',$question);
                  $data1=$list[0];
                  $data=['prioritie_id'=>$id,
                         'question'=> $data1['question'],
                         'option_a'=> $data1['option_a'],
                         'option_b'=> $data1['option_b'],
                         'option_c'=> $data1['option_c'],
                         'option_d'=> $data1['option_d'],
                         'correct_answer'=> $data1['correct_answer'],
                         'your_answer'=> $answer ];
                  if(!empty($_POST['sugg_opt']) && !empty($_POST['sugg_text']))
                  { $data['suggest_option']=$_POST['sugg_opt'];
                    $data['comment']=$_POST['sugg_text'];
                  }
                  $response=$this->users->insertdata('report_list',$data);  
                  print_r($response);  die;  
                }
            } 
            else
            {   
                $question=$_POST['question'][0]['value'];
                $id=$_POST['id'];
                $res=$this->users->check_question_exist('report_list',$question,$id);
                if(!$res)
                {  
                $list=$this->users->get_quest_list('question_list',$question);
                $data1=$list[0];
                $data=['prioritie_id'=>$id,
                       'question'=> $data1['question'],
                       'option_a'=> $data1['option_a'],
                       'option_b'=> $data1['option_b'],
                       'option_c'=> $data1['option_c'],
                       'option_d'=> $data1['option_d'],
                       'correct_answer'=> $data1['correct_answer']]; 
                       if(!empty($_POST['sugg_opt']) && !empty($_POST['sugg_text']))
                       { $data['suggest_option']=$_POST['sugg_opt'];
                         $data['comment']=$_POST['sugg_text']; }  
                      $response=$this->users->insertdata('report_list',$data);  
                      print_r($response);  die; }else{ echo 'already'; die;} 
            }
        } 
        public function check_anwers()
        {
            if(!empty($_POST['question']))
            {$question=$_POST['question'];
            $count=count($question); }
            if(!empty($_POST['answer']))
            $answer=$_POST['answer'];
            foreach($question as  $val)
            {
              foreach ($answer as  $value1) 
              {
                   if($val['name'] == $value1['name'])
                   {   $i=1;
                       $quest=$val['value'];
                       $ans=$value1['value'];
                       $response=$this->users->check_answer('question_list',$quest,$ans);
                       if($response == 'right')
                       {  $your_option=$ans;
                          $result='right'; 
                           $score[]=$i; }
                       else
                       {   $your_option=$ans;
                           $result='wrong'; }
                           $res=$this->users->get_quest_list('question_list',$quest);
                           $data = [];
                           $data =$res[0]; 
                           $data['your_option']=$your_option;
                           $data['attempt']='Attempt';
                           $data['result']=$result;
                           $x[]=$data;
                   }
              } 
            }
            if(!empty($score))
            {
            $score=count($score);
            $x['score']=$score;
            }else { $x['score'] = 0; } 
            $count=count($x);
            $attempt=$count-1; 
            $x['attempt']=$attempt;
            $total=10;
            if($attempt != $total)
            { $not_attempt= $total-$attempt;
              $x['not_attempt']=$not_attempt; }
            header('Content-Type: application/json');
            echo json_encode($x);
            die;
        }
        /*End Of User Functions */ 
        /*Start Admin functions*/ 
        public function adminlogin()
        {    
             if(!empty($this->session->userdata('admin_id')))
             return redirect(base_url('usercon/dashboard'));
             if(empty($_POST))
             $this->load->view('admin/login');
             if($_SERVER['REQUEST_METHOD']=="POST")
             { 
                  if(empty($_POST['email']) || empty($_POST['password'])) 
                  {echo 'Please'; die;}
                  else
                  {   
                    $data=['email'   =>$this->input->post('email'),
                           'password'=>$this->input->post('password')];
                    $response=$this->users->adminlogin($data);
                    if(!$response){  echo 'check'; die;   }
                    else{ $this->session->set_userdata('admin_id',$response);
                    echo 'success'; die; }
                  } 
              }
        }
        public  function admin_profile()
        {
         
          if(empty($_POST))
          {$response['list']=$this->users->get_listbyname('admin_login');
           $this->load->view('admin/profile',$response);}
          if(!empty($_POST['name']))
          { $data=['name'=>$this->input->post('name'),
                   'email'=>$this->input->post('email'),
                   'mobile_no'=>$this->input->post('mobile_no')];
            $response=$this->users->updatedatabyid('admin_login',$_POST['id'],$data);
            if($response)echo 'success'; die;
          }
          if(!empty($_POST['current']))
          {   $response=$this->users->checkexistby_colname('admin_login',$_POST['id'],
              'password',$_POST['current']);
              if(!$response){echo 'not';  }
              else
              {
                if($_POST['new'] == $_POST['confirm'])
                { $data=['password'=>$this->input->post('confirm')];
                  $response=$this->users->updatedatabyid('admin_login',$_POST['id'],
                  $data);
                  if($response){ echo 'success'; die; }
                }
              }
          }
        } 
        public function adminprofile_upload()
        { 
            if($_SERVER['REQUEST_METHOD']=="POST")
            {   
                if(!empty($_FILES))
                {
                    $config['upload_path']    = FCPATH."/assets/admin/images/upload";
                    $config['allowed_types']  = 'gif|jpg|png|pdf|doc|jpeg';
                    $config['max_size']       = 100;
                    $config['max_width']      = 1024;
                    $config['max_height']     = 768;

                   $this->load->library('upload', $config);
                   if(!$this->upload->do_upload('files'))
                   {
                     $this->form_validation->set_error_delimiters('<p class="error">', '</p>');
                     $error = array('error' => $this->upload->display_errors());
                     echo 'not'; die;       
                   }
                   else
                   {
                      $data = array('upload_data' => $this->upload->data());
                      $file=$data['upload_data']['orig_name']; 
                      $id=$_POST['id'];
                      $response=$this->users->adminimgupload('admin_login',$file,$id);
                      if($response) 
                      $response=$this->users->selectimgpath($id); 
                      if(!empty($response))
                      $response[0]->imgpath;
                      echo 'success'; die;
                   }   
                }
              }
        }
        public  function admin_logout()
        {
              if(!empty($this->session->userdata('admin_id')))
              { session_destroy(); 
              return redirect(base_url('usercon/adminlogin'));
              }else
              return redirect(base_url('usercon/adminlogin'));
        }
        public function dashboard()
        {
           $i=0;
           $data=$this->users->get_listbyname_numofrow('frontuser_entries'); 
           $data1=$this->users->get_listbyname_numofrow('question_list');
           $data2=$this->users->get_numofcolumn('report_list');
           foreach($data2 as  $value)
           { $i++; }
           $res1['list1']=$this->users->get_listbynameindesc('frontuser_entries'); 
           $res2=$this->users->get_listbyname('country');
           foreach ($res1['list1'] as $key => $value)
           {
              foreach ($res2 as $keys => $value2) 
              {  if($value['country_id']==$value2['id'])
                 $res1['list1'][$key]['country_id'] = $value2['country_name'];
              }
           }
           $result=[];
           $result['total_users']=$data;
           $result['total_question']=$data1;
           $result['attempt_test']=$i;
           $result['user_list']=$res1;
           $result2['list']=$result;
           $this->load->view('admin/index',$result2);
        }
        public function addfilter_country()
        { 
          $this->load->library('pagination');
          $config=[
          'base_url'   => base_url('usercon/addfilter_country'),
          'per_page'   =>7,
          'total_rows' =>$this->users->num_rows(),
          ];  
          $this->pagination->initialize($config);
          $response['list']=$this->users->getlist($config['per_page'],$this->uri->segment(3),0);
          $this->load->view('admin/addfilter-country',$response);
        }
        public function addcountry()
        { 
            if(!empty($_POST['country_name']) && !empty($_POST['typeofactive']))
            {   
                $data=['country_name'=>$_POST['country_name'],
                       'status'=>$_POST['typeofactive'] ];
                $response=$this->users->countryexist($data['country_name']);
                if($response){echo 'already'; die;}
                else{$response=$this->users->addcountryname($data);
                echo 'success'; die;} 
            }else{ echo 'please'; } 
        }
        public function add_page()
        { 
           $this->load->view('admin/listing-page');
        }
        public function edit_listing()
        {
             if(!empty($_GET))
             {
                   $id=$_GET['id'];
                   $response['data']=$this->users->edit_list($id);
                   $this->load->view('admin/edit_page',$response);
             }
             if(!empty($_POST))
             {  
                if(!empty($_POST['country_name']) && !empty($_POST['typeofactive']))
                {   $data=['country_name'=>$_POST['country_name'],
                            'status'     =>$_POST['typeofactive']];
                    $response=$this->users->editcountryexist($data['country_name'],
                    $_POST['hidden']);
                    if($response) { echo 'already'; die;}
                    else
                    $response=$this->users->updateedit($data,$_POST['hidden']);
                    if($response){ echo 'success'; die; }  
                }else{ echo 'please'; }      
             }   
        } 
        public function delete_list()
        {
              if(!empty($_GET))
              { $response=$this->users->delete_list($_GET['id']);
                if($response)
                redirect('http://localhost/multiplechoice/usercon/addfilter_country', '
                refresh');   
              }
        } 
        public function addstate()
        { 
             if(!empty($_POST['state_name']) && !empty($_POST['typeofactive']))
               {   
                    $data=['state_name'=>$_POST['state_name'],
                           'status'=>$_POST['typeofactive'],
                           'country_id'=>$_POST['country_id'] 
                           ];
                    $response=$this->users->stateexist($data['state_name'],$data['country_id']);
                    if($response){echo 'already'; die;}
                    else{$response=$this->users->addstatename($data);
                    echo 'success'; die;} 
               }else{ echo 'please'; } 
        }
        public function addfilter_state()
        {
            $response['list']=$this->users->getcountry();
            $this->load->view('admin/addfilter-state',$response);
        }
        public function editfilter_state()
        {  
             if(!empty($_GET))
             {
                   $id=$_GET['id'];
                   $result1=$this->users->edit_state($id);
                   $result2=$this->users->getcountry();
                   $response['list']=[$result1,$result2];
                   $this->load->view('admin/editfilter-state',$response);
              }
              if(!empty($_POST))
              { 
                  if(!empty($_POST['state_name']) && !empty($_POST['typeofactive']))
                  {
                    $data=['state_name'=>$_POST['state_name'],
                           'status'=>$_POST['typeofactive'],
                           'country_id'=>$_POST['country_id'] 
                          ];
                    $response=$this->users->updatestateeexist($data['state_name'],
                    $_POST['hidden'],$data['country_id']);
                    if($response){echo 'already'; die;}
                    else{$response=$this->users->updatestatename($data,$_POST['hidden']);
                    echo 'success'; die;} 
                  }else{ echo 'please'; } 
              }
        }
        public function deletestate_list()
        {    
              if(!empty($_GET))
              { $response=$this->users->deletestate_list($_GET['id']);
                if($response)
                redirect('http://localhost/multiplechoice/usercon/state_province', '
                refresh');   
              }
        }
        public function state_province()
        {
             $result1['list']=$this->users->getstatelist();
             $result2=$this->users->getcountry();
             foreach ($result1['list'] as $key => $value)
             {
                foreach ($result2 as $keys => $value2) 
                {  if($value['country_id']==$value2['id'])
                   $result1['list'][$key]['country_id'] = $value2['country_name'];
                }
             }$this->load->view('admin/state-province',$result1);
        }
        public function course()
        {
           $response['list']=$this->users->get_courselist();
           $this->load->view('admin/course',$response);
        }
        public function addfilter_course()
        {
          $this->load->view('admin/addfilter-course');
        }
        public function addcourse()
        { 
              if(!empty($_POST['course_name']) && !empty($_POST['typeofactive']))
              {   
                    $data=['course_name'=>$_POST['course_name'],
                           'status'=>$_POST['typeofactive'],
                           ];
                    $response=$this->users->courseexist($data['course_name']);
                    if($response){echo 'already'; die;}
                    else{$response=$this->users->addcoursename($data);
                    echo 'success'; die;} 
              }else{ echo 'please'; } 
        }
        public function editfilter_course()
        {
             if(!empty($_GET))
             {
                   $id=$_GET['id'];
                   $response['data']=$this->users->edit_courselist($id);
                   $this->load->view('admin/editfilter-course',$response);
             }
             if(!empty($_POST))
             {  
                if(!empty($_POST['course_name']) && !empty($_POST['typeofactive']))
                {   $data=['course_name'=>$_POST['course_name'],
                            'status'     =>$_POST['typeofactive']];
                    
                    $response=$this->users->editcourseexist($data['course_name'],
                    $_POST['hidden']);
                    if($response) { echo 'already'; die;}
                    else
                    $response=$this->users->updatecourselist($data,$_POST['hidden']);
                    if($response){ echo 'success'; die; }  
                }else{ echo 'please'; }      
             } 
        }
        public function deletecourse_list()
        {    
              if(!empty($_GET))
              { $response=$this->users->deletecourse_list($_GET['id']);
                if($response)
                redirect('http://localhost/multiplechoice/usercon/course', '
                refresh');   
              }
        } 
        public function grade_level()
        { 
           $response['list']=$this->users->get_gradelist();
           $this->load->view('admin/grade-level',$response);
        }
        public function addfilter_grade()
        {
          $this->load->view('admin/addfilter-grade');
        }
        public function addgrade()
        { 
             if(!empty($_POST['grade_name']) && !empty($_POST['typeofactive']))
             {   
                  $data=['grade_name'=>$_POST['grade_name'],
                         'status'=>$_POST['typeofactive'], ];
                  $response=$this->users->gradeexist($data['grade_name']);
                  if($response){echo 'already'; die;}
                  else{$response=$this->users->addgradename($data);
                  echo 'success'; die;} 
             }else{ echo 'please'; } 
        } 
        public function editfilter_grade()
        {
            if(!empty($_GET))
             {
                   $id=$_GET['id'];
                   $response['data']=$this->users->edit_gradelist($id);
                   $this->load->view('admin/editfilter-grade',$response);
             }
             if(!empty($_POST))
             {  
                
                if(!empty($_POST['grade_name']) && !empty($_POST['typeofactive']))
                {   $data=['grade_name'=>$_POST['grade_name'],
                            'status'     =>$_POST['typeofactive']];
                    
                    $response=$this->users->editgradeexist($data['grade_name'],
                    $_POST['hidden']);
                    if($response) { echo 'already'; die;}
                    else
                    $response=$this->users->updategradelist($data,$_POST['hidden']);
                    if($response){ echo 'success'; die; }  
                }else{ echo 'please'; }      
             } 

        }
        public function deletegrade_list()
        {    
              if(!empty($_GET))
              { $response=$this->users->deletegrade_list($_GET['id']);
                if($response)
                redirect('http://localhost/multiplechoice/usercon/grade_level', '
                refresh');   
              }
        }
        public function year()
        {  
            $response['list']=$this->users->get_yearlist();
            $this->load->view('admin/year',$response);
        }
        public function addfilter_year()
        {
              $this->load->view('admin/addfilter-year');
              if(!empty($_POST))
              {    
                   $data=['year'=>$_POST['year'],
                   'status'=>$_POST['typeofactive'],];
                   $response=$this->users->yearexist($data);
                   if($response){echo 'already'; die;}
                   else{$response=$this->users->addyear($data);
                   echo 'success'; die;} 
              }
        }
        public function editfilter_year()
        {
            if(!empty($_GET))
            {
                  $id=$_GET['id'];
                  $response['data']=$this->users->edit_yearlist($id);
                  $this->load->view('admin/editfilter-year',$response);
            }
            if(!empty($_POST))
            {  
                  $data=['year'=>$_POST['year'],
                         'status' =>$_POST['typeofactive']];
                  $response=$this->users->edityearexist($data,$_POST['hidden']);
                  if($response) { echo 'already'; die;}
                  else
                  $response=$this->users->updateyearlist($data,$_POST['hidden']);
                  if($response){ echo 'success'; die; }  
            } 
        }
        public function deleteyear_list()
        {    
              if(!empty($_GET))
              { $response=$this->users->deletemultiplebyid('year_list',$_GET['id']);
                if($response)
                redirect('http://localhost/multiplechoice/usercon/year', '
                refresh');   
              }
        }
        public function subject()
        {  
             $result1['list']=$this->users->get_subjectlist('subject_list');
             $result2=$this->users->get_courselist();
             foreach ($result1['list'] as $key => $value)
             {
                foreach ($result2 as $keys => $value2) 
                {  if($value['course_id']==$value2['id'])
                   $result1['list'][$key]['course_id'] = $value2['course_name'];
                }
             }
             $this->load->view('admin/subject',$result1); 

        }
        public function addfilter_subject()
        {
           $response['list']=$this->users->get_courselist();
           $this->load->view('admin/addfilter-subject',$response);
        }
        public function addsubject()
        { 
           if(!empty($_POST['subject_name']) && !empty($_POST['typeofactive']))
             {   
                $data=['subject_name'=>$_POST['subject_name'],
                         'status'=>$_POST['typeofactive'],
                         'course_id'=>$_POST['course_id'] 
                         ];
                  $response=$this->users->subjectexist('subject_list',$data['subject_name'],$data['course_id']);
                  if($response){echo 'already'; die;}
                  else{$response=$this->users->addsubjectname('subject_list',$data);
                  echo 'success'; die;} 
             }else{ echo 'please'; } 
        }  
        public function editfilter_subject()
        {
             if(!empty($_GET))
             {
                   $id=$_GET['id'];
                   $result1=$this->users->edit_subject('subject_list',$id);
                   $result2=$this->users->get_courselist();
                   $response['list']=[$result1,$result2];
                   $this->load->view('admin/editfilter-subject',$response);
              }
              if(!empty($_POST))
              { 
                   if(!empty($_POST['subject_name']) && !empty($_POST['typeofactive']))
                   {
                      $data=['subject_name'=>$_POST['subject_name'],
                             'status'=>$_POST['typeofactive'],
                             'course_id'=>$_POST['course_id'] 
                             ];
                      $response=$this->users->updatesubjectexist('subject_list',$data['subject_name'], $_POST['hidden'],$data['course_id']);
                      if($response){echo 'already'; die;}
                      else{$response=$this->users->updatesubjectname('subject_list',$data,$_POST['hidden']);
                      echo 'success'; die;} 
                   }else{ echo 'please'; } 
               }
        } 
        public function deletesubject_list()
        {    
              if(!empty($_GET))
              { $response=$this->users->deletesubject_list('subject_list',$_GET['id']);
                if($response)
                redirect('http://localhost/multiplechoice/usercon/subject', '
                refresh');   
              }
        }
        public function chapter()
        { 
            $response['list']=$this->users->get_chapterlist('chapter_list');
            $this->load->view('admin/chapter',$response);
        }
        public function addfilter_chapter()
        { 
           $response['list']=$this->users->get_subjectlist('subject_list');
           $this->load->view('admin/addfilter-chapter',$response);
        }
        public function addchapter()
        { 
             if(!empty($_POST['chapter_name']) && !empty($_POST['typeofactive']))
             {   
                  $data=['chapter_name'=>$_POST['chapter_name'],
                         'status'      =>$_POST['typeofactive'], 
                         'subject_id'  =>$_POST['subject_id']];
                  $response=$this->users->chapterexist('chapter_list',$data['chapter_name'],
                  $data['subject_id']);
                  if($response){echo 'already'; die;}
                  else{$response=$this->users->addchaptername('chapter_list',$data);
                  echo 'success'; die;} 
             }else{ echo 'please'; } 
        }
        public function editfilter_chapter()
        {   
            if(!empty($_GET))
             {      
                   $id=$_GET['id'];
                   $result1=$this->users->edit_chapterlist('chapter_list',$id);
                   $result2=$this->users->get_subjectlist('subject_list');
                   $response['list']=[$result1,$result2];
                   $this->load->view('admin/editfilter-chapter',$response);
             }
             if(!empty($_POST))
             {  
                if(!empty($_POST['chapter_name']) && !empty($_POST['typeofactive']))
                {   $data=['chapter_name'=>$_POST['chapter_name'],
                            'status'     =>$_POST['typeofactive'],
                            'subject_id' =>$_POST['subject_id']];
                            
                    $response=$this->users->editchapterexist('chapter_list',$data['chapter_name'],$_POST['hidden'],$data['subject_id']);
                    if($response) { echo 'already'; die;}
                    else  
                    $response=$this->users->updatechapterlist('chapter_list',$data,$_POST['hidden']);
                    if($response){ echo 'success'; die; }  
                }else{ echo 'please'; }      
             } 
        }
        public function deletechapter_list()
        {    
              if(!empty($_GET))
              { $response=$this->users->deletemultiplebyid('chapter_list',$_GET['id']);
                if($response)
                redirect('http://localhost/multiplechoice/usercon/chapter', '
                refresh');   
              }
        }
        public function questions()
        {
           //** This is for get all list **//
           if(empty($_POST))
           {  
              $result1=$this->users->get_countrylist('country');
              $result2=$this->users->get_courselist();
              $result3=$this->users->get_gradelist();
              $result4=$this->users->get_yearlist();
              $result5=$this->users->get_listbyname('question_list');
              $response['list']=[$result1,$result2,$result3,$result4,$result5];
              $this->load->view('admin/questions',$response);
           }
           //** This is for get state list by select country **//
           if( !empty($_POST) && !empty($_POST['country_id']) )
           {     
                 $response=$this->users->get_statelistbyid('state',$_POST['country_id']);
                 header('Content-Type: application/json');
                 echo json_encode($response);
                 die;
           }
           //** This is for get subjects and chapter list by select course **//
           if(!empty($_POST)  && !empty($_POST['course_id']) )
           {
                 $result1=$this->users->get_subjectlistbyid('subject_list',$_POST['course_id']);
                 $subject_id=$result1[0]['id'];
                 $result2=$this->users->get_chapterlistbyid('chapter_list',$subject_id); 
                 $response=[$result1,$result2];
                 header('Content-Type: application/json');
                 echo json_encode($response);
                 die;
            } 
        }
        public function addfilter_priorities()
        {
             //**This is for insert data by id in priorities table **//
             if(!empty($_POST['country_id']) && !empty($_POST['course_id']) 
             && !empty($_POST['grade_id'])   && !empty($_POST['year_id'])
             && !empty($_POST['chapter_id']) && !empty($_POST['subject_id'])
             && !empty($_POST['state_id'])) 
             {
                    
                    $data=['country_id' =>$this->input->post('country_id'),
                           'year_id'    =>$this->input->post('year_id'),
                           'course_id'  =>$this->input->post('course_id'),
                           'grade_id'   =>$this->input->post('grade_id'),
                           'chapter_id' =>$this->input->post('chapter_id'),
                           'subject_id' =>$this->input->post('subject_id'),
                           'state_id'   =>$this->input->post('state_id')];
                    
                    $response=$this->users->existpriorities('priorities_list',$data);       
                    if($response){echo "already"; die; }
                    else{$response=$this->users->insertdatagetid('priorities_list',$data);
                    if($response){ echo "success"; die; }}  
              }
              //**This is for insert question ans in question table **//
              if(!empty($_POST['option_a']) && !empty($_POST['option_b'])
              && !empty($_POST['option_c']) && !empty($_POST['option_d']) 
              && !empty($_POST['addquest']))
              { 
                    if(!empty($_POST['materialExampleRadios']))
                    {  
                       if(!empty($_POST['prioritie_id']))
                       {
                         $data=['option_a'  =>$this->input->post('option_a'),
                                'option_b'  =>$this->input->post('option_b'),
                                'option_c'  =>$this->input->post('option_c'),
                                'option_d'  =>$this->input->post('option_d'),
                                'question'  =>$this->input->post('addquest')];   
                         $data['correct_answer']=$_POST['materialExampleRadios'];
                         $data['prioritie_id']=$_POST['prioritie_id'];
                         $response=$this->users->existdata('question_list',$data);
                         if($response){ echo 'already'; die; }
                         else $response=$this->users->insertdata('question_list',$data);
                         if($response){ echo "success"; die; } 
                                   
                        }else{echo 'emptyprioritie'; die; }

                    }else{ echo 'notselected'; die; }  
                   
             }else{ echo "please"; die; }
        }
        public function editfilter_priorities()
        {   
             //** This is for get all form list by id **//
             if(!empty($_POST) && !empty($_POST['id']))
             {
                $res1=$this->users->get_listbyid('question_list'  ,$_POST['id']);
                $res2=$this->users->get_listbyid('priorities_list',$res1[0]['prioritie_id']);
                $res3=$this->users->get_listbyid('country'        ,$res2[0]['country_id']);
                $res4=$this->users->get_listbyid('chapter_list'   ,$res2[0]['chapter_id']);
                $res5=$this->users->get_listbyid('subject_list'   ,$res2[0]['subject_id']);
                $res6=$this->users->get_listbyid('course_list'    ,$res2[0]['course_id']);
                $res7=$this->users->get_listbyid('grade_list'     ,$res2[0]['grade_id']);
                $res8=$this->users->get_listbyid('year_list'      ,$res2[0]['year_id']);
                $res9=$this->users->get_listbyid('state'          ,$res2[0]['state_id']);
                $response=[$res1,$res3,$res4,$res5,$res6,$res7,$res8,$res9];
                header('Content-Type: application/json');
                echo json_encode($response);
                die;
             }
             //**This is for update data by id in priorities table **//
             if(!empty($_POST['country_id']) && !empty($_POST['course_id']) 
             && !empty($_POST['grade_id'])   && !empty($_POST['year_id'])
             && !empty($_POST['chapter_id']) && !empty($_POST['subject_id'])
             && !empty($_POST['state_id'])   && !empty($_POST['prioritie_id'])) 
             {  
                    $data=['country_id' =>$this->input->post('country_id'),
                           'year_id'    =>$this->input->post('year_id'),
                           'course_id'  =>$this->input->post('course_id'),
                           'grade_id'   =>$this->input->post('grade_id'),
                           'chapter_id' =>$this->input->post('chapter_id'),
                           'subject_id' =>$this->input->post('subject_id'),
                           'state_id'   =>$this->input->post('state_id')];
                    $response=$this->users->updateexistpriorities('priorities_list',
                    $_POST['prioritie_id'],$data);
                    if($response){echo "already"; die; }
                    else
                    {  $response=$this->users->updatedatabyid('priorities_list',
                       $_POST['prioritie_id'],$data);
                       if($response)
                       {   
                          if(!empty($_POST['editquest']) && !empty($_POST['editopt_a']) 
                          && !empty($_POST['editopt_b'])&& !empty($_POST['editopt_c'])
                          && !empty($_POST['editopt_d'])  ) 
                          {  
                             if(!empty($_POST['materialExampleRadios']))
                             {  
                                 $data=['option_a'  =>$this->input->post('editopt_a'),
                                          'option_b'  =>$this->input->post('editopt_b'),
                                          'option_c'  =>$this->input->post('editopt_c'),
                                          'option_d'  =>$this->input->post('editopt_d'),
                                          'question'  =>$this->input->post('editquest')];   
                                   $data['correct_answer']=$_POST['materialExampleRadios'];
                                   $response=$this->users->updatedatabyid('question_list',
                                   $_POST['question_id'],$data);
                                  if($response){ echo "success"; die; } 
                              }else{ echo 'notselected'; die; }  
                             
                            }else{ echo "please"; die; }
                          }
                        }
            }else{  echo 'prioritieempty'; die; }
        }
        public function del_QuestandPrioritie_id()
        {
            if(!empty($_POST['del_id']))
            {
              print_r($_POST['del_id']); die;
            }
            if(!empty($_POST['delete_id']))
            {
               $res=$this->users->get_listbyid('question_list',$_POST['delete_id']);
               $res1=$this->users->del_listbyid('priorities_list',$res[0]['prioritie_id']);
               if($res1) 
               $res=$this->users->del_listbyid('question_list',$_POST['delete_id']);
               if($res){ echo 'success'; die; }
            } 
        }
        public function updatestatus_byid()
        {   
              $data=['status'=>2];
              $response=$this->users->updatedatabyid($_POST['tbl'],$_POST['user_id'],$data);
              if($response)echo 'success'; die;
        }
        public function deletetbl_listbyid()
        {   
              if(!empty($_POST['del_id']))
                {  print_r($_POST['del_id']); die; }
              if(!empty($_POST['delete_id']) && !empty($_POST['tbl']) )
              { $response=$this->users->del_listbyid($_POST['tbl'],$_POST['delete_id']);
                if($response){ echo "success"; die;}
              }
        }
        public function users()
        {
            $res1['list']=$this->users->get_listbyname('frontuser_entries'); 
            $res2=$this->users->get_listbyname('country');
            foreach ($res1['list'] as $key => $value)
            {
                foreach ($res2 as $keys => $value2) 
                {  if($value['country_id']==$value2['id'])
                   $res1['list'][$key]['country_id'] = $value2['country_name'];
                }
             }
             $this->load->view('admin/users',$res1);
        }
        public function users_detail()
        { 
           $res1=$this->users->getdata($_GET['id']);
           $res2=$this->users->get_listbyname('country');
           $response['data']=[$res1,$res2];
           $this->load->view('admin/users-detail',$response);
        }
        public function adminprofile_edit()
        {  
           $this->session->set_userdata('id',$_GET['id']);
           $res1=$this->users->getdata($_GET['id']);
           $res2=$this->users->get_listbyname('country');
           $response['data']=[$res1,$res2];
           $this->load->view('userview/userprofile-view',$response);
             
        }
        public function adminuser_add()
        {
               if(empty($_POST))
               { $response['list']=$this->users->get_listbyname('country');  
                 $this->load->view('userview/adminuser_add',$response); }
               if($_SERVER['REQUEST_METHOD']=="POST")
               {   
                  
                  if(empty($_POST['firstname']) || empty($_POST['lastname']) ||
                     empty($_POST['cpassword']) || empty($_POST['mobile_no'])|| 
                     empty($_POST['password'])  || empty($_POST['email'])    ||
                     empty($_POST['dob']))
                     {echo 'please'; die;}
                    else
                    { 
                      $data=['firstname'   =>$this->input->post('firstname'),
                             'lastname'    =>$this->input->post('lastname'),
                             'email'       =>$this->input->post('email'),
                             'mobile_no'   =>$this->input->post('mobile_no'),
                             'country_id'  =>$this->input->post('country_id'),
                             'dob'         =>$this->input->post('dob'),
                             'password'    =>$this->input->post('password'),
                             'status'      =>$this->input->post('status')
                              ];
                       $response=$this->users->emailexist($this->input->post('email'));
                       if($response){ echo 'already'; die; }
                       else{ 
                       $response=$this->users->register($data);
                       if($response){ echo 'success'; die;}}
                    } 
               }
        }
        public function adminuser_edit()
        {
               if($_SERVER['REQUEST_METHOD']=="POST")
               { 
                  if(empty($_POST['firstname']) || empty($_POST['lastname']) || empty($_POST['mobile_no']) ||  empty($_POST['email']))
                    {echo 'Please'; die;}
                   else
                   {   
                      $data=['firstname'=>$this->input->post('firstname'),
                             'lastname' =>$this->input->post('lastname'),
                             'mobile_no'=>$this->input->post('mobile_no'),
                             'email'    =>$this->input->post('email'),
                             'country_id'=>$this->input->post('country_id'),
                             'status'    =>$this->input->post('status'),
                             'dob' =>$this->input->post('dob')
                              ];
                       if(!empty($_POST['password'])) $data['password']=$_POST['password'];   
                       $response=$this->users->useremailexist($this->input->post('email'),
                       $_POST['user_id']);
                       if($response){ echo 'already'; die; }
                       else{
                       $response=$this->users->userupdate($data,$_POST['user_id']);
                       if($response){  echo 'success'; die; }}
                    }
               }
        }
        public function adminuserfile_upload()
        { 
            if($_SERVER['REQUEST_METHOD']=="POST")
            {   
                if(!empty($_FILES))
                {
                    $config['upload_path']    = FCPATH."/assets/user/images/upload";
                    $config['allowed_types']  = 'gif|jpg|png|pdf|doc|jpeg';
                    $config['max_size']       = 100;
                    $config['max_width']      = 1024;
                    $config['max_height']     = 768;

                   $this->load->library('upload', $config);
                   if(!$this->upload->do_upload('files'))
                   {
                     $this->form_validation->set_error_delimiters('<p class="error">', '</p>');
                     $error = array('error' => $this->upload->display_errors());
                     echo 'not'; die;       
                   }
                   else
                   {
                      $data = array('upload_data' => $this->upload->data());
                      $file=$data['upload_data']['orig_name']; 
                      $id=$_POST['id'];
                      $response=$this->users->imgupload($file,$id);
                      if($response) 
                      $response=$this->users->selectimgpath($id); 
                      if(!empty($response))
                      $response[0]->imgpath;
                      echo 'success'; die;
                   }   
                }
              }
        }
        public function adminadduser_priorities()
        {
             //**This is for insert data by id in priorities table **//
             if(!empty($_POST['country_id']) && !empty($_POST['course_id']) 
             && !empty($_POST['grade_id'])   && !empty($_POST['year_id'])
             && !empty($_POST['chapter_id']) && !empty($_POST['subject_id'])
             && !empty($_POST['state_id'])) 
             {
                    
                    $data=['country_id' =>$this->input->post('country_id'),
                           'year_id'    =>$this->input->post('year_id'),
                           'course_id'  =>$this->input->post('course_id'),
                           'grade_id'   =>$this->input->post('grade_id'),
                           'chapter_id' =>$this->input->post('chapter_id'),
                           'subject_id' =>$this->input->post('subject_id'),
                           'state_id'   =>$this->input->post('state_id')];
                    
                    $response=$this->users->insertdatagetid('priorities_list',$data);
                    if($response){ echo "success"; die; }  
              }
              //**This is for insert question ans in question table **//
              if(!empty($_POST['option_a']) && !empty($_POST['option_b'])
              && !empty($_POST['option_c']) && !empty($_POST['option_d']) 
              && !empty($_POST['addquest']))
              {  
                    if(!empty($_POST['materialExampleRadios']))
                    {  
                       if(!empty($_POST['prioritie_id']))
                       {
                         $option_a=$_POST['option_a'];
                         $option_b=$_POST['option_b'];
                         $option_c=$_POST['option_c'];
                         $option_d=$_POST['option_d'];
                         $addquest=$_POST['addquest'];
                         $correct_answer=$_POST['materialExampleRadios'];
                         $count = count($_POST['addquest']);
                         for($i=1; $i<=$count; $i++) {
                         $data = [
                                 'option_a' => $option_a[$i], 
                                 'option_b' => $option_b[$i],
                                 'option_c' => $option_c[$i],
                                 'option_d' => $option_d[$i],
                                 'question' => $addquest[$i], 
                                 'correct_answer' => $correct_answer[$i]
                                 ];
                            $data['prioritie_id']=$_POST['prioritie_id'];
                            $response=$this->users->insertdata('question_list',$data);
                            }
                           if($response){ echo "success"; die; } 
                                   
                        }else{echo 'emptyprioritie'; die; }

                    }else{ echo 'notselected'; die; }  
                   
             }else{ echo "please"; die; }
        }
        public function reports()
        { 
          for($i=1;$i<=12;$i++)
          {   
              $active=0;$inactive=0;$del_user=0; 
              $response=$this->users->get_listorderby('frontuser_entries','created_at',$i);
              if(!empty($response))
              {   $getdata=$response; 
                  foreach ($getdata as  $value) 
                  {   if($value['status'] == 1 )
                      $active++; 
                      else if($value['status'] == 0) 
                      $inactive++;
                      else  
                      {$del_user++;  }
                      $result=[];$result['active']=$active;$result['inactive']=$inactive;
                      $result['del_user']=$del_user;
                  }
              }else
              {
                $result=[];
                $result['active']='0';
                $result['inactive']='0';
                $result['del_user']='0';
              } 
              $final[$i]=$result;
              unset($active); unset($inactive); unset($del_user);
          }
          for($i=1;$i<=12;$i++)
          {   $res=$this->users->get_listorderby('question_list','created_at',$i);
              if(!empty($res))
              {  $add=[];$add=count($res); } 
              else{  $add=0;   } 
              $addquest[$i]=$add;
          }
          for($i=1;$i<=12;$i++)
          {   $res1=$this->users->get_listorderbytbl('report_list','created_at',$i);  
              if(!empty($res1))
              { $attempt=[];$attempt= count($res1); }
              else
              { $attempt=0; }
              $series[$i]=$attempt;
          }
          $complete['list']=$final;
          $complete['addquest']=$addquest;
          $complete['attempt']=$series;
          $this->load->view('admin/reports',$complete);
        }
        public function adminprofile()
        {
          $this->load->view('admin/profile');
        }
        public function subscribe()
        {
          $this->load->view('admin/subscription');
        }
        public function multiple()
        {
           $this->load->view('multiplefile');
        }
        public function file_to_upload()
        {
            if(!empty($_FILES))
            {
                    $config['upload_path']    = FCPATH."/assets/user/images/upload";
                    $config['allowed_types']  = 'gif|jpg|png|pdf|doc|jpeg';
                    $config['max_size']       = 100;
                    $config['max_width']      = 1024;
                    $config['max_height']     = 768;
                    $this->load->library('upload', $config);
                    $this->load->library('upload');
                    $files = $_FILES;
                    $cpt = count($_FILES['files']['name']);
                    for($i=0; $i<$cpt; $i++)
                    {           
                        $_FILES['files']['name']= $files['files']['name'][$i];
                        $_FILES['files']['type']= $files['files']['type'][$i];
                        $_FILES['files']['tmp_name']= $files['files']['tmp_name'][$i];
                        $_FILES['files']['error']= $files['files']['error'][$i];
                        $_FILES['files']['size']= $files['files']['size'][$i];    
                       if(!$this->upload->do_upload('files'))
                       {
                         $this->form_validation->set_error_delimiters('<p class="error">', '</p>');
                         $error = array('error' => $this->upload->display_errors());
                         echo 'not'; die;       
                       }
                       else
                       {
                          $data = array('upload_data' => $this->upload->data());
                          $file=$data['upload_data']['orig_name']; 
                          $id='1';
                          $response=$this->users->imgupload($file,$id);
                          if($response) 
                          $response=$this->users->selectimgpath($id); 
                          if(!empty($response))
                          $response[0]->imgpath;
                          echo 'success'; die;
                       }   
                    }
                    echo $response; die;
            }   
        }
       //*End Of Admin Functions**/
       // END OF USERCON CLASS ** 
}       
?>
