<?php
class Users extends CI_Model
{    
      public $table='frontuser_entries';
      public $table2='admin_login';
      public $table3='country';
      public $table4='state';
      public $table5='course_list';
      public $table6='grade_list';
      public $table7='year_list';
      public function __construct()
      { 
         parent::__construct();
         $this->load->database();
      } 
      public function emailexist($email=[])
      {
         $query=$this->db
         ->where('email',$email)
         ->get($this->table);
         if( $query->num_rows() > 0 ){ return TRUE; } else { return FALSE; }
      }
      public function register($data=[])
      { 
         $data['created_at'] = date('Y-m-d');
         $sql=$this->db->insert($this->table,$data); 
         return $sql;
      }
      public function check_user_status($data=[])
      {  
          $query=$this->db
          ->select('status')
          ->from($this->table)
          ->where('email',$data['email'])
          ->where('password',$data['password'])
          ->get()->result_array();
          return $query[0]['status'];
      } 
      public function login($data=[])
      {  
          $query=$this->db
          ->select('id')
          ->from($this->table)
          ->where('email',$data['email'])
          ->where('password',$data['password'])
          ->get()->result_array();
          if(!empty($query) && count($query) > 0)
          { return $query[0]['id']; }else { return '0'; }
      }
      public function generatetoken($email,$token)
      {
          $query=$this->db 
          ->set('token',$token)
          ->where('email',$email)
          ->update($this->table);
           return $query;
      }
      public function tokenexist($token)
      {
         $query=$this->db
         ->where('token',$token)
         ->get($this->table);
         if( $query->num_rows() > 0 ){ return TRUE; } else { return FALSE; }
      }
      public function updatepass($data=[])
      {
          $query=$this->db 
          ->set('password',$data['password'])
          ->where('token',$data['token'])
          ->update($this->table);
          return $query;
      }
      public function getdata($id)
      {
        $query=$this->db
        ->where('id',$id)
        ->get($this->table);
        return  $query->result(); 
      }
      public function useremailexist($email,$id)
      { 
         $query=$this->db
         ->where('id !=',$id)
         ->where('email',$email)
         ->get($this->table);
         if( $query->num_rows() > 0 ){ return TRUE; } else { return FALSE; }
      }
      public function userupdate($data,$id)
      {   
          $data['created_at'] = date('Y-m-d');
          $this->db->set($data);
          $this->db->where('id',$id);
          $sql=$this->db->update($this->table,$data);
          return $sql; 
      }
      public function imgupload($file,$id)
      {
          $query=$this->db 
          ->set('imgpath',$file)
          ->where('id',$id)
          ->update($this->table);
          return $query;
      } 
      public function selectimgpath($id)
      {
        $query=$this->db
        ->select('imgpath')
        ->where('id',$id) 
        ->get($this->table)->result();
        return $query; die;
      }
      public function countryname($data=[])
      {
        return $this->db->insert($this->table3,$data); 
      }
      /*Start Admin Functions */
      public function adminlogin($data)
      {  
          $sql=$this->db
          ->select('id')
          ->from($this->table2)
          ->where('email',$data['email'])
          ->where('password',$data['password'])
          ->get()->result_array();
          if(!empty($sql) && count($sql) > 0)
          return $sql[0]['id']; 
          else
          return '0';
      }
      public function adminimgupload($tbl,$file,$id)
      {
          $query=$this->db 
          ->set('imgpath',$file)
          ->where('id',$id)
          ->update($tbl);
          return $query;
      } 
      public function addcountryname($data=[])
      { 
         $sql=$this->db->insert($this->table3,$data); 
         return $sql;
      }
      public function countryexist($country)
      {
         $query=$this->db
         ->where('country_name',$country)
         ->get($this->table3);
         if( $query->num_rows() > 0 ){ return TRUE; } else { return FALSE; }
      }
      public function getcountry()
      {
         $sql=$this->db
         ->get($this->table3);
         return $sql->result_array(); 
      }
      public function edit_list($id)
      {
         $query=$this->db
         ->select('id,country_name,status')
         ->from($this->table3)
         ->where('id',$id)
         ->get();
         return $query->result();          
      } 
      public function editcountryexist($country,$id)
      {
         $query=$this->db
         ->where('country_name',$country)
         ->where('id !=',$id)
         ->get($this->table3);
         if( $query->num_rows() > 0 ){ return TRUE; } else { return FALSE; }
      }
      public function updateedit($data,$id)
      {
           $sql=$this->db
           ->set($data)
           ->where('id',$id)
           ->update($this->table3,$data); 
           return $sql;
      }
      public function delete_list($id)
      {
         $sql=$this->db
         ->where('id',$id)
         ->delete($this->table3);
         return $sql;
      }
      public function num_rows()
      {
        $sql=$this->db->get($this->table3 );
        return $sql->num_rows();  
      }
      public function getlist($limit,$offset,$num)
      {
         $sql=$this->db 
         ->select('*')
         ->from($this->table3)
         ->where('id >',$num)
         ->limit($limit,$offset)
         ->get()->result_array();
         return $sql; 
      }
      public function getstatelist()
      {
         $sql=$this->db
         ->get($this->table4);
         return $sql->result_array(); 
      }
      public function stateexist($state_name,$country_id)
      {   
         $query=$this->db
         ->where('state_name=',$state_name)
         ->where('country_id',$country_id)
         ->get($this->table4);
         if( $query->num_rows() > 0 ){ return TRUE; } else { return FALSE; } 
      }  
      public function addstatename($data=[])
      {   
        $sql=$this->db->insert($this->table4,$data); 
         return $sql;
      }
      public function edit_state($id)
      { 
         $query=$this->db
         ->select('id,state_name,status,country_id')
         ->from($this->table4)
         ->where('id',$id)
         ->get();
         return $query->result_array(); 
      }
      public function updatestateeexist($state_name,$id,$country_id)
      {   
         $query=$this->db
         ->where('state_name',$state_name)
         ->where('country_id',$country_id)
         ->where('id !=',$id)
         ->get($this->table4);
         if( $query->num_rows() > 0 ){ return TRUE; } else { return FALSE; } 
      }
      public function updatestatename($data,$id)
      {
           $sql=$this->db
           ->set($data)
           ->where('id',$id)
           ->update($this->table4,$data); 
            return $sql;
      } 
      public function deletestate_list($id)
      {
         $sql=$this->db
         ->where('id',$id)
         ->delete($this->table4);
         return $sql;
      }   
      public function get_courselist()
      {
        $sql=$this->db
        ->get($this->table5);
        return $sql->result_array(); 
      }
      public function courseexist($course_name)
      {   
         $query=$this->db
         ->where('course_name=',$course_name)
         ->get($this->table5);
         if( $query->num_rows() > 0 ){ return TRUE; } else { return FALSE; } 
      } 
      public function addcoursename($data=[])
      {   
         $sql=$this->db->insert($this->table5,$data); 
         return $sql;
      } 
      public function edit_courselist($id)
      {
         $query=$this->db
         ->select('id,course_name,status')
         ->from($this->table5)
         ->where('id',$id)
         ->get();
         return $query->result_array();          
      }  
      public function editcourseexist($course,$id)
      {
         $query=$this->db
         ->where('course_name',$course)
         ->where('id !=',$id)
         ->get($this->table5);
         if( $query->num_rows() > 0 ){ return TRUE; } else { return FALSE; }
      }
      public function updatecourselist($data,$id)
      {
           $sql=$this->db
           ->set($data)
           ->where('id',$id)
           ->update($this->table5,$data); 
           return $sql;
      } 
      public function deletecourse_list($id)
      {
         $sql=$this->db
         ->where('id',$id)
         ->delete($this->table5);
         return $sql;
      }
      public function get_gradelist()
      {
        $sql=$this->db
        ->get($this->table6);
        return $sql->result_array(); 
      } 
      public function gradeexist($grade_name)
      {   
         $query=$this->db
         ->where('grade_name=',$grade_name)
         ->get($this->table6);
         if( $query->num_rows() > 0 ){ return TRUE; } else { return FALSE; } 
      } 
      public function addgradename($data=[])
      {   
         $sql=$this->db->insert($this->table6,$data); 
         return $sql;
      } 
      public function edit_gradelist($id)
      {
         $query=$this->db
         ->select('id,grade_name,status')
         ->from($this->table6)
         ->where('id',$id)
         ->get();
         return $query->result_array();          
      } 
      public function editgradeexist($grade,$id)
      {
         $query=$this->db
         ->where('grade_name',$grade)
         ->where('id !=',$id)
         ->get($this->table6);
         if( $query->num_rows() > 0 ){ return TRUE; } else { return FALSE; }
      }         
      public function updategradelist($data,$id)
      {
          $sql=$this->db
          ->set($data)
          ->where('id',$id)
          ->update($this->table6,$data); 
          return $sql;
      } 
      public function deletegrade_list($id)
      {
         $sql=$this->db
         ->where('id',$id)
         ->delete($this->table6);
         return $sql;
      } 
      public function get_yearlist()
      {
        $sql=$this->db
        ->get($this->table7);
        return $sql->result_array(); 
      }
      public function yearexist($data=[])
      {   
         $query=$this->db
         ->where('year=',$data['year'])
         ->where('status',$data['status'])
         ->get($this->table7);
         if( $query->num_rows() > 0 ){ return TRUE; } else { return FALSE; } 
      }
      public function addyear($data=[])
      {   
         $sql=$this->db->insert($this->table7,$data); 
         return $sql;
      }
      public function edit_yearlist($id)
      {
         $query=$this->db
         ->select('id,year,status')
         ->from($this->table7)
         ->where('id',$id)
         ->get();
         return $query->result_array();          
      } 
      public function edityearexist($data,$id)
      {
         $query=$this->db
         ->where('year',$data['year'])
         ->where('status',$data['status'])
         ->where('id !=',$id)
         ->get($this->table7);
         if( $query->num_rows() > 0 ){ return TRUE; } else { return FALSE; }
      } 
      public function updateyearlist($data,$id)
      {
           $sql=$this->db
           ->set($data)
           ->where('id',$id)
           ->update($this->table7,$data); 
           return $sql;
      }
      public function get_chapterlist($tbl)
      {
          $sql=$this->db
            ->select(" chapter_list.*,course_list.course_name,subject_list.subject_name")
            ->from('chapter_list')
            ->join('subject_list','subject_list.id=chapter_list.subject_id')
            ->join('course_list','course_list.id=subject_list.course_id')
            ->get()->result_array();
          return $sql;         
      }
      public function chapterexist($tbl,$name,$subject_id)
      {   
         $query=$this->db
         ->where('chapter_name=',$name)
         ->where('subject_id',$subject_id)
         ->get($tbl);
         if( $query->num_rows() > 0 ){ return TRUE; } else { return FALSE; } 
      }   
      public function addchaptername($tbl,$data=[])
      {   
         $sql=$this->db->insert($tbl,$data); 
         return $sql;
      } 
      public function edit_chapterlist($tbl,$id)
      {
         $query=$this->db
         ->select('id,chapter_name,status,subject_id')
         ->from($tbl)
         ->where('id',$id)
         ->get();
         return $query->result_array();          
      } 
      public function editchapterexist($tbl,$name,$id,$subject_id)
      {
         $query=$this->db
         ->where('chapter_name',$name)
         ->where('subject_id',$subject_id)
         ->where('id !=',$id)
         ->get($tbl);
         if( $query->num_rows() > 0 ){ return TRUE; } else { return FALSE; }
      }
      public function updatechapterlist($tbl,$data,$id)
      {
          $sql=$this->db
           ->set($data)
           ->where('id',$id)
           ->update($tbl,$data); 
          return $sql;
      } 
      public function deletemultiplebyid($tbl,$id)
      {
         $sql=$this->db
         ->where('id',$id)
         ->delete($tbl);
         return $sql;
      } 
      public function get_subjectlist($tbl)
      {
        $sql=$this->db->get($tbl);
        return $sql->result_array(); 
      }
      public function subjectexist($tbl,$subject_name,$course_id)
      {   
         $query=$this->db
         ->where('subject_name=',$subject_name)
         ->where('course_id',$course_id)
         ->get($tbl);
         if( $query->num_rows() > 0 ){ return TRUE; } else { return FALSE; } 
      }
      public function addsubjectname($tbl,$data=[])
      {   
         $sql=$this->db->insert($tbl,$data); 
         return $sql;
      }
      public function edit_subject($tbl,$id)
      { 
         $query=$this->db
         ->select('id,subject_name,status,course_id')
         ->from($tbl)
         ->where('id',$id)
         ->get();
         return $query->result_array(); 
      }
      public function updatesubjectexist($tbl,$subject_name,$id,$course_id)
      {  
          $query=$this->db
         ->where('subject_name',$subject_name)
         ->where('course_id',$course_id)
         ->where('id !=',$id)
         ->get($tbl);
         if( $query->num_rows() > 0 ){ return TRUE; } else { return FALSE; } 
      }
      public function updatesubjectname($tbl,$data,$id)
      {
          $sql=$this->db
           ->set($data)
           ->where('id',$id)
           ->update($tbl,$data); 
          return $sql;
      } 
      public function deletesubject_list($tbl,$id)
      {
         $sql=$this->db
         ->where('id',$id)
         ->delete($tbl);
         return $sql;
      }  
      public function get_countrylist($tbl)
      { 
        return $this->db->get($tbl)->result_array();
      }
      public function get_listbynamewithlimit($tbl,$limit)
      { 
         $sql=$this->db 
         ->select('*')
         ->from($tbl)
         ->limit($limit)
         ->get()->result_array();
         return $sql;
      }
      public function get_listbyname($tbl)
      { 
        return $this->db->get($tbl)->result_array();
      }
      public function  get_listbynameindesc($tbl)
      { 
        return $this->db->order_by("id","desc") ->get($tbl)->result_array();
      }
     
      public function get_listbyid($tbl,$id)
      { 
        return $this->db->where('id',$id)->get($tbl)->result_array();
      }
      public function get_statelistbyid($tbl,$id)
      {  
        return $this->db->select('id,state_name')->where('country_id',$id)->get($tbl)->
        result_array();
      } 
      public function get_subjectlistbyid($tbl,$id)
      { 
        return $this->db->select('id,subject_name')->where('course_id',$id)->get($tbl)->
        result_array();
      }                           
      public function get_chapterlistbyid($tbl,$id)
      {  
        return $this->db->select('id,chapter_name')->where('subject_id',$id)->get($tbl)->
        result_array();
      }
      public function existdata($tbl,$data=[])
      {   
          $sql=$this->db
           ->where('question',$data['question'])
           ->where('correct_answer',$data['correct_answer'])
           ->get($tbl)->result();
          return $sql;
      }
      public function insertdata($tbl,$data=[])
      { 
         $data['created_at'] = date('Y-m-d');
         $sql=$this->db->insert($tbl,$data); 
         return $sql; 
      }
      public function existpriorities($tbl,$data=[])
      {   
          $sql=$this->db
           ->where('country_id',$data['country_id'])
           ->where('year_id',   $data['year_id'])
           ->where('course_id', $data['course_id'])
           ->where('grade_id',  $data['grade_id'])
           ->get($tbl)->result();
          return $sql;
      }
      public function insertdatagetid($tbl,$data=[])
      {
        $this->db->insert($tbl,$data);
        $sql= $this->db->insert_id();
        print_r($sql); die;
      }
      public function updateexistpriorities($tbl,$id,$data=[])
      {   
          $sql=$this->db
           ->where('country_id',$data['country_id'])
           ->where('year_id',   $data['year_id'])
           ->where('course_id', $data['course_id'])
           ->where('grade_id',  $data['grade_id'])
           ->where('id !=', $id)
           ->get($tbl)->result();
          return $sql;
      }
      public function updatedatabyid($tbl,$id,$data=[])
      {
          $query=$this->db 
          ->where('id',$id)
          ->update($tbl,$data);
          return $query;
      }
      public function del_listbyid($tbl,$id)
      {
         $sql=$this->db
         ->where('id',$id)
         ->delete($tbl);
         return $sql;
      }
      public function checkexist_priorities($tbl,$data=[])
      {
          $sql=$this->db
           ->select('id')
           ->from($tbl)
           ->where($data)
           ->get()->result_array();
          return $sql;
      } 
      public function existtbl_name_columnid($tbl,$id)
      {
          $sql=$this->db
           ->select('*')
           ->from($tbl)
           ->where_in('prioritie_id',$id)
           ->get()->result_array();
          print_r($sql); die;
          
      }
      public function get_numofcolumn($tbl)
      {  
            $query=$this->db
             ->select('prioritie_id, count(prioritie_id) AS num_of_time')
             ->group_by('prioritie_id')
             ->order_by('prioritie_id', 'DESC')
             ->get($tbl);
            return $query->result_array();  
      }
      public function numof_submitans($tbl,$prioritie_id)
      {
            $query=$this->db
             ->select('count(prioritie_id) AS submit_answer')
             ->where('prioritie_id',$prioritie_id)
             ->where('your_answer!=',"")
             ->get($tbl)->result_array();
            return $query;
      }
      public function numof_suggestans($tbl,$prioritie_id)
      {
            $query=$this->db
             ->select('count(prioritie_id) AS suggest_answer')
             ->where('prioritie_id',$prioritie_id)
             ->where('suggest_option!=',"")
             ->get($tbl)->result_array();
            return $query;
      }
      public function get_listorderbytbl($tbl,$column,$month)
      {    
            $query=$this->db
           ->select('prioritie_id, count(prioritie_id) AS num_of_time')
           ->from($tbl)
           ->group_by('prioritie_id')
           ->order_by('prioritie_id', 'ASC')
           ->where("MONTH(created_at)=$month")
           ->get(); 
           return $query->result_array();
      }
      public function get_listorderby($tbl,$column,$month)
      {    
            $query=$this->db
           ->from($tbl)
           ->where("MONTH(created_at)=$month")
           ->get(); 
           return $query->result_array();
      }
      public function get_listbyname_id($tbl,$id)
      {
          $query=$this->db
          ->where('prioritie_id',$id)
          ->get($tbl);
          return  $query->result_array(); 
      }
      public function check_question_exist($tbl,$question,$prioritie_id)
      {
          $query=$this->db
           ->where('question',$question)
           ->where('prioritie_id',$prioritie_id)
           ->get($tbl);
          if( $query->num_rows() > 0 ){ return TRUE; } else { return FALSE; }
          
      }
      public function update_question_answer($tbl,$id,$question,$data=[])
      {
          $query=$this->db 
           ->set($data)
           ->where('prioritie_id',$id)
           ->where('question',$question)
           ->update($tbl);
          return $query;
      }
      public function check_answer($tbl,$data1,$data2)
      {
          $sql=$this->db
           ->where('question',$data1)
           ->where('correct_answer',$data2)
           ->get($tbl);
          if($sql->num_rows() > 0 )
          { return 'right'; }else{ return 'wrong'; }

      }
      public function get_quest_list($tbl,$data=[])
      {
         $sql=$this->db
         ->where('question',$data)
         ->get($tbl);
         return $sql->result_array();
      }
      public function get_listbyname_numofrow($tbl)
      {
          $sql=$this->db
         ->get($tbl);
         return $sql->num_rows();
      }
      public function get_listbyname_numofwhere($tbl,$col1,$col2)
      {
          $sql=$this->db
         ->select('*')
         ->from($tbl)
         ->where("$col1=$col2")
         ->get();
         return $sql->num_rows();
      }
      public function checkexistby_colname($tbl,$id,$col1,$col2)
      {
         
          $sql=$this->db
           ->where('id',$id)
           ->where($col1,$col2)
           ->get($tbl);
           return $sql->result(); 
          
          
      }
      public function get_listbyname_numofwherebyid($tbl,$col1,$col2,$id)
      {
          $sql=$this->db
           ->select('*')
           ->from($tbl)
           ->where('prioritie_id',$id)
           ->where("$col1=$col2")
           ->get();
          return $sql->num_rows();
      }
      /*End of Admin Functions */
      /* End of usermodel functions */ 
}
?>