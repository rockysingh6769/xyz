<!DOCTYPE html>
<html lang="en" class="no-js">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>About Us</title>
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta name="author" content="" />
    <base href="<?php echo base_url();?>">
    <link rel="shortcut icon" href="assets/front/images/favicon.ico">
    <link rel="stylesheet" href="assets/front/css/magnific-popup.css">
    <link href="assets/front/css/owl.carousel.css" rel="stylesheet">
    <link href="assets/front/css/owl.theme.css" rel="stylesheet">
    <link href="assets/front/css/owl.transitions.css" rel="stylesheet">
    <link href="assets/front/css/mobiriseicons.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="assets/front/css/materialdesignicons.min.css" />
    <link rel="stylesheet" type="text/css" href="assets/front/css/bootstrap.min.css" />
    <link href="assets/front/css/style.css" rel="stylesheet">
	<link rel="stylesheet" href="assets/front/css/font-awesome.css">
</head>

    <body>

        <!-- START NAVBAR -->
        <nav class="contactnav navbar navbar-expand-lg fixed-top custom_nav_menu sticky">
            <div class="container">
                <!-- LOGO -->
                <h3 class="navbar-brand logo text_custom">
                   Multiple Choice Online
                </h3>

                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="mdi mdi-menu"></i>
                </button>
                <div class="collapse navbar-collapse" id="navbarCollapse">
                    <ul class="navbar-nav mx-auto">
                        <li class="nav-item">
                            <a href="<?php echo base_url();?>" class="nav-link">Home</a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link">Online Tests</a>
                        </li>
                        <li class="nav-item active">
                            <a href="aboutus.html" class="nav-link">About Us</a>
                        </li>
                        <li class="nav-item">
                            <a href="contactus.html" class="nav-link">Contact Us</a>
                        </li>
                     
                        <li class="nav-item">
                            <a href="pricing.html" class="nav-link">Pricing</a>
                        </li>
                        <li class="nav-item">
                            <a href="faq.html" class="nav-link">Faq</a>
                        </li>
                <li class="nav-item">
                    <a href="referral.html" class="nav-link">Referral</a>
                </li>
             
                    </ul>
                    <a href="login.html" class="btn_custom btn btn_small text-capitalize navbar-btn">Login / Sign Up</a>
                </div>
            </div>
        </nav>
        <!-- END NAVBAR -->



	<?php $path= base_url();?>	
    <div class="contactslider slider-area">
        <div class="page-title">
            <div class="single-slider slider-height slider-height-breadcrumb d-flex align-items-center" style="background-image: url(img/bg/others_bg.jpg');">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="slider-content slider-content-breadcrumb text-center">
                                <h1 class="white-color f-700">About us</h1>
                                <nav class="text-center" aria-label="breadcrumb">
                                    <ol class="breadcrumb justify-content-center">
                                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                                        <li class="breadcrumb-item active" aria-current="page">About Us</li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>		
		
<!-- start content-->		
<section class="what-we-do sp-two">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="sec-title">
                    
                        <h4 class="font-weight-bold text-capitalize features_box_title_top">We Are Experienced</h4>
						<div class="features_border_top"></div>
                    </div>
                    <div class="text">
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit sed do ei usmod tempor incididunt.enim ad minim veniam, quis nostrud exer citation ullamco laboris nisi ut aliquip ex ea commodo con sequat duis aute irure dolor. excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est labo rum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore.</p>
                        <p>Enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit sed quia consequuntur.</p>
                    </div>
                    <div class="link-btn mb-30 mt-40"><a href="#" class="theme-btn btn-style-one">Get Started</a></div>
                </div>
                <div class="col-lg-6">
                    <div class="stacked-image-carousel">
                        <!--Slides-->
                        <div class="slides">
                            <!--Slide-->
                            <div class="slide">
                                
                                <div class="image"><img src="<?php echo $path.'assets/front/images/3.jpg';?>" alt=""></div>
                            </div>
                            <!--Slide-->
                            <div class="slide active">
                                <div class="image"><img class="imgnone" src="<?php echo $path.'assets/front/images/service-6.jpg';?>" alt=""></div>
                            </div>
                           
                        </div>
                    </div>
                        
                </div>
            </div>
        </div>
    </section>

    <!-- About Mission -->
    <section class="our-mission">
        <div class="container">
            <div class="row">
                <div class="col-lg-4">
                    <div class="mission-block">
                        <div class="inner-box">
                            <div class="count">01.</div>
                            <h4><a href="#">Learn</a></h4>
                            <div class="text">Lorem ipsum dolor sit amet consectetur adipisicing elit sed eius mod tempor incididunt ut labore et dolore magn a aliqua. enim</div>
                            <ul class="list-style-six">
                                <li>Convallis ligula ligula gravida tristique.</li>
                                <li>Lobortis massa fringilla odio.</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="mission-block">
                        <div class="inner-box">
                            <div class="count">02.</div>
                            <h4><a href="#">Play</a></h4>
                            <div class="text"><p>Lorem ipsum dolor sit amet consectetur adipisicing elit sed eius mod tempor incididunt ut labore et dolore magn a aliqua. enim</p> <p>Nisi ut aliquip ex commodo consequat. duis aute irure dolor in reprehenderit.</p></div>
                        </div>
                    </div>
                </div>
				
                <div class="col-lg-4">
                    <div class="mission-block">
                        <div class="inner-box">
                            <div class="count">03.</div>
                            <h4><a href="#">Review</a></h4>
                            <div class="text"><p>Lorem ipsum dolor sit amet consectetur adipisicing elit sed eius mod tempor incididunt ut labore et dolore magn a aliqua. enim</p> <p>Nisi ut aliquip ex commodo consequat. duis aute irure dolor in reprehenderit.</p></div>
                        </div>
                    </div>
                </div>				
				
				
            </div>
        </div>
    </section>

<section class="indexprojectdetail text-center">
<div class="container-fluid">
		  

					<div class="wrapper">
					  <div class="row">
    <div class="col-lg-3 col-sm-12 col-12">
         <img src="<?php echo $path .'assets/front/images/counter_icon1.png';?>" alt="">
      <h2 class="timer count-title count-number" data-to="599" data-speed="1500"></h2>
       <p class="count-text ">Satisfied Students</p>
    </div>

    <div class="col-lg-3 col-sm-12 col-12">
         <img src="<?php echo $path.'assets/front/images/counter_icon2.png';?>" alt="">
      <h2 class="timer count-title count-number" data-to="252" data-speed="1500"></h2>
      <p class="count-text ">Online Tests</p>
    </div>

   <div class="col-lg-3 col-sm-12 col-12">
         <img src="<?php echo $path.'assets/front/images/counter_icon3.png';?>" alt="">
      <h2 class="timer count-title count-number" data-to="32" data-speed="1500"></h2>
      <p class="count-text ">categories</p>
    </div>

    <div class="col-lg-3 col-sm-12 col-12">
         <img src="<?php $path.'assets/front/images/counter_icon4.png';?>" alt="">
      <h2 class="timer count-title count-number" data-to="957" data-speed="1500"></h2>
      <p class="count-text ">Questions</p>
    </div>
</div>
					
					</div>
					</div>

</section>


<section class="section_all bg-light" id="services">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-12">
                        <div class="section_heading  text-center">
                            <h3 class="all_section_heading">All You Need to Deliver Online Learning</h3>
                            <div><i class="mbri-magic-stick all_section_icons  text_custom font-weight-bold"></i></div>
                            <p class="text-muted pt-2 all_section_heading_details text-center mx-auto">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                        </div>
                    </div>
                </div>

                <div class="row mt-5">
                    <div class="col-lg-4">
                        <div class="services_box bg-white text-center p-4 mt-3">
                            <div class="service_icon">
                                <img class="learning-icon" src="<?php echo $path.'assets/front/images/3.png';?>">
                            </div>
                            <div class="service_content mt-4">
                                <h5 class="font-weight-bold">history</h5>
                                <p class="mt-3 text-muted mb-0">quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo </p>
                                <a href="#" class="services_read_more mt-3 d-block">
                                    <i class="mdi mdi-arrow-right text_custom"></i>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4">
                        <div class="services_box bg-white text-center p-4 mt-3">
                            <div class="service_icon">
                                <img class="learning-icon" src="<?php $path.'assets/front/images/4.png';?>">
                            </div>
                            <div class="service_content mt-4">
                                <h5 class="font-weight-bold">Science</h5>
                                <p class="mt-3 text-muted mb-0">Duis aute irure dolor in reprehenderit in voluptate velit esse cillum </p>
                                <a href="#" class="services_read_more mt-3 d-block">
                                    <i class="mdi mdi-arrow-right text_custom"></i>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4">
                        <div class="services_box bg-white text-center p-4 mt-3">
                            <div class="service_icon">
                               <img class="learning-icon" src="<?php echo $path.'assets/front/images/2.png';?>">
                            </div>
                            <div class="service_content mt-4">
                                <h5 class="font-weight-bold">Mathematics</h5>
                                <p class="mt-3 text-muted mb-0">Excepteur sint occaecat cupidatat non proident, sunt in culpa </p>
                                <a href="#" class="services_read_more mt-3 d-block">
                                    <i class="mdi mdi-arrow-right text_custom"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

              
				
				
            </div>
        </section>
    <!-- Our Team -->

<section class="what-we-do-three sp-two grey-bg">
        <div class="container">
		
						<div class="sec-title">
                    
                        <h4 class="font-weight-bold text-capitalize features_box_title_top">What We Do?</h4>
						<div class="features_border_top"></div>
                    </div>
		
            <div class="row">

                <div class="col-lg-4">

			  <div class="card aboutflaticon text-center py-3 mt-2 mb-2" width="100%">
				<img class="card-img-top text-center" src="<?php echo $path.'assets/front/images/maths2.png';?>" alt="Card image" width="100%">
				<div class="card-body">
				  <h2 class="card-title">Plus!</h2>
				  <p class="card-text">Lorem ipsum dolor sit amet conse tetur adipisicing elit sed dousmod tempor incididunt.enim ad minim veniam, quis nostrud exer citation ullamco.</p>
				  <a href="#" class="">Read More</a>
				</div>
			  </div>				
				
				
                </div>				
				
				
                <div class="col-lg-4">

  <div class="card aboutflaticon text-center py-3 mt-2 mb-2" width="100%">
    <img class="card-img-top text-center" src="<?php echo $path.'assets/front/images/maths.png';?>" alt="Card image" width="100%">
    <div class="card-body">
      <h2 class="card-title">Plus!</h2>
      <p class="card-text"> Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt</p>
      <a href="#" class="">Read More</a>
    </div>
  </div>				
				
				
                </div>
				
               <div class="col-lg-4">

  <div class="card aboutflaticon text-center py-3 mt-2 mb-2" width="400%">
    <img class="card-img-top text-center" src="<?php echo $path.'assets/front/images/maths1.png';?>" alt="Card image" width="100%">
    <div class="card-body">
      <h2 class="card-title">Plus!</h2>
      <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis</p>
      <a href="#" class="">Read More</a>
    </div>
  </div>				
				
				
                </div>				
				
				
            </div>
        </div>
    </section>
		
		
		

        <!-- Start Footer -->
        <footer class="footer_detail">
            <div class="container">
                <div class="row pt-5 pb-5">
                    <div class="col-lg-12">
                        <div class="text-center footer_about">
                            <h3 class="text_custom">Multiple Choice Online</h3>
                            <p class="mx-auto mt-3 mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation </p>
                        </div>
                        <div class="text-center subcribe-newslatter mt-5">
                            <form class="mx-auto position-relative">
                                <input type="email" placeholder="Enter Your Email" required="">
                                <button type="submit" class="btn btn_custom">Subcribe</button>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="fot_bor"></div>
                <div class="row pt-3 pb-3">
                    <div class="col-lg-12">
                        <div class=" text-center">
                            <p class="text-white mb-0">2018 &copy; Multiple Choice Online. Design by Indi IT Solutions.</p>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- End Footer -->

        <!-- Back To Top Up Arrow Start -->
        <a href="#" class="back_top_angle_up" style="display: inline;"> 
            <i class="mbri-arrow-up"> </i>
        </a>
        <!-- Back To Top Up Arrow End -->

       
        <!-- JAVASCRIPTS -->
        <script src="<?php echo $path.'assets/front/js/jquery.min.js';?>"></script>
        <script src="<?php echo $path.'assets/front/js/popper.min.js';?>"></script>
        <script src="<?php echo $path.'assets/front/js/bootstrap.min.js';?>"></script>
        <!--TESTISLIDER JS-->
        <script src="<?php echo $path.'assets/front/js/owl.carousel.min.js';?>"></script>
        <script src="<?php echo $path.'assets/front/js/masonry.pkgd.min.js';?>"></script>
        <script src="<?php echo $path.'assets/front/js/jquery.magnific-popup.min.js';?>"></script>
        <script src="<?php echo $path.'assets/front/js/jquery.easing.min.js';?>"></script>
        <script src="<?php echo $path.'assets/front/js/custom.js';?>"></script>

		
		
		
		
	<script>
	
	(function ($) {
	$.fn.countTo = function (options) {
		options = options || {};
		
		return $(this).each(function () {
			// set options for current element
			var settings = $.extend({}, $.fn.countTo.defaults, {
				from:            $(this).data('from'),
				to:              $(this).data('to'),
				speed:           $(this).data('speed'),
				refreshInterval: $(this).data('refresh-interval'),
				decimals:        $(this).data('decimals')
			}, options);
			
			// how many times to update the value, and how much to increment the value on each update
			var loops = Math.ceil(settings.speed / settings.refreshInterval),
				increment = (settings.to - settings.from) / loops;
			
			// references & variables that will change with each update
			var self = this,
				$self = $(this),
				loopCount = 0,
				value = settings.from,
				data = $self.data('countTo') || {};
			
			$self.data('countTo', data);
			
			// if an existing interval can be found, clear it first
			if (data.interval) {
				clearInterval(data.interval);
			}
			data.interval = setInterval(updateTimer, settings.refreshInterval);
			
			// initialize the element with the starting value
			render(value);
			
			function updateTimer() {
				value += increment;
				loopCount++;
				
				render(value);
				
				if (typeof(settings.onUpdate) == 'function') {
					settings.onUpdate.call(self, value);
				}
				
				if (loopCount >= loops) {
					// remove the interval
					$self.removeData('countTo');
					clearInterval(data.interval);
					value = settings.to;
					
					if (typeof(settings.onComplete) == 'function') {
						settings.onComplete.call(self, value);
					}
				}
			}
			
			function render(value) {
				var formattedValue = settings.formatter.call(self, value, settings);
				$self.html(formattedValue);
			}
		});
	};
	
	$.fn.countTo.defaults = {
		from: 0,               // the number the element should start at
		to: 0,                 // the number the element should end at
		speed: 1000,           // how long it should take to count between the target numbers
		refreshInterval: 100,  // how often the element should be updated
		decimals: 0,           // the number of decimal places to show
		formatter: formatter,  // handler for formatting the value before rendering
		onUpdate: null,        // callback method for every time the element is updated
		onComplete: null       // callback method for when the element finishes updating
	};
	
	function formatter(value, settings) {
		return value.toFixed(settings.decimals);
	}
}(jQuery));

jQuery(function ($) {
  // custom formatting example
  $('.count-number').data('countToOptions', {
	formatter: function (value, options) {
	  return value.toFixed(options.decimals).replace(/\B(?=(?:\d{3})+(?!\d))/g, ',');
	}
  });
  
  // start all the timers
  $('.timer').each(count);  
  
  function count(options) {
	var $this = $(this);
	options = $.extend({}, options || {}, $this.data('countToOptions') || {});
	$this.countTo(options);
  }
});
	
	</script>		
		<script>
	$('#owl-carousel3').owlCarousel({
    loop:true,
    margin:10,
	autoplay:true,
    nav:false,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:1
        }
    }
})
	</script>		
		
		
    </body>
</html>