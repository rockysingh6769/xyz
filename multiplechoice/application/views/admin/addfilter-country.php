<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="#">
    <title>Multiple Choice Online</title>
    <base href="<?php echo base_url();?>">
    <link href="assets/admin/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/admin/css/perfect-scrollbar.css" rel="stylesheet">
    <link rel="stylesheet" href="assets/admin/css/font-awesome.css"> 
    <link href="assets/admin/assets/plugins/chartist-js/dist/chartist.min.css" rel="stylesheet">
    <link href="assets/admin/assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css" rel="stylesheet">
    <link href="assets/admin/assets/plugins/c3-master/c3.min.css" rel="stylesheet">
    <link href="assets/admin/css/style.css" rel="stylesheet">
    <link href="assets/admin/css/dashboard1.css" rel="stylesheet">
    <link href="assets/admin/css/default-dark.css" id="theme" rel="stylesheet">

    <!-- datatable css -->
    <link rel="stylesheet" type="text/css" href="assets/admin/assets/plugins/datatables/media/css/dataTables.bootstrap4.css"> 

<style type="text/css">
.alert-warning,.alert-danger,.alert-success
{ 
    display: none;
    position: fixed;
    max-width: 50%; 
    width: 100%; 
    margin: auto;   
    top: 45%;   left: 16%;    
    z-index: 111111;     
    box-shadow: 1px 1px 8px rgba(0, 0, 0, 0.15);     
    text-align: center;     
    right: 0;    
} 
#addbtn1
{
margin-left: 750px;
}

center a {
    margin: 13px !important;
    border: 1px solid grey;
    padding: 10px;
    background-color: yellowgreen;
    border-radius: 50%;
}
</style>

</head>

<body class="fix-header fix-sidebar card-no-border">



<div class="alert alert-warning">
<strong><center class="one"></center></strong>  
</div>

<div class="alert alert-success">
<strong><center class="two"></center></strong> 
</div>

<div class="alert alert-danger">
<strong><center class="three"></center></strong> 
</div>


    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label">Multiple Choice Online</p>
        </div>
    </div>

    <div id="main-wrapper">

        <?php include 'header.php'; ?>


        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <?php include 'sidebar.php' ; ?>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            
            <div class="container-fluid ">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
 
                    <div class="col-md-12 ">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Add Country</a></li>
                            
                        </ol>
                        <a href="usercon/add_page" id="addbtn1" class="btn btn-info btn-rounded float-right btn-sm m-0"><i class="mdi mdi-plus">
                        </i> Add Country</a>
                    
                    </div>
                    <!-- <div class=""> -->
                        <!-- <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button> -->
                    <!-- </div> -->
                </div>


                <!-- ============================================================== -->
                <!-- Projects of the month -->
                <!-- ============================================================== -->
                 
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body userdatabtn-color">
                                

                                  <table class="table table-dark">
                                       <tr>
                                         <th>Name</th>
                                         <th>Status</th>
                                         <th>Action</th>
                                        </tr>

                                          <?php foreach ($list as $item): ?>
                                        <tr class="mytbl">
                                          <td class="country_name"><?php echo $item['country_name']?></td>
                                          <td class="status"><?php echo $item['status']?></td>
                                          <td class="action"><a href="<?php echo base_url('usercon/edit_listing');?>?id=<?php echo $item['id'];?>"><i> edit |</i></a><a href="<?php echo base_url('usercon/delete_list');?>?id=<?php echo $item['id'];?>" onclick="return confirm('Are you sure want to delete')"><i>  delete </i></a></td>
                                        </tr>

                                        <?php endforeach; ?>
                 
                                 </table>



                            </div>
                        </div>
                        
                    </div>
                    
                    
                </div>
                 
<center>
<?= $this->pagination->create_links();?>
</center> 
            </div>     
               
            <!-- ============================================================== -->
            <!-- End Container fluid  -->

            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <?php include 'footer.php'; ?>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script>
    $(function() {
        $('#myTable').DataTable();
        $(document).ready(function() {
            var table = $('#example').DataTable({
                "columnDefs": [{
                    "visible": false,
                    "targets": 2
                }],
                "order": [
                    [2, 'asc']
                ],
                "displayLength": 25,
                "drawCallback": function(settings) {
                    var api = this.api();
                    var rows = api.rows({
                        page: 'current'
                    }).nodes();
                    var last = null;
                    api.column(2, {
                        page: 'current'
                    }).data().each(function(group, i) {
                        if (last !== group) {
                            $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
                            last = group;
                        }
                    });
                }
            });
            // Order by the grouping
            $('#example tbody').on('click', 'tr.group', function() {
                var currentOrder = table.order()[0];
                if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                    table.order([2, 'desc']).draw();
                } else {
                    table.order([2, 'asc']).draw();
                }
            });
        });
    });
    $('#example23').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });
    $('.buttons-copy, .buttons-csv, .buttons-print, .buttons-pdf, .buttons-excel').addClass('btn btn-primary mr-1');
    </script>




</body>
</html>

