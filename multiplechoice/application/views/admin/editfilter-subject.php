<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="#">
    <title>Multiple Choice Online</title>
    <base href="<?php echo base_url();?>">
    <link href="assets/admin/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/admin/css/perfect-scrollbar.css" rel="stylesheet">
    <link rel="stylesheet" href="assets/admin/css/font-awesome.css"> 
    <link href="assets/admin/assets/plugins/chartist-js/dist/chartist.min.css" rel="stylesheet">
    <link href="assets/admin/assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css" rel="stylesheet">
    <link href="assets/admin/assets/plugins/c3-master/c3.min.css" rel="stylesheet">
    <link href="assets/admin/css/style.css" rel="stylesheet">
    <link href="assets/admin/css/dashboard1.css" rel="stylesheet">
    <link href="assets/admin/css/default-dark.css" id="theme" rel="stylesheet">

    <!-- datatable css -->
    <link rel="stylesheet" type="text/css" href="assets/admin/assets/plugins/datatables/media/css/dataTables.bootstrap4.css"> 
<?php include 'style.php'; ?>
</head>

<body class="fix-header fix-sidebar card-no-border">


<div class="alert alert-warning">
<strong><center class="one"></center></strong>  
</div>

<div class="alert alert-success">
<strong><center class="two"></center></strong> 
</div>

<div class="alert alert-danger">
<strong><center class="three"></center></strong> 
</div>


    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
             <p class="loader__label">Multiple Choice Online</p>
        </div>
    </div>

    <div id="main-wrapper">

        <?php include 'header.php'; ?>


        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <?php include 'sidebar.php' ; ?>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            
            <div class="container-fluid ">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
 
                    <div class="col-md-12 ">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item active"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item">Add Subject</li>

                        </ol>
                    </div>
                    <!-- <div class=""> -->
                        <!-- <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button> -->
                    <!-- </div> -->
                </div>


                <!-- ============================================================== -->
                <!-- Projects of the month -->
                <!-- ============================================================== -->
               
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body userdatabtn-color">
                                 <form  id="myform" method="post" class="mt-4">
                                    <div class="row">
                                        <div class="col-md-6 offset-3">
                                         <div class="form-group mb-0">
                                                <label for="Property Select" >Select Course</label>&nbsp;  
                                                <?php  ?> 
                                                <select class="form-control" id="select_data" name="course_id" > 
                                                <?php 
                                                     echo "<pre>"; 
                                                     $course_id= $list[0][0]['course_id']; 
                                                     $listcount=count($list[1]);
                                                     $listreal= $listcount-1; 
                                                     for($i=0;$i<=$listreal;$i++)
                                                     {
                                                         if($list[1][$i]['id']==$course_id)
                                                         {
                                                          $course_name=($list[1][$i]['course_name']);
                                                         }
                                                    }
                                                 ?>

                                                <option value="<?php echo $course_id; ?>" ><?php echo $course_name; ?></option>
                                                <?php foreach($list[1] as $key => $value){ ?>
                                                <option  value="<?php print_r($value['id']);?>"><?php echo $value['course_name']; ?></option> 
                                                <?php } ?>
                                                </select>
                                            </div>
                                            
                                            <div class="form-group">
                                          
                                        <label>Subject Name</label>  
                                        <input type="text" name="subject_name"
                                        value="<?php echo $list[0][0]['subject_name']; ?>" class="form-control form-control-lg">
                                        <input type="hidden" name="hidden" value="<?php echo
                                         $list[0][0]['id']; ?>">
                                         <br><br>        
                                        <?php  if($list[0][0]['status']=='Active')
                                            echo "<select class='form-control'  
                                                   name='typeofactive'>
                                                   <option>Active</option>
                                                   <option>Inactive</option>
                                                  </select>";
                                            else   
                                            echo "<select class='form-control'  
                                                   name='typeofactive'>
                                                   <option>Inactive</option>
                                                   <option>Active</option>
                                                  </select>";
                                            ?> 
                                        <br><br>                                         
                                         <input type="submit" value="Edit"  id="editbtn" class="btn btn-info ">
                                            </div>

                                            
                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>
                        
                    </div>
                    
                    
                </div>

            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->

            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <?php include 'footer.php'; ?>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script>
    $(function() {
        $('#myTable').DataTable();
        $(document).ready(function() {
            var table = $('#example').DataTable({
                "columnDefs": [{
                    "visible": false,
                    "targets": 2
                }],
                "order": [
                    [2, 'asc']
                ],
                "displayLength": 25,
                "drawCallback": function(settings) {
                    var api = this.api();
                    var rows = api.rows({
                        page: 'current'
                    }).nodes();
                    var last = null;
                    api.column(2, {
                        page: 'current'
                    }).data().each(function(group, i) {
                        if (last !== group) {
                            $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
                            last = group;
                        }
                    });
                }
            });
            // Order by the grouping
            $('#example tbody').on('click', 'tr.group', function() {
                var currentOrder = table.order()[0];
                if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                    table.order([2, 'desc']).draw();
                } else {
                    table.order([2, 'asc']).draw();
                }
            });
        });
    });
    $('#example23').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });
    $('.buttons-copy, .buttons-csv, .buttons-print, .buttons-pdf, .buttons-excel').addClass('btn btn-primary mr-1');
    </script>   
</body>
</html>

<script type="text/javascript">
$('#editbtn').click(function()
{
    $("#myform").validate(
    {   rules:
        {   subject_name:{ required: true }
        },
        submitHandler: function(form) 
        { 
           var data = $("#myform").serialize();
           var url='<?php echo base_url();?>index.php/usercon/editfilter_subject'; 
           $.ajax(
           {   
                url: url,
                data:data,
                type:'post',
                success:function(done)
                {     
                        console.log(done);
                         if(done.trim()=='already')
                         {  $('.one').text('this subject already exist ');
                            $('.alert-warning').show();
                            setTimeout(function(){
                            $('.alert-warning').hide();  }, 2000); 
                         }
                         
                         if(done.trim()=='success')
                         { $('.two').text('successfully Edit ');
                           $('.alert-success').show();
                           setTimeout(function(){
                           window.location= "<?php  echo base_url('index.php/usercon/subject'); ?>";  },  1000); 
                         }

                         if(done.trim()=='please')
                         { $('.three').text('Please Fill Your Empty Filed');
                           $('.alert-danger').show();
                           setTimeout(function(){
                           $('.alert-danger').hide(); }, 2000); 
                            
                         }
                }
               
           });
        } 
    });      
});
</script>