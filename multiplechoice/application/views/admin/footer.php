<script src="assets/admin/assets/plugins/jquery/jquery.min.js"></script>
<script src="assets/admin/assets/plugins/bootstrap/js/popper.min.js"></script>
<script src="assets/admin/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="assets/admin/js/perfect-scrollbar.jquery.min.js"></script>
<script src="assets/admin/js/waves.js"></script>
<script src="assets/admin/js/validation.js"></script>
<script src="assets/admin/js/validate.js"></script>
<script src="assets/admin/js/sidebarmenu.js"></script>
<script src="assets/admin/assets/plugins/sticky-kit-master/dist/sticky-kit.min.js"></script>
<script src="assets/admin/assets/plugins/sparkline/jquery.sparkline.min.js"></script>
<script src="assets/admin/js/custom.min.js"></script>
<script src="assets/admin/assets/plugins/styleswitcher/jQuery.style.switcher.js"></script>
<script src="assets/admin/assets/plugins/datatables/datatables.min.js"></script>
<script src="assets/admin/assets/plugins/chartist-js/dist/chartist.min.js"></script>
<script src="assets/admin/assets/plugins/d3/d3.min.js"></script>
<script src="assets/admin/assets/plugins/c3-master/c3.min.js"></script>
<script src="assets/admin/assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.min.js"></script>
    

<footer class="footer">© 2018 Multiple Choice Online. Design by Indi IT Solutions.</footer>