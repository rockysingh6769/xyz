<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="#">
    <base href="<?php echo base_url();?>">
    <title>Multiple Choice Online</title>
    <link href="assets/admin/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/admin/css/perfect-scrollbar.css" rel="stylesheet">
	<link rel="stylesheet" href="assets/admin/css/font-awesome.css">	
    <link href="assets/admin/assets/plugins/chartist-js/dist/chartist.min.css" rel="stylesheet">
    <link href="assets/admin/assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css" rel="stylesheet">
    <link href="assets/admin/assets/plugins/c3-master/c3.min.css" rel="stylesheet">
    <link href="assets/admin/css/style.css" rel="stylesheet">
    <link href="assets/admin/css/dashboard1.css" rel="stylesheet">
    <link href="assets/admin/css/default-dark.css" id="theme" rel="stylesheet">

</head>

<body class="fix-header fix-sidebar card-no-border">

    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
             <p class="loader__label">Multiple Choice Online</p>
        </div>
    </div>

    <div id="main-wrapper">

        <?php include 'header.php'; ?>

        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->

        <?php include 'sidebar.php' ; ?>

        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid ">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
 
                    <div class="col-md-12 ">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Dashboard</li>
                        </ol>
                    </div>
                    <!-- <div class=""> -->
                        <!-- <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button> -->
                    <!-- </div> -->
                </div>
                <div class="row">
                    <div class="col-lg-4">
                        <div class="card  indexicon bg-success">
                            <div class="card-body">
                                <div class="d-flex no-block">
                                    <div class="m-r-20 align-self-center"><!-- <span class="lstick m-r-20"></span> --><i class="mdi mdi-account-multiple text-white "></i></div>
                                    <div class="align-self-center">
                                     <h6 class="text-white m-t-10 m-b-0">Total Users</h6>
                                            <? if(!empty($list['total_users'])) 
                                    {
                                     echo "<h2 class='text-white  m-t-0'>".$list['total_users'];" ?></h2>";
                                    }else
                                    {echo "<h2 class='text-white  m-t-0'>0</h2>"; } 
                                    ?> 
                                  </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="card indexicon bg-warning">
                            <div class="card-body">
                                <div class="d-flex no-block text-white ">
                                    <div class="m-r-20 align-self-center"><!-- <span class="lstick m-r-20"></span> --><i class="fas fa-question"></i></div>
                                    <div class="align-self-center">
                                    
                                        <h6 class="text-white  m-t-10 m-b-0">Total Questions</h6>
                                       <? if(!empty($list['total_question'])) 
                                       {
                                        echo "<h2 class='text-white  m-t-0'>".$list['total_question'];" ?></h2>";
                                       }else
                                       {echo "<h2 class='text-white  m-t-0'>0</h2>"; } 
                                       ?> 
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="card indexicon bg-primary">
                            <div class="card-body">
                                <div class="d-flex no-block text-white ">
                                    <div class="m-r-20 align-self-center"><!-- <span class="lstick m-r-20"></span> --><i class="
 far fa-file-excel"></i></div>
                                    <div class="align-self-center">
                                        <h6 class="text-white m-t-10 m-b-0">Attempted Test</h6>
                                        <? if(!empty($list['attempt_test'])) 
                                        {
                                           echo "<h2 class='text-white  m-t-0'>".$list['attempt_test'];" ?></h2>";
                                         }else
                                         {echo "<h2 class='text-white  m-t-0'>0</h2>"; } 
                                        ?> 
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- Projects of the month -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="d-flex no-block">
                                    <div>
                                        <h4 class="card-title"><span class="lstick"></span>Recent Users</h4></div>

                                </div>
                                <div class="table-responsive m-t-20 no-wrap">
                                    <table class="table vm no-th-brd pro-of-month">
                                        <thead>
                                            <tr>
                                                <th>SNo.</th>
                                                <th>Images</th>
                                                <th>Name</th>
                                                <th>Country</th>
                                                <th>Email</th>
                                                <th>Mobile No</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                              <? $i=1; ?>
                                               <?php foreach ($list['user_list']['list1'] as  $value):?>
                                                <?php $path= base_url();?>
                                                <td><? echo $i++; ?></td>
                                                <td style="width:50px;"><span class="round"><img src="<?php echo $path.'assets/user/images/upload/'.$value['imgpath'].'';?>" alt="user" width="50" height="50" ></span></td>                                    
                                                <td><?php  echo $value['firstname'].' '.
                                                                $value['lastname'];?></td>
                                                <td><?php  echo $value['country_id']; ?></td>
                                                <td><?php  echo $value['email'];     ?></td>
                                                <td><?php  echo $value['mobile_no']; ?></td>
                                            </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="d-flex">
                                    <div>
                                        <h3 class="card-title m-b-5"><span class="lstick"></span>Recent Users</h3>
                                        <h6 class="card-subtitle">Year 2018</h6></div>
                                    <div class="ml-auto">
                                        <ul class="list-inline">
                                            <li>
                                                <div class="d-flex">
                                                    <i class="fa fa-circle font-10 m-r-10 text-primary m-t-10"></i>
                                                    <div>
                                                        <h2 class="m-b-0">10368</h2>
                                                        <h6 class="text-muted">Earning</h6></div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="d-flex">
                                                    <i class="fa fa-circle font-10 m-r-10 text-info m-t-10"></i>
                                                    <div>
                                                        <h2 class="m-b-0">12659</h2>
                                                        <h6 class="text-muted">Expense</h6></div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="d-flex">
                                                    <i class="fa fa-circle font-10 m-r-10 text-muted m-t-10"></i>
                                                    <div>
                                                        <h2 class="m-b-0">15478</h2>
                                                        <h6 class="text-muted">Sales</h6></div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div id="sales-overview" class="p-relative" style="height:400px;"></div>
                            </div>
                        </div>
                    </div>
                </div-->

            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->

            <?php include 'footer.php'; ?>

        </div>
 
    </div>
    <!--<script src="<?php //echo $path. 'assets/admin/assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.min.js';?>"></script>-->
    
    <!--<script src="<?php// echo $path. 'assets/admin/js/dashboard1.js';?>"></script>-->
</body>
</html>