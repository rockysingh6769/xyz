<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="#">
    <title>Multiple Choice Online</title>
    <base href="<? echo base_url(); ?>">
    <link href="assets/admin/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/admin/css/perfect-scrollbar.css" rel="stylesheet">
    <link rel="stylesheet" href="assets/admin/css/font-awesome.css"> 
    <link href="assets/admin/assets/plugins/chartist-js/dist/chartist.min.css" rel="stylesheet">
    <link href="assets/admin/assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css" rel="stylesheet">
    <link href="assets/admin/assets/plugins/c3-master/c3.min.css" rel="stylesheet">
    <link href="assets/admin/css/style.css" rel="stylesheet">
    <link href="assets/admin/css/dashboard1.css" rel="stylesheet">
    <link href="assets/admin/css/default-dark.css" id="theme" rel="stylesheet">
   
</head>
<?php include 'style.php'; ?>
<body class="fix-header fix-sidebar card-no-border">
<div class="alert alert-warning">
<strong><center class="one"></center></strong>  
</div>
<div class="alert alert-success">
<strong><center class="two"></center></strong> 
</div>
    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label">Multiple Choice Online</p>
        </div>
    </div>

    <div id="main-wrapper">

        <?php include 'header.php'; ?>
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <?php include 'sidebar.php' ; ?>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <!-- <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor">Profile</h3>
                </div> -->
                <div class="col-md-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Dashboard</a></li>
                        <li class="breadcrumb-item active">Profile</li>
                    </ol>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- Row -->
                <div class="row">
                    <!-- Column -->
                    <div class="col-lg-4 col-xlg-3 col-md-5">
                        <div class="card">
                            <div class="card-body">
                                <center class="m-t-30">   <div class="icn-edit">
                                           <?php $path= base_url();?>
                                           <?php if(!empty($list[0]['imgpath'])) echo "<img src=".$path.'assets/admin/images/upload/'.$list[0]['imgpath']."  width='150' height='150'    >";   
                                               else 
                                               echo "<img src=".'assets/user/images/user1.jpg'."  width='150' height='150'>";  ?>
                                           <label class="icn-profl" for="profl">
                                            <i class="material-icons dp48">i</i>
                                            <input type="file" name="imgpath" id="profl">
                                        </label>
                                    </div>
                                    <h4 class="card-title m-t-10"><? echo $list[0]['name'] ?></h4>
                                </center>
                            </div>
                            <!--div>
                                <hr> </div-->
                            <!--div class="card-body"> <small class="text-muted">Email address </small>
                                <h6>hannagover@gmail.com</h6> <small class="text-muted p-t-30 db">Phone</small>
                                <h6>+91 654 784 547</h6>
                            </div-->
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="col-lg-8 col-xlg-9 col-md-7">
                        <div class="card">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs profile-tab" role="tablist">
                                <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#home" role="tab">Account Settings</a> </li>
                                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#profile" role="tab">Update Password</a> </li>
                                <!-- <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#settings" role="tab">Settings</a> </li> -->
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div class="tab-pane active" id="home" role="tabpanel">
                                    <div class="card-body">
                                        <form id="myform" method="post" class="form-horizontal form-material">
                                            <div class="form-group">
                                                <label class="col-md-12">Full Name</label>
                                                <div class="col-md-12">
                                                    <input type="text" name="name" value="<? echo $list[0]['name'] ?>" class="form-control form-control-line">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="example-email" class="col-md-12">Email</label>
                                                <div class="col-md-12">
                                                    <input type="email" name="email"  class="form-control form-control-line" value="<? echo $list[0]['email'] ?>"  id="example-email">
                                                </div>
                                            </div>
                                            <!--div class="form-group">
                                                <label class="col-md-12">Password</label>
                                                <div class="col-md-12">
                                                    <input type="password" value="password" class="form-control form-control-line">
                                                </div>
                                            </div-->
                                            <div class="form-group">
                                                <label class="col-md-12">Phone No</label>
                                                <div class="col-md-12">
                                                    <input type="text" name="mobile_no" class="form-control form-control-line" value="<? echo $list[0]['mobile_no'] ?>">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <input type="submit" value="Update Profile" name="updbtn">
                                                    <!-- <button class="btn btn-success">Update Profile</button> -->
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <!--second tab-->
                                <div class="tab-pane" id="profile" role="tabpanel">
                                    <div class="card-body">
                                        <form id="myform2" method="post" class="form-horizontal form-material">
                                            <div class="form-group">
                                                <label class="col-md-12">Current Password</label>
                                                <div class="col-md-12">
                                                    <input type="password" name="pass" id="password" class="form-control form-control-line">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-12">New Password</label>
                                                <div class="col-md-12">
                                                    <input type="password" name="npass" id="newpassword" class="form-control form-control-line">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-12">Confirm New Password</label>
                                                <div class="col-md-12">
                                                    <input  type="password" name="cpass" id="cpassword" class="form-control form-control-line">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                   <input type="submit" value="Update Password" name="updpass">
                                                    <!-- <button class="btn btn-success">Update Password</button> -->
                                                </div>
                                            </div>
                                   <input type="hidden" name="admin_id" id="admin_id" value="<? echo $list[0]['id'] ?>">         
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                </div>
                <!-- Row -->
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
           </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <?php include 'footer.php'; ?>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
<script type="text/javascript">
$("#myform2").validate(
{   
    rules:
    {   pass:  { required: true },
        npass: { required: true },
        cpass: { required:true,equalTo:"#newpassword"} 
    },
    submitHandler: function(form) 
        { 
           var currpass=$('#password').val();
           var newpass=$('#newpassword').val();
           var conpass=$('#cpassword').val();
           var id=$('#admin_id').val();
          
           var url='<?php echo base_url();?>index.php/usercon/admin_profile';
           $.ajax(
           {    url: url,
                data:{current:currpass,new:newpass,confirm:conpass,id:id},
                type:'post',
                success:function(done)
                {
                    console.log(done);
                    if(done.trim()=='not')
                    {  $('.one').text('your current password not matched');
                       $('.alert-warning').show();
                       setTimeout(function(){
                       $('.alert-warning').hide(); }, 1000); 
                    }
                    if(done.trim()=='success')
                    {  $('.two').text('successfully update');
                       $('.alert-success').show();
                       setTimeout(function(){
                       window.location.reload() }, 1000); 
                    }
                  
                }
            });
        }
});
$("#myform").validate(
{    rules:
     {   mobile_no:  { required: true },
         name:       { required: true },
         email:      { required: true,email:true }
         
      },
        submitHandler: function(form) 
        { 
           var data = $("#myform").serializeArray();
           var val=$('#admin_id').val();
           data.push({name: 'id', value: val});
           var url='<?php echo base_url();?>index.php/usercon/admin_profile';
           $.ajax(
           {    url: url,
                data: data,
                type:'post',
                success:function(done)
                {
                    console.log(done);
                    if(done.trim()=='success')
                    {  $('.two').text('successfully update');
                       $('.alert-success').show();
                       setTimeout(function(){
                       $('.alert-success').hide(); }, 1000); 
                    }
                  
                }
            });
        }
});     
$('#profl').on('change', function()
{
  var url='<?php echo base_url();?>index.php/usercon/adminprofile_upload';
  var location='<?php base_url();?>assets/admin/images/upload/';
  var val=$('#admin_id').val(); 
  var id='id';
  var fd = new FormData(this);
  fd.append('files',$('#profl')[0].files[0]);
  fd.append(id, val);
    $.ajax(
    {
        method:"POST",
        url:url,    
        data:fd,
        cache: false,
        contentType: false,
        processData: false,   
        success:function(data)
        { 
            console.log(data); 
            if(data.trim()=='not')
            {  $('.one').text('your image size so big');
               $('.alert-warning').show();
               setTimeout(function(){
               window.location.reload() }, 1000); 
            }
            if(data.trim()=='success')
            {  $('.two').text('your image is uploaded');
               $('.alert-success').show();
               setTimeout(function(){
               window.location.reload() }, 1000); 
            }
        }     //$("#imgsrc").attr('src', location+data);  
              //var aba=$('#imgsrc').append('<img src="' + location+data + '" />');
    });
});
</script>

</body>
</html>