<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="icon" type="image/png" sizes="16x16" href="#">
	<base href="<?php echo base_url();?>">
	<title>Multiple Choice Online</title>
	<link href="assets/admin/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<link href="assets/admin/css/perfect-scrollbar.css" rel="stylesheet">
	<link rel="stylesheet" href="assets/admin/css/font-awesome.css"> 
	<link href="assets/admin/assets/plugins/chartist-js/dist/chartist.min.css" rel="stylesheet">
	<link href="assets/admin/assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css" rel="stylesheet">
	<link href="assets/admin/assets/plugins/c3-master/c3.min.css" rel="stylesheet">
	<link href="assets/admin/css/style.css" rel="stylesheet">
	<link href="assets/admin/css/dashboard1.css" rel="stylesheet">
	<link href="assets/admin/css/default-dark.css" id="theme" rel="stylesheet">

	<!-- datatable css -->
	<link rel="stylesheet" type="text/css" href="assets/admin/assets/plugins/datatables/media/css/dataTables.bootstrap4.css"> 
<style type="text/css">

.alert-warning,.alert-danger,.alert-success
{ 
	display: none;
	position: fixed;
	max-width: 50%; 
	width: 100%; 
	margin: auto;   
	top: 45%;   left: 16%;    
	z-index: 111111;     
	box-shadow: 1px 1px 8px rgba(0, 0, 0, 0.15);     
	text-align: center;     
	right: 0;    
}     
</style>
</head>
<body class="fix-header fix-sidebar card-no-border">

<div class="alert alert-warning">
<strong><center class="one"></center></strong>  
</div>

<div class="alert alert-success">
<strong><center class="two"></center></strong> 
</div>

<div class="alert alert-danger">
<strong><center class="three"></center></strong> 
</div>

	<div class="preloader">
		<div class="loader">
			<div class="loader__figure"></div>
		   <p class="loader__label">Multiple Choice Online</p>
		</div>
	</div>

	<div id="main-wrapper">

		<?php include 'header.php'; ?>


		<!-- ============================================================== -->
		<!-- ============================================================== -->
		<!-- Left Sidebar - style you can find in sidebar.scss  -->
		<!-- ============================================================== -->
		<?php include 'sidebar.php' ; ?>
		<!-- ============================================================== -->
		<!-- End Left Sidebar - style you can find in sidebar.scss  -->
		<!-- ============================================================== -->
		<!-- ============================================================== -->
		<!-- Page wrapper  -->
		<!-- ============================================================== -->
		<div class="page-wrapper">
			
			<!-- ============================================================== -->
			<!-- Container fluid  -->
			<!-- ============================================================== -->
			
			<div class="container-fluid ">
				<!-- ============================================================== -->
				<!-- Bread crumb and right sidebar toggle -->
				<!-- ============================================================== -->
				<div class="row page-titles">
 
					<div class="col-md-12">
						<ol class="breadcrumb">
							<li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
							<li class="breadcrumb-item active">Questions</li>
						</ol>
											<button type="button" class="btn btn-info btn-rounded float-right btn-sm m-0"  data-toggle="modal" data-target="#myModal"><i class="mdi mdi-plus"></i> Add Question</button>
					</div>
					<!-- <div class=""> -->
						<!-- <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button> -->
					<!-- </div> -->
				</div>

	
				<!-- ============================================================== -->
				<!-- Projects of the month -->
				<!-- ============================================================== -->
			   
				<div class="row">
					<div class="col-md-12">
<div class="card">
							<div class="card-body userdatabtn-color">
							
								<div class="table-responsive">
									<table id="myTable" class="table table-bordered table-striped">
									

										<thead>
											<tr>
												<th>SNo.</th> 
												<th>Question</th>
												<th>Correct Answer</th>
												<th class="">Action</th>
											</tr>
										</thead>
										<tbody>
										 <tr> <? $i=1;?>
												<?php foreach ($list[4] as  $value):?>
												<td><? echo $i++; ?></td>
												<td><?php  echo $value['question']; ?></td>
												<?php if($value['correct_answer']=='option_a')
													  { $ans=$value['option_a'];  }
												 else if($value['correct_answer']=='option_b')
													  { $ans=$value['option_b'];  }
												 else if($value['correct_answer']=='option_c')
													  { $ans=$value['option_c'];  } 
												 else   $ans=$value['option_d']; 
												 ?>
												 <td><?php  echo $ans;  ?></td>
                                
												
												
												<td class="questionwidthequal">
												<a  href="javascript:avoid(0)"  class="btn-info mr-1" data-toggle="modal" 
												onclick="getfunction1(<?php echo $value['id']; ?>);" data-target="#viewquestionpop">
												<i class="mdi mdi-eye"></i></a>
												<a href="javascript:avoid(0)" class="btn-info mr-1" data-toggle="modal" id="editpage"
											   onclick="getfunction2(<?php echo $value['id']; ?>);" data-target="#edit">
												<i class="mdi mdi-pencil"></i></a>
												<a href="#" class=" btn-danger mt-1"   data-toggle="modal" onclick="getfunction3(<?php echo $value['id']; ?>);" data-target="#delete">
												<i class="mdi mdi-delete-empty"></i></a></td>
										 </tr>
										<?php endforeach; ?>   
										</tbody>
									</table>
								</div>
							</div>
						</div>
						
					</div>
					
					
				</div>

			</div>
			<!-- ============================================================== -->
			<!-- End Container fluid  -->

			<!-- ============================================================== -->
			<!-- End Container fluid  -->
			<!-- ============================================================== -->
			<!-- ============================================================== -->
			<!-- footer -->
			<!-- ============================================================== -->
			<?php include 'footer.php'; ?>
			<!-- ============================================================== -->
			<!-- End footer -->
			<!-- ============================================================== -->
		</div>
		<!-- ============================================================== -->
		<!-- End Page wrapper  -->
		<!-- ============================================================== -->
	</div>
	<!-- ============================================================== -->
	<!-- End Wrapper -->
	<!-- ============================================================== -->
	<!-- ============================================================== -->
	<!-- All Jquery -->
	<!-- ============================================================== -->


<!-- Add Question -->
	 
								<div class="modal fade bs-example-modal-md question-modal-btn" tabindex="-1" role="dialog" id="myModal" aria-hidden="true" style="display: none;">
									<div class="modal-dialog modal-lg">
										<div class="modal-content">
											<div class="modal-header border-0">
												<h2 class="modal-title" id="myLargeModalLabel">Add Question</h2>
												<button type="button"  class="close" data-dismiss="modal" aria-hidden="true">×</button>
											</div>
											<div class="modal-body">
											<div class="row">

					   <div class="tab-inn ">
					   
					   
					   
					   
					   
							<p class="fontsize-p">Choose Your Priorities</p>
								<div class="col-md-12">
										<div id="demo">
						
						<!--  start  Priorities list form -->
						<form id="addform" method="post">
						
										<div class="row"> 	
									<div class="col-md-6">
									
									<div class="form-group lgbtn">
										
									   <select id="addcountry"  name="addcountry" class="form-control"  > 
										  <option required value="">Select/Country</option>
										  <?php foreach($list[0] as $key => $value){ ?>
												<option  value="<?php print_r($value['id'])?>">
												<?php echo $value['country_name']; ?></option> 
												<?php } ?>
										</select>
									 </div>
                                    </div>
									<div class="col-md-6">
                                        <div class="form-group lgbtn">
                                       	    <select required class="form-control" id="addstate">
												<option value="">State/Province</option>
											</select>
										</div>
                                    </div>
									<div class="col-md-6">
										<div class="form-group lgbtn">
											<select required id="addcourse"  class="form-control"  > 
										  <option value="">Select/Course </option>
										  <?php foreach($list[1] as $key => $value){ ?>
												<option  value="<?php print_r($value['id'])?>">
												<?php echo $value['course_name']; ?></option> 
												<?php } ?>
										</select>
										</div>
                                    </div>
									<div class="col-md-6">
                                       <div class="form-group lgbtn">
                                      	 <select required id="addgrade"  class="form-control"  > 
												 <option value="">Grade/Level</option>
												 <?php foreach($list[2] as $key => $value){ ?>
												 <option  value="<?php print_r($value['id'])?>">
												 <?php echo $value['grade_name']; ?></option> 
												<?php } ?>
											</select>
										</div>
                                    </div>
									<div class="col-md-6">
                                        <div class="form-group lgbtn">

											  <select required id="addyear"  class="form-control"  > 
												 <option value="">Year/List</option>
												 <?php foreach($list[3] as $key => $value){ ?>
												 <option  value="<?php print_r($value['id'])?>">
												 <?php echo $value['year']; ?></option> 
												<?php } ?>
											</select>
										</div>
                                    </div>
									<div class="col-md-6">
                                        <div class="form-group lgbtn">

											<select required class="form-control" id="addsubject" >
												<option value="">Subject/List</option>
											</select>
										</div>
                                    </div>
									<div class="col-md-6">
										<div class="form-group lgbtn">
											<select class="form-control" id="addchapter">
												<option value="">Chapter/List</option>
											</select>
										</div>
									</div>											
							  </div>	
						
						<!-- End of Priorities list form  -->
						</form>

<!-- form list data post -->		
<script type="text/javascript">

//This is for validation Priorities form //
$('#addyear').click(function(){
  if( $("#addyear option:selected").val() =='')
  alert("Please select atlease one option");
});

$('#addgrade').click(function(){
  if( $("#addgrade option:selected").val() =='')
  alert("Please select atlease one option");
});


// select country with state //
$('#addcountry').click(function()
{ 
	if( $("#addcountry option:selected").val() ==''){
	  alert("Please select atlease one option");
	} 
	else 
	{     var data = $('#addcountry').val();
		  var url='<?php echo base_url();?>usercon/questions'; 
		  $.ajax(
		  {  
				url: url,
				data:{country_id:data},
				type:'post',
				success:function(response)
				{     
					console.log(response);
					$('#addstate').empty();
					$('#addstate').append('<option value="" disabled selected>Select/state</option>');
					if(response.length > 0)
					{  
					   var selOpts = "";
						for (i=0;i<response.length;i++)
						{   var id =response[i]['id'];
							var val = response[i]['state_name'];
							selOpts += "<option value="+id+">"+val+"</option>";
						}
						$('#addstate').append(selOpts);

					}
					else
					{
					 $('#addstate').empty();
					} 
					
				}
		  });
	}     
});
// select cource automatic subject and chapter //
$('#addcourse').click(function()
{
	if($("#addcourse option:selected").val() ==''){
	  alert("Please select atlease one option");
	  $('#addsubject').empty();
	  $('#addchapter').empty();
	}  
	else    
	{     var data = $('#addcourse').val();
		  var url='<?php echo base_url();?>usercon/questions'; 
		  $.ajax(
		  {  
				url: url,
				data:{course_id:data},
				type:'post',
				success:function(response)
				{      
					console.log(response);
					var result1=response[0];
					var result2=response[1];
					
                    $('#addsubject').empty();
                    if(result1.length > 0  )
					{ 
						var selOpts = "";
						for (i=0;i<result1.length;i++)
						{   var id=result1[i]['id'];
							var val = result1[i]['subject_name'];
							selOpts += "<option value="+id+">"+val+"</option>";
						}
						$('#addsubject').append(selOpts);
					 }
					 else
					 {
					   $('#addsubject').empty();
					 }   
					 $('#addchapter').empty();
					 if(result2.length > 0 )
					 { 
						var selOpts = "";
						for (i=0;i<result2.length;i++)
						{   var id=result2[i]['id'];
							var val = result2[i]['chapter_name'];
							selOpts += "<option value="+id+">"+val+"</option>";
						}
						$('#addchapter').append(selOpts);
					 }
					 else
					 {
					 $('#addchapter').empty();
					 }
				}     
		   });
	}

});
</script>
							<!-- Start MCQ Question form -->
								<form id="addquestion" method="post">  
												<div class="row">
												<p class="fontsize-p">Question</p>

													<div class="form-group col-md-12">
														<textarea name="addquest" class="form-control" rows="3" placeholder="Question"></textarea>
													</div>
												</div>	
												<div class="row">
													<div class="form-group col-lg-6">
														<label for="Optiona">Option A</label>
														<input name="option_a" class="form-control" id="Optiona" type="text">
													</div>
													<div class="form-group col-lg-6">
														<label for="Optionb">Option B</label>
														<input name="option_b" class="form-control" id="Optionb" type="text">
													</div>
													<div class="form-group col-lg-6">
														<label for="Optionc">Option C</label>
														<input name="option_c" class="form-control" id="Optionc" type="text">
													</div>
													<div class="form-group col-lg-6">
														<label for="Optiond">Option D</label>
														<input name="option_d" class="form-control" id="Optiond" type="text">
													</div>

													<div class="form-group col-lg-12 questioninput-size">
												<p class="fontsize-p pl-0">Correct Answer</p>
													
																						
												<!-- Material unchecked -->
																<div class="form-check-inline ">
																  <input type="radio"  class="form-check-input" value="option_a" id="Option1" name="materialExampleRadios">
																  <label class="form-check-label" for="Option1" name="aa">Option A</label>
																</div>
																

																<!-- Material checked -->


																<div class="form-check-inline padding">
																
																
																  <input type="radio"  class="form-check-input" value="option_b" id="Option2" name="materialExampleRadios">
																  <label class="form-check-label" for="Option2">Option B</label>
																</div>
																<div class="form-check-inline padding">
																  <input type="radio"  class="form-check-input" value="option_c" id="Option3" name="materialExampleRadios">
																  <label class="form-check-label" for="Option3">Option C</label>
																</div>

																<!-- Material checked -->
																<div class="form-check-inline padding">
																  <input type="radio"  class="form-check-input" value="option_d" id="Option4" name="materialExampleRadios" >
																  <label class="form-check-label" for="Option4">Option D</label>
																</div>												
													</div>									
                                             </div>
                                          </div>					
						       	     </div>
						         </div>
											
											</div>
											
											</div>
											<div class="modal-footer border-0">
												<button type="button"  class="btn btn-secondary waves-effect text-left" data-dismiss="modal">Cancel</button>
												<button type="button" id="addbtn" class="btn btn-success waves-effect text-left">Add</button>                                               
											</div>
										</div>
										<!-- /.modal-content -->
									</div>
									<!-- /.modal-dialog -->
								</div>
								<!-- /Add Question -->
								<input type="hidden" id="hidden" name="prioritie_id" value="">
			<!-- End of MCQ Form -->
			</form>         
<script src="<?php echo $path.'assets/admin/js/validate.js';?>"></script>
<script type="text/javascript">
//** submit Priorities form **// 
$('#addform').click(function(){
 
 if( $('#addcountry').val() !='' && $('#addcourse').val() !='' && $('#addgrade').val() !=''
	 && $('#addyear').val() !='' && $('#addchapter').val()!='' && $('#addstate').val() !=''
	 && $('#addsubject').val() !='')
 {
		var country_id  =  $('#addcountry').val();
		var course_id   =  $('#addcourse') .val();
		var grade_id    =  $('#addgrade')  .val();
		var year_id     =  $('#addyear')   .val();
		var chapter_id  =  $('#addstate   option:selected').val(); 
		var subject_id  =  $('#addsubject option:selected').val();
		var state_id    =  $('#addchapter option:selected').val();
	   
		
	   var url='<?php echo base_url();?>usercon/addfilter_priorities'; 
	   $.ajax(
	   {
			  url: url,
			  data:{country_id:country_id,course_id:course_id,grade_id:grade_id,
					year_id:year_id,chapter_id:chapter_id,subject_id:subject_id,
					state_id:state_id },
			  type:'post',
			  success:function(success)
			  { 
				 console.log(success);
				 if(success.trim()=='please')
				 { $('.three').text('please fill All prioritie infomation ');
				   $('.alert-danger').show();
				   setTimeout(function(){
				   $('.alert-danger').hide(); }, 2000); 
				 }
				 if(success.trim()=='already')
				 {  $('.one').text('this Country, Year, Course and Grade Already Exist ');
					$('.alert-warning').show();
					setTimeout(function(){
					$('.alert-warning').hide();  }, 3000); 
				 }
				 if(success.trim()=='success')
				 {  $('.two').text('successfully add ');
					$('.alert-success').show();
					setTimeout(function(){
					$('.alert-success').hide();  }, 2000); 
				 }
				 else{ $('#hidden').val(success); }

				  
			  }  


		});
 }  
});
//** submit question form **//
$('#addbtn').click(function(){

	 var data = $("#addquestion").serialize();
	 var url='<?php echo base_url();?>usercon/addfilter_priorities'; 
	 $.ajax(
	 {
		  url: url,
		  data: data,
		  type:'post',
		  success:function(done)
		  { 
				 console.log(done);
				 if(done.trim()=='already')
				 {  $('.one').text('this question and answer already exist ');
					$('.alert-warning').show();
					setTimeout(function(){
					$('.alert-warning').hide();  }, 2000); 
				 }
				 if(done.trim()=='please')
				 {  $('.one').text('please fill your question and All options ');
					$('.alert-warning').show();
					setTimeout(function(){
					$('.alert-warning').hide();  }, 2000); 
				 }
				 
				 if(done.trim()=='success')
				 { $('.two').text('successfully Add ');
				   $('.alert-success').show();
				   setTimeout(function(){
				   window.location= "<?php  echo base_url('usercon/questions'); ?>";  },  1000); 
				 }

				 if(done.trim()=='notselected')
				 { $('.three').text('please select correct answer ');
				   $('.alert-danger').show();
				   setTimeout(function(){
				   $('.alert-danger').hide(); }, 2000); 
					
				 }
				 if(done.trim()=='emptyprioritie')
				 { $('.three').text('please fill your prioritie infomation ');
				   $('.alert-danger').show();
				   setTimeout(function(){
				   $('.alert-danger').hide(); }, 2000); 
					
				 }
		  }  

	 });
		



});
</script>



								 <!-- Edit modal -->
								<div class="modal fade bs-example-modal-md question-modal-btn" tabindex="-1" role="dialog" id="edit" aria-hidden="true" style="display: none;">
									<div class="modal-dialog modal-lg">
										<div class="modal-content">
											<div class="modal-header border-0">
												<h4 class="modal-title" id="myLargeModalLabel">Edit Question</h4>
												<button type="button"  class="close" data-dismiss="modal" aria-hidden="true">×</button>
											</div>
 <div class="modal-body">
											<div class="row">

					   <div class="tab-inn ">
					   
					   
					   
					   
					   
					<p class="fontsize-p">Choose Your Priorities</p>
					<!-- Start Edit Priorities information form -->
					<form id="editform" method="post">
								 

								   
									<div class="col-md-12">
										<div id="demo">
											<form>
										<div class="row"> 	
									<div class="col-md-6">




										<div class="form-group lgbtn">

											<select id="editcountry"  name="editcountry" class="form-control" > 
										<option  value="">Select/Country</option>
										  <?php foreach($list[0] as $key => $value){ ?>
												<option   value="<?php print_r($value['id'])?>">
												<?php echo $value['country_name']; ?></option> 
												<?php } ?>
										</select>
										</div>



									</div>
									<div class="col-md-6">




										<div class="form-group lgbtn">

										<select required class="form-control" id="editstate" >
												<option value="">State/Province</option>
											</select>
										</div>



									</div>
									<div class="col-md-6">



										<div class="form-group lgbtn">

											<select required id="editcourse"  class="form-control"  > 
										  <option value="">Select/Course </option>
										  <?php foreach($list[1] as $key => $value){ ?>
												<option  value="<?php print_r($value['id'])?>">
												<?php echo $value['course_name']; ?></option> 
												<?php } ?>
										</select>
										</div>



									</div>
									<div class="col-md-6">




										<div class="form-group lgbtn">

											<select required id="editgrade"  class="form-control"  >
												<option value="">Grade/Level</option>
												 <?php foreach($list[2] as $key => $value){ ?>
												 <option  value="<?php print_r($value['id'])?>">
												 <?php echo $value['grade_name']; ?></option> 
												<?php } ?>
											</select>
										</div>



									</div>
									<div class="col-md-6">




										<div class="form-group lgbtn">

											 <select required id="edityear"  class="form-control"  > 
												  <option value="">Year/List</option>
												 <?php foreach($list[3] as $key => $value){ ?>
												 <option  value="<?php print_r($value['id'])?>">
												 <?php echo $value['year']; ?></option> 
												<?php } ?>
											</select>
										</div>


									</div>
									<div class="col-md-6">




										<div class="form-group lgbtn">

											<select required class="form-control" id="editsubject" >
												<option value="">Subject/List</option>
												
											</select>
										</div>


									</div>
									<div class="col-md-6">
										<div class="form-group lgbtn">
											<select class="form-control" id="editchapter">
												<option value="default">Chapter/List</option>
												
											</select>
										</div>
									</div>											
										</div>	
					   <input type="hidden" id="quest_prioritie_id" name="quest_prioritie_id" val="">
					   <!-- End of edit Priorities form -->			
						</form>					
						<!-- Start MCQ  Question Edit form -->
						<form id="editquestion" method="post"> 			
												<div class="row">
												<p class="fontsize-p">Question</p>

													<div class="form-group col-md-12">
														<textarea name="editquest" class="form-control" rows="3"  id="area1" ></textarea>
													</div>
												</div>	
												<div class="row">
													<div class="form-group col-lg-6">
														<label for="Optiona">Option A</label>
														<input class="form-control" id="select_a" name="editopt_a" type="text"  value="">
													</div>
													<div class="form-group col-lg-6">
														<label for="Optionb">Option B</label>
														<input class="form-control" id="select_b" name="editopt_b" type="text"  value="">
													</div>
													<div class="form-group col-lg-6">
														<label for="Optionc">Option C</label>
														<input class="form-control" id="select_c" name="editopt_c" type="text"  value="">
													</div>
													<div class="form-group col-lg-6">
														<label for="Optiond">Option D</label>
														<input class="form-control" id="select_d" name="editopt_d" type="text"  value="">
													</div>

													<div class="form-group col-lg-12 questioninput-size">
												<p class="fontsize-p pl-0">Correct Answer</p>
										
																						
												<!-- Material unchecked -->
																<div class="form-check-inline ">
																  <input type="radio" class="form-check-input" value="option_a" id="Option5" name="materialExampleRadios">
																  <label class="form-check-label" for="Option5">Option A</label>
																</div>

																<!-- Material checked -->
																<div class="form-check-inline padding">
																  <input type="radio" class="form-check-input" value="option_b" id="Option6" name="materialExampleRadios">
																  <label class="form-check-label" for="Option6">Option B</label>
																</div>
																<div class="form-check-inline padding">
																  <input type="radio" class="form-check-input" value="option_c" id="Option7" name="materialExampleRadios">
																  <label class="form-check-label" for="Option7" >Option C</label>
																</div>

																<!-- Material checked -->
																<div class="form-check-inline padding">
																  <input type="radio" class="form-check-input" value="option_d" id="Option8" name="materialExampleRadios"  >
																  <label class="form-check-label" for="Option8">Option D</label>
																</div>									
													</div>												

										</div>
									</div>					
								
                           					
							</div>
						</div>
											</div>
											
											</div>
											<div class="modal-footer border-0">
												<button type="button"  class="btn btn-secondary waves-effect text-left" data-dismiss="modal">Cancel</button>
												<button type="button" id="editbtn" class="btn btn-success waves-effect text-left">Save Change</button>                                               
											</div>
										</div>
										<!-- /.modal-content -->
									</div>
									<!-- /.modal-dialog -->
								</div>
								<!-- /Edit modal -->   
                          <input type="hidden" id="question_id" name="question_id" val="">   
				<!-- End of MCQ Edit form -->
				  </form>  


<!-- delete modal -->
								<div class="modal fade bs-example-modal-md user-delete-btn" tabindex="-1" role="dialog" id="delete" aria-hidden="true" style="display: none;">
									<div class="modal-dialog modal-md">
										<div class="modal-content">
											<div class="modal-header p-0 border-0">
										 
												<!--button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button-->
											</div>
											<div class="modal-body text-center">
										    
											   <i class="mdi mdi-close"></i>
											  <h1>Are you sure</h1>
											  <p>Do you really want to delete these records? This process cannot be undone.</p>
											 <ul>
											 <a href="#" class="btn btn-secondary" data-dismiss="modal">Cancel</a>
											 <a  type="button" id="delbtn"  class="btn btn-danger">Delete</a>
											 </ul>											  
										      
											</div>
                      <input type="hidden" id="delete_id" name="delete_id" value="">
										</div>
										<!-- /.modal-content -->
									</div>
									<!-- /.modal-dialog -->
								</div>




	<!--add view modal-->							
 <div class="modal fade bs-example-modal-md question-modal-btn" tabindex="-1" role="dialog" id="viewquestionpop" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header border-0">
				<h2 class="modal-title" id="myLargeModalLabel">View Question</h2>
				<button type="button" id="clobtn" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="tab-inn ">
					   <p class="fontsize-p">Your Priorities</p>
		
<!-- start view  form -->
		<form>
						 <div class="col-md-12">
						  <div id="demo">
			              <div class="row">     
								 <div class="col-md-6">
								  <div class="form-group lgbtn">
								   <select disabled="" class="form-control" id="viewcountry">
								</select>
							</div>
						</div>
						<div class="col-md-6">
						  <div class="form-group lgbtn">
						   <select disabled="" class="form-control" id="viewstate">
						</select>
					</div>
				</div>
				<div class="col-md-6">
				  <div class="form-group lgbtn">

				   <select disabled="" class="form-control" id="viewcourse">
				</select>
			</div>
		</div>
		<div class="col-md-6">
		  <div class="form-group lgbtn">
		   <select disabled="" class="form-control" id="viewgrade">
		</select>
	</div>
</div>
<div class="col-md-6">
  <div class="form-group lgbtn">

   <select disabled="" class="form-control" id="viewyear">
</select>
</div>
</div>
<div class="col-md-6">
  <div class="form-group lgbtn">

   <select disabled="" class="form-control" id="viewsubject">
</select>
</div>
</div>
<div class="col-md-6">
  <div class="form-group lgbtn">
   <select disabled="" class="form-control" id="viewchapter">
</select>
</div>
</div>                                          
</div>

<div class="row">
	<p class="fontsize-p mt-3">Your Question</p>
 <div class="form-group col-md-12">
 <textarea disabled="" class="form-control" id="view_ques" rows="3" placeholder="Add Question">
</textarea>
</div>
</div>  
<div class="row">
 <div class="form-group col-lg-6">
  <label for="Optiona">Option A</label>
  <input class="form-control" id="view_a"  type="text" disabled="">
</div>
<div class="form-group col-lg-6">
  <label for="Optionb">Option B</label>
  <input class="form-control" id="view_b"  type="text" disabled="">
</div>
<div class="form-group col-lg-6">
  <label for="Optionc">Option C</label>
  <input class="form-control" id="view_c"  type="text" disabled="">
</div>
<div class="form-group col-lg-6">
  <label for="Optiond">Option D</label>
  <input class="form-control" id="view_d"  type="text" disabled="">
</div>                                          
</div>

<div class="row">
<div class="form-group col-lg-12 questioninput-size">
<p class="fontsize-p pl-0 ml-0">Correct Answer</p>


<!-- Material unchecked -->
<div class="form-check-inline ">
 <input disabled="" type="radio" class="form-check-input disabled" id="ansview_opt_a" name="materialExampleRadios">
 <label class="form-check-label" for="Option11">Option A</label>
</div>

<!-- Material checked -->
<div class="form-check-inline padding">
 <input disabled="" type="radio" class="form-check-input disabled" id="ansview_opt_b" name="materialExampleRadios">
 <label class="form-check-label" for="Option22">Option B</label>
</div>
<div class="form-check-inline padding">
 <input disabled="" type="radio" class="form-check-input disabled" id="ansview_opt_c" name="materialExampleRadios">
 <label class="form-check-label" for="Option33">Option C</label>
</div>

<!-- Material checked -->
<div class="form-check-inline padding">
 <input checked="" type="radio" class="form-check-input" id="ansview_opt_d" name="materialExampleRadios">
 <label class="form-check-label" for="Option44">Option D</label>
</div>  
</div>
</div>


</div>    <!-- End of view form -->              
              </form>                     
</div>
</div>
</div>
</div>

<script type="text/javascript">

//** This is for view page **// 
function getfunction1(id)
{
  var url='<?php echo base_url();?>usercon/editfilter_priorities';  
    	$.ajax(
		{  
			url: url,
			data:{id:id},
			type:'post',
			success:function(view)
		    {
                  console.log(view);
                  var answer1=view[0];
				  var answer2=view[1];
				  var answer3=view[2];   
				  var answer4=view[3];
				  var answer5=view[4];  
				  var answer6=view[5];
				  var answer7=view[6];
				  var answer8=view[7]; 

             	    if(answer1.length > 0  )
					{ 
						for (i=0;i<answer1.length;i++)
						{   var question = answer1[i]['question'];
							var option_a = answer1[i]['option_a'];
					        var option_b = answer1[i]['option_b']; 
							var option_c = answer1[i]['option_c'];
                            var option_d = answer1[i]['option_d'];
                            var answer   = answer1[i]['correct_answer'];
                            var prio_id  = answer1[i]['prioritie_id'];
                            var quest_id = answer1[i]['id']; 
							if(option_a != '')
							{ 
							 $('#view_a').empty();	
							 $('#view_a').val(option_a); 
							} 
							if(option_b != '')
							{ 
							 $('#view_b').empty();	
							 $('#view_b').val(option_b); 
							}
							if(option_c != '')
							{ 
							 $('#view_c').empty();	
							 $('#view_c').val(option_c); 
							}  
							if(option_d != '')
							{ 
							 $('#view_d').empty();	
							 $('#view_d').val(option_d); 
							} 
							if(question != '')
							{ 
							 $('textarea#view_ques').empty();	
							 $('textarea#view_ques').val(question); 
							}
                            if(answer != '' )
                            {
                            	if(answer == 'option_a')
                            	{ $('#view_a').addClass('form-control correctans');
                            	  $('#ansview_opt_a').attr("checked", "checked"); } 
                            	else if(answer == 'option_b')
                            	{ $('#view_b').addClass('form-control correctans'); 
                                  $('#ansview_opt_b').attr("checked", "checked"); }
                            	else if(answer == 'option_c')
                            	{ $('#view_c').addClass('form-control correctans'); 
                                  $('#ansview_opt_c').attr("checked", "checked"); }
                            	else
                            	{ $('#view_d').addClass('form-control correctans'); 
                                  $('#ansview_opt_d').attr("checked", "checked"); }
                            }
                            if(prio_id != '')
                            {
                                $('#quest_prioritie_id').val(prio_id); 
                            }
                            if(quest_id != '')
                            {
                                $('#question_id').val(quest_id); 
                            }
                      	}
				    }
				    if(answer2.length > 0  )
					{   var selOpts = "";
						for (i=0;i<answer2.length;i++)
						{   var id=answer2[i]['id'];
				            var val = answer2[i]['country_name'];
							selOpts += "<option selected value="+id+">"+val+"</option>";
						}
						$('#viewcountry').append(selOpts);
					 }
					 if(answer3.length > 0 )
					 {  var selOpts = "";
						for (i=0;i<answer3.length;i++)
						{   var id=answer3[i]['id'];
							var val = answer3[i]['chapter_name'];
							selOpts += "<option selected value="+id+">"+val+"</option>";
						}
						$('#viewchapter').append(selOpts);
					 }
					 if(answer4.length > 0 )
					 {  var selOpts = "";
						for (i=0;i<answer4.length;i++)
						{   var id=answer4[i]['id'];
							var val = answer4[i]['subject_name'];
							selOpts += "<option selected value="+id+">"+val+"</option>";
						}
						$('#viewsubject').append(selOpts);
					 }
					 if(answer5.length > 0 )
					 {  var selOpts = "";
						for (i=0;i<answer5.length;i++)
						{   var id=answer5[i]['id'];
							var val = answer5[i]['course_name'];
							selOpts += "<option selected value="+id+">"+val+"</option>";
						}
						$('#viewcourse').append(selOpts);
					 }
					 if(answer6.length > 0 )
					 {  var selOpts = "";
						for (i=0;i<answer6.length;i++)
						{   var id=answer6[i]['id'];
							var val = answer6[i]['grade_name'];
							selOpts += "<option selected value="+id+">"+val+"</option>";
						}
						$('#viewgrade').append(selOpts);
					 }
					 if(answer7.length > 0 )
					 {  var selOpts = "";
						for (i=0;i<answer7.length;i++)
						{   var id=answer7[i]['id'];
							var val = answer7[i]['year'];
							selOpts += "<option selected value="+id+">"+val+"</option>";
						}
						$('#viewyear').append(selOpts);
					 }
					 if(answer8.length > 0 )
					 {  var selOpts = "";
						for (i=0;i<answer8.length;i++)
						{   var id=answer8[i]['id'];
							var val = answer8[i]['state_name'];
							selOpts += "<option selected value="+id+">"+val+"</option>";
							
						}
						$('#viewstate').append(selOpts);
					 }
		    }     
	    });

}

//** This is for show full form information by id **//
function getfunction2(id)
{       
		var url='<?php echo base_url();?>usercon/editfilter_priorities';  
		$.ajax(
		{  
			url: url,
			data:{id:id},
			type:'post',
			success:function(response)
			{     
				  console.log(response);
				  var result1=response[0];
				  var result2=response[1];
				  var result3=response[2];   
				  var result4=response[3];
				  var result5=response[4];  
				  var result6=response[5];
				  var result7=response[6];
				  var result8=response[7];  

					
             	    if(result1.length > 0  )
					{ 
						for (i=0;i<result1.length;i++)
						{   var question = result1[i]['question'];
							var option_a = result1[i]['option_a'];
					        var option_b = result1[i]['option_b']; 
							var option_c = result1[i]['option_c'];
                            var option_d = result1[i]['option_d'];
                            var answer   = result1[i]['correct_answer'];
                            var prio_id  = result1[i]['prioritie_id'];
                            var quest_id = result1[i]['id']; 
							if(option_a != '')
							{ 
							 $('#select_a').empty();	
							 $('#select_a').val(option_a); 
							} 
							if(option_b != '')
							{ 
							 $('#select_b').empty();	
							 $('#select_b').val(option_b); 
							}
							if(option_c != '')
							{ 
							 $('#select_c').empty();	
							 $('#select_c').val(option_c); 
							}  
							if(option_d != '')
							{ 
							 $('#select_d').empty();	
							 $('#select_d').val(option_d); 
							} 
							if(question != '')
							{ 
							 $('textarea#area1').empty();	
							 $('textarea#area1').val(question); 
							}
                            if(answer != '' )
                            {
                            	if(answer == 'option_a')
                            	{ $('#select_a').addClass('form-control correctans');
                            	  $('#Option5').attr("checked", "checked"); } 
                            	else if(answer == 'option_b')
                            	{ $('#select_b').addClass('form-control correctans'); 
                                  $('#Option6').attr("checked", "checked"); }
                            	else if(answer == 'option_c')
                            	{ $('#select_c').addClass('form-control correctans'); 
                                  $('#Option7').attr("checked", "checked"); }
                            	else
                            	{ $('#select_d').addClass('form-control correctans'); 
                                  $('#Option8').attr("checked", "checked"); } 
                            }
                            if(prio_id != '')
                            {
                                $('#quest_prioritie_id').val(prio_id); 
                            }
                            if(quest_id != '')
                            {
                                $('#question_id').val(quest_id); 
                            }
                      	}
				    }
				    if(result2.length > 0  )
					{   var selOpts = "";
						for (i=0;i<result2.length;i++)
						{   var id=result2[i]['id'];
				            var val = result2[i]['country_name'];
							selOpts += "<option selected value="+id+">"+val+"</option>";
						}
						$('#editcountry').append(selOpts);
					 }
					 if(result3.length > 0 )
					 {  var selOpts = "";
						for (i=0;i<result3.length;i++)
						{   var id=result3[i]['id'];
							var val = result3[i]['chapter_name'];
							selOpts += "<option selected value="+id+">"+val+"</option>";
						}
						$('#editchapter').append(selOpts);
					 }
					 if(result4.length > 0 )
					 {  var selOpts = "";
						for (i=0;i<result4.length;i++)
						{   var id=result4[i]['id'];
							var val = result4[i]['subject_name'];
							selOpts += "<option selected value="+id+">"+val+"</option>";
						}
						$('#editsubject').append(selOpts);
					 }
					 if(result5.length > 0 )
					 {  var selOpts = "";
						for (i=0;i<result5.length;i++)
						{   var id=result5[i]['id'];
							var val = result5[i]['course_name'];
							selOpts += "<option selected value="+id+">"+val+"</option>";
						}
						$('#editcourse').append(selOpts);
					 }
					 if(result6.length > 0 )
					 {  var selOpts = "";
						for (i=0;i<result6.length;i++)
						{   var id=result6[i]['id'];
							var val = result6[i]['grade_name'];
							selOpts += "<option selected value="+id+">"+val+"</option>";
						}
						$('#editgrade').append(selOpts);
					 }
					 if(result7.length > 0 )
					 {  var selOpts = "";
						for (i=0;i<result7.length;i++)
						{   var id=result7[i]['id'];
							var val = result7[i]['year'];
							selOpts += "<option selected value="+id+">"+val+"</option>";
						}
						$('#edityear').append(selOpts);
					 }
					 if(result8.length > 0 )
					 {  var selOpts = "";
						for (i=0;i<result8.length;i++)
						{   var id=result8[i]['id'];
							var val = result8[i]['state_name'];
							selOpts += "<option selected value="+id+">"+val+"</option>";
							
						}
						$('#editstate').append(selOpts);
					 }
			}
			   
		});
}  
//** This is for update information **//
$('#editbtn').click(function()
{
  if( $('#editcountry').val() !='' && $('#editcourse').val() !='' && $('#editgrade').val() !=''
	 && $('#edityear').val() !='' && $('#editchapter').val()!='' && $('#editstate').val() !=''
	 && $('#editsubject').val() !=''&& $('#quest_prioritie_id').val()!='' )
  {
		var country_id  =  $('#editcountry option:selected').val();
		var course_id   =  $('#editcourse  option:selected').val();
		var grade_id    =  $('#editgrade   option:selected').val();
		var year_id     =  $('#edityear    option:selected').val();
		var chapter_id  =  $('#editstate   option:selected').val(); 
		var subject_id  =  $('#editsubject option:selected').val();
		var state_id    =  $('#editchapter option:selected').val();
		var prioritie_id=  $('#quest_prioritie_id').val();
		var data = $('#editquestion').serializeArray();
        data.push({name: 'country_id', value: country_id});
        data.push({name: 'course_id', value: course_id});
        data.push({name: 'grade_id', value: grade_id});
        data.push({name: 'year_id', value: year_id});
        data.push({name: 'chapter_id', value: chapter_id});
        data.push({name: 'subject_id', value: subject_id});
        data.push({name: 'state_id', value: state_id});
        data.push({name: 'prioritie_id', value: prioritie_id});
		
	   var url='<?php echo base_url();?>usercon/editfilter_priorities'; 
	   $.ajax(
	   {
			  url: url,
			  data: data,
			  type:'post',
			  success:function(success)
			  { 
				 console.log(success);
				 if(success.trim()=='already')
				 {  $('.one').text('this Country, Year, Course and Grade Already Exist ');
					$('.alert-warning').show();
					setTimeout(function(){
					$('.alert-warning').hide();  }, 3000); 
				 }
				 if(success.trim()=='success')
				 { $('.two').text('successfully Update ');
				   $('.alert-success').show();
				   setTimeout(function(){
				   window.location= "<?php  echo base_url('usercon/questions'); ?>";  },  1000); 
				 }
				  if(success.trim()=='prioritieempty')
				 {  $('.one').text('please fill All Prioritie information ');
					$('.alert-warning').show();
					setTimeout(function(){
					$('.alert-warning').hide();  }, 2000); 
				 }

				  
			  }  


		});
  }else{  alert('please Select All Priorities Information'); }  
});

//This is for validation Priorities form //
$('#edityear').click(function(){
  if( $("#edityear option:selected").val() =='')
  alert("Please select atlease one option");
});

$('#editgrade').click(function(){
  if( $("#editgrade option:selected").val() =='')
  alert("Please select atlease one option");
});


// select country with state //
$('#editcountry').click(function()
{ 
	if( $("#editcountry option:selected").val() ==''){
	  alert("Please select atlease one option");
	} 
	else 
	{     var data = $('#editcountry').val();
		  var url='<?php echo base_url();?>usercon/questions'; 
		  $.ajax(
		  {  
				url: url,
				data:{country_id:data},
				type:'post',
				success:function(response)
				{     
					console.log(response);
					$('#editstate').empty();
					 $('#editstate').append('<option value="" disabled selected>Select/state</option>');
					if(response.length > 0)
					{  
					   var selOpts = "";
						for (i=0;i<response.length;i++)
						{   var id =response[i]['id'];
							var val = response[i]['state_name'];
							selOpts += "<option value="+id+">"+val+"</option>";
						}
						$('#editstate').append(selOpts);

					}
					else
					{
					 $('#editstate').empty();
					} 
					
				}
			   
		   });
	}     
});

// select cource automatic subject and chapter //
$('#editcourse').click(function()
{
	if($("#editcourse option:selected").val() ==''){
	  alert("Please select atlease one option");
	  $('#editsubject').empty();
	  $('#editchapter').empty();
	}  
	else    
	{     var data = $('#editcourse').val();
		  var url='<?php echo base_url();?>usercon/questions'; 
		  $.ajax(
		  {  
				url: url,
				data:{course_id:data},
				type:'post',
				success:function(response)
				{      
					console.log(response);
					var result1=response[0];
					var result2=response[1];
					

					$('#editsubject').empty();
					 $('#editsubject').append('<option value="" disabled selected>Select/subject</option>');
					if(result1.length > 0  )
					{ 
						var selOpts = "";
						for (i=0;i<result1.length;i++)
						{   var id=result1[i]['id'];
							var val = result1[i]['subject_name'];
							selOpts += "<option value="+id+">"+val+"</option>";
						}
						$('#editsubject').append(selOpts);
					 }
					 else
					 {
					   $('#editsubject').empty();
					 }   
					 $('#editchapter').empty();
					 $('#editchapter').append('<option value="" disabled selected>Select/chapter</option>');
					 if(result2.length > 0 )
					 { 
						var selOpts = "";
						for (i=0;i<result2.length;i++)
						{   var id=result2[i]['id'];
							var val = result2[i]['chapter_name'];
							selOpts += "<option value="+id+">"+val+"</option>";
						}
						$('#editchapter').append(selOpts);
					 }
					 else
					 {
					 $('#editchapter').empty();
					 }
				}     
		   });
	}

});

//* This is get id next function for delete*//
function getfunction3(id)
{         
	var urldel='<?php echo base_url();?>usercon/del_QuestandPrioritie_id';
	var tbl1='question_list'; 
	$.ajax(
    {  
		url: urldel,
		data:{del_id:id},
		type:'post',
		success:function(done)
		{      
		   console.log(done);
		   $('#delete_id').val(done);
		}     
    });
       
}	

$('#delbtn').click(function(){

var delete_id=$('#delete_id').val();
var urldel='<?php echo base_url();?>usercon/del_QuestandPrioritie_id';
$.ajax(
    {  
		url: urldel,
        data:{delete_id:delete_id},
		type:'post',
		success:function(done)
		{  
	        console.log(done);
	        if(done.trim()=='success')
			{ $('.two').text('successfully Delete ');
			  $('.alert-success').show();
		      setTimeout(function(){
			  window.location= "<?php  echo base_url('usercon/questions'); ?>";  }, 
              1000); 
			}
		}     
    });

});


//**This is for Refresh page **//
$('.btn-secondary').click(function(){
location.reload();
});
$('.close').click(function(){
location.reload();
});




</script>


<!--div class="modal-footer border-0">
	<button type="button" class="btn btn-secondary waves-effect text-left" data-dismiss="modal">Cancel</button>
<button type="button" class="btn btn-success waves-effect text-left">Add</button>
</div-->
</div>
</div>
<!-- /view question pop end -->
</div>
</div>								
	<script>
	$(function() {
		$('#myTable').DataTable();
		$(document).ready(function() {
			var table = $('#example').DataTable({
				"columnDefs": [{
					"visible": false,
					"targets": 2
				}],
				"order": [
					[2, 'asc']
				],
				"displayLength": 25,
				"drawCallback": function(settings) {
					var api = this.api();
					var rows = api.rows({
						page: 'current'
					}).nodes();
					var last = null;
					api.column(2, {
						page: 'current'
					}).data().each(function(group, i) {
						if (last !== group) {
							$(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
							last = group;
						}
					});
				}
			});
			// Order by the grouping
			$('#example tbody').on('click', 'tr.group', function() {
				var currentOrder = table.order()[0];
				if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
					table.order([2, 'desc']).draw();
				} else {
					table.order([2, 'asc']).draw();
				}
			});
		});
	});
	$('#example23').DataTable({
		dom: 'Bfrtip',
		buttons: [
			'copy', 'csv', 'excel', 'pdf', 'print'
		]
	});
	$('.buttons-copy, .buttons-csv, .buttons-print, .buttons-pdf, .buttons-excel').addClass('btn btn-primary mr-1');
	</script>   
</body>

</html>





 




