<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="#">
    <title>Multiple Choice Online</title>
    <base href="<?php echo base_url();?>">
    <link href="assets/admin/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/admin/css/perfect-scrollbar.css" rel="stylesheet">
    <link rel="stylesheet" href="assets/admin/css/font-awesome.css"> 
    <link href="assets/admin/assets/plugins/chartist-js/dist/chartist.min.css" rel="stylesheet">
    
    <link href="assets/admin/assets/plugins/c3-master/c3.min.css" rel="stylesheet">
    <link href="assets/admin/css/style.css" rel="stylesheet">
    <link href="assets/admin/css/dashboard1.css" rel="stylesheet">
    <link href="assets/admin/css/default-dark.css" id="theme" rel="stylesheet">

    <!-- datatable css -->
    <link rel="stylesheet" type="text/css" href="assets/admin/assets/plugins/datatables/media/css/dataTables.bootstrap4.css"> 
</head>
<body class="fix-header fix-sidebar card-no-border">

    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label">Multiple Choice Online</p>
        </div>
    </div>
    <div id="main-wrapper">

        <?php include 'header.php'; ?>


        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <?php include 'sidebar.php' ; ?>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
      

        <div class="page-wrapper">
            <div class="container-fluid ">
                <div class="row page-titles">
                    <div class="col-md-12 ">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Reports</li>
                        </ol>
                    </div>
                </div>
     
                <div class="row">
                    <div class="col-md-12">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="card">
                                            <div class="card-body">
                                                <h3 class="card-title m-b-5"><span class="lstick"></span>User Registered</h3>
                                                <div id="sales-overview" class="p-relative" style="height:400px;">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="card">
                                            <div class="card-body">
                                                <h4 class="card-title">Added Questions</h4>
                                                <div>
                                                    <canvas id="chart1" height="150"></canvas>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="card">
                                            <div class="card-body">
                                                <h4 class="card-title">Test Attempted</h4>
                                                <div>
                                                    <canvas id="chart2" height="150"></canvas>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                    </div>
                </div>

            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->

            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <?php include 'footer.php'; ?>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
<?
foreach ($list as  $value)
{

$act[]  = $value['active'];
$inact[]= $value['inactive'];
$del[]  = $value['del_user'];
}
$act2=implode(',', $act);
$inact2=implode(',', $inact);
$del2=implode(',', $del);
$active   = array_map('intval', explode(',', $act2));
$inactive = array_map('intval', explode(',', $inact2));
$del_user = array_map('intval', explode(',', $del2));
?> 
<?
foreach ($addquest as  $value)
{ $add[]= $value; }
$seperate=implode(',', $add);
$quest_series= array_map('intval', explode(',', $seperate));
?> 
<?
foreach ($attempt as  $value)
{ $attem[]= $value; }
$seper=implode(',', $attem);
$attempt_series= array_map('intval', explode(',', $seper));
?> 

    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="assets/admin/js/dashboard1.js"></script>
    <!-- Chart JS -->
    <script src="assets/admin/assets/plugins/Chart.js/chartjs.init.js"></script>
    <script src="assets/admin/assets/plugins/Chart.js/Chart.min.js"></script>
    <!-- Style switcher -->
    <!-- ============================================================== -->
    <script>
    //$(function () {
    $(document).ready(function()
    {
            new Chartist.Line("#sales-overview", 
            {  
                labels: ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'],
                series: 
                [ {meta:"Active",    data: <?php echo json_encode($active); ?>},  //purple
                  {meta:"Inactive ", data: <?php echo json_encode($inactive); ?>},//green
                  {meta:"Delete",    data: <?php echo json_encode($del_user); ?>} //white
                ]
            }, 
            {
                low: 0 ,
                high:400 ,
                showArea: true ,
                fullWidth: true,
                showLine: false,
                chartPadding:30,
                axisX:
                {
                 showLabel: true,
                 divisor: 1,
                 showGrid: false,
                 offset: 50
                },     
                plugins: 
                [ Chartist.plugins.tooltip() ],
                 
                axisY: 
                {
                  onlyInteger: true,
                  showLabel: true,
                  scaleMinSpace: 50, 
                  showGrid: true,
                  offset: 10,
                  labelInterpolationFnc: function(value) {
                  return (value / 100) + 'k'},
                }
            });

        new Chart(document.getElementById("chart1"),
        {
            "type":"line",
            "data":{"labels":["January","February","March","April","May","June","July","Aug","Sep","Oct","Nov","Dec"],
            "datasets":[{ 
                            "label":"My First Dataset",
                            "data":<?php echo json_encode($quest_series); ?>,
                            "fill":false,
                            "borderColor":"rgb(86, 192, 216)",
                            "lineTension":0.1
                        }]
        },"options":{}});


          new Chart(document.getElementById("chart2"),
        {
            "type":"bar",
            "data":{"labels":["January","February","March","April","May","June","July","Aug","Sep","Oct","Nov","Dec"],
            "datasets":[{
                            "label":"My First Dataset",
                            "data":<?php echo json_encode($attempt_series); ?>,
                            "fill":false,
                            "backgroundColor":["rgba(255, 99, 132, 0.2)","rgba(255, 159, 64, 0.2)","rgba(255, 205, 86, 0.2)","rgba(75, 192, 192, 0.2)","rgba(54, 162, 235, 0.2)","rgba(153, 102, 255, 0.2)","rgba(201, 203, 207, 0.2)"],
                            "borderColor":["rgb(239, 83, 80)","rgb(255, 159, 64)","rgb(255, 178, 43)","rgb(86, 192, 216)","rgb(57, 139, 247)","rgb(153, 102, 255)","rgb(201, 203, 207)"],
                            "borderWidth":1}
                        ]},
            "options":{
                "scales":{"yAxes":[{"ticks":{"beginAtZero":true}}]}
            }
        });
    });
    </script> 
</body>
</html>