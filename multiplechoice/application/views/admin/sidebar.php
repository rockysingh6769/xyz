<aside class="left-sidebar">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                        <!--li class="user-profile"> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><img src="assets/images/users/profile.png" alt="user" /><span class="hide-menu">Steave Jobs </span></a>
                            <ul aria-expanded="false" class="collapse">
                             <li><a href="javascript:void()">My Profile </a></li> 
                                <li><a href="profile.php">Profile</a></li>
                                <li><a href="javascript:void()">Logout</a></li>
                            </ul>
                        </li-->
                        <base href="<?php echo base_url();?>">
                        <li class="nav-devider"></li>

                        <li class="nav-small-cap">PERSONAL</li>

                        <li> <a class="waves-effect waves-dark" href="usercon/dashboard" aria-expanded="false"><i class="mdi mdi-gauge"></i><span class="hide-menu">Dashboard</a>
                        </li>

                        <li> <a class="waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-file-tree"></i><span class="hide-menu">Category Filters</a>
                           
                           <ul aria-expanded="false" class="collapse">
                            <li><a href="usercon/addfilter_country">Country</a></li>
                            <li><a href="usercon/state_province">State/Province</a></li>
                            <li><a href="usercon/course">Course</a></li>
                            <li><a href="usercon/grade_level">Grade/Level</a></li>
                            <li><a href="usercon/year">Year</a></li>
                            <li><a href="usercon/subject">Subject</a></li>
                            <li><a href="usercon/chapter">Chapter</a></li>								

                        </ul>							
                    </li>

                    <li> <a class="waves-effect waves-dark" href="usercon/Questions" aria-expanded="false"><i class="mdi mdi-network-question"></i><span class="hide-menu">Questions</a>
                    </li>

                    <li> <a class="waves-effect waves-dark" href="usercon/users" aria-expanded="false"><i class="mdi mdi-account-multiple"></i><span class="hide-menu">Users</a>
                    </li>
                    <li> <a class="waves-effect waves-dark" href="usercon/reports" aria-expanded="false"><i class="mdi mdi-chart-areaspline"></i><span class="hide-menu">Reports</a></li>
                    <li>
                        <a class="waves-effect waves-dark" href="usercon/subscribe" aria-expanded="false"><i class="mdi-briefcase-check mdi"></i><span class="hide-menu">Subscription</a>
                    </li>


                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </aside>