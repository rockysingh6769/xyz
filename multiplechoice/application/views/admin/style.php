<style type="text/css">
label.icn-profl i {
    width: 30px;
    height: 30px;
    background: #ffffff;
    line-height: 26px;
    color: #33d085;
    border-radius: 50%;
    position: absolute;
    bottom: 25px;
    font-size: 17px;
    right: 0px;
    border: 2px solid #33d085;
    cursor: pointer;
}
label.icn-profl input {
    visibility: hidden;
    height: 0px;
    width: 0px;
}.icn-edit {
    display: block;
    width: 100px;
    margin: 0 auto;
    text-align: center;
    position: relative;
}
.alert-warning,.alert-danger,.alert-success
{ 
    display: none;
    position: fixed;
    max-width: 50%; 
    width: 100%; 
    margin: auto;   
    top: 45%;   left: 16%;    
    z-index: 111111;     
    box-shadow: 1px 1px 8px rgba(0, 0, 0, 0.15);     
    text-align: center;     
    right: 0;    
} 
.select_title{ margin:0px 0px 0px 220px; }
.error       { color: red    !important; }
.status      { color:#545151 !important; }
.country     { color:#545151 !important; }
.year        { color:#545151 !important; }
.subject     { color:#545151 !important; }
.course      { color:#545151 !important; }
</style>