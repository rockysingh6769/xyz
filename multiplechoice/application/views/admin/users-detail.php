<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="#">
    <title>Multiple Choice Online</title>
    <base href="<?php echo base_url();?>">
    <link href="assets/admin/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/admin/css/perfect-scrollbar.css" rel="stylesheet">
    <link rel="stylesheet" href="assets/admin/css/font-awesome.css"> 
    <link href="assets/admin/assets/plugins/chartist-js/dist/chartist.min.css" rel="stylesheet">
    <link href="assets/admin/assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css" rel="stylesheet">
    <link href="assets/admin/assets/plugins/c3-master/c3.min.css" rel="stylesheet">
    <link href="assets/admin/css/style.css" rel="stylesheet">
    <link href="assets/admin/css/dashboard1.css" rel="stylesheet">
    <link href="assets/admin/css/default-dark.css" id="theme" rel="stylesheet">

    <!-- datatable css -->
    <link rel="stylesheet" type="text/css" href="assets/admin/assets/plugins/datatables/media/css/dataTables.bootstrap4.css"> 

</head>

<body class="fix-header fix-sidebar card-no-border">

    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label">Multiple Choice Online</p>
        </div>
    </div>

    <div id="main-wrapper">

        <?php include 'header.php'; ?>


        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <?php include 'sidebar.php' ; ?>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            
            <div class="container-fluid ">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
 
                    <div class="col-md-12 ">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Users Detail</li>
                        </ol>
                    </div>
                    <!-- <div class=""> -->
                        <!-- <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button> -->
                    <!-- </div> -->
                </div>
                     

                <!-- ============================================================== -->
                <!-- Projects of the month -->
                <!-- ============================================================== -->
               
                    <div class="row">
                    <!-- Column -->
                    <div class="col-lg-4 col-xlg-3 col-md-5">
                        <div class="card">
                            <div class="card-body bg-info ">
                               <?php $path= base_url();?>
                                <center class="m-t-30">
                                    <?php if(!empty($data[0][0]->imgpath)){ echo "<img src=".$path.'assets/user/images/upload/'.$data[0][0]->imgpath." class='img-circle' height='150' width='150'>";
                                        }else echo  "<img src=".'assets/user/images/user1.jpg'."  height='150' class='img-circle' width='150'>" ;
                                          ?>
                                     <br><br><h4><?php echo $data[0][0]->firstname.' '.$data[0]
                                     [0]->lastname;?></h4>
                                    <h6><?php  echo $data[0][0]->email;?></h6>
                              </center>
                            </div>
                            <div>
                                 </div>

                        </div>
                    </div>
                    <!-- Column -->

					
 <div class="col-lg-8 col-xlg-9 col-md-7">
                        <div class="card">
                            <!-- Nav tabs -->
                            <!--ul class="nav nav-tabs profile-tab" role="tablist">
                                <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#home" role="tab">Details</a> </li>
                                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#profile" role="tab">Questions</a> </li>
                            
                            </ul-->
                            <!-- Tab panes -->
                         
                                <div class="tab-pane">
                                    <div class="card-body pt-4 pb-3">
						<div class="container-fluid">
						<div class="row">
                    <div class="col-lg-6 col-md-6 mt-1">						
                        <span>Name</span>
					
						<h4 class="bg-light py-2 pl-2"><?php echo $data[0][0]->firstname.' '.$data[0][0]->lastname; ?></h4>
						</div>
                    <div class="col-lg-6 col-md-6 mt-1">						
                        <span>Phone No</span>
					
						<h4 class="bg-light py-2 pl-2"><?php echo $data[0][0]->mobile_no; ?></h4>
						</div>
                    <div class="col-lg-6 col-md-6 mt-1">						
                        <span>DOB</span>
					
						<h4 class="bg-light py-2 pl-2"><?php echo $data[0][0]->dob; ?></h4>
						</div>
                    <div class="col-lg-6 col-md-6 mt-1">						
                        <span>Country</span>
					  

                            <?php  foreach ($data[0] as $key => $value) {
                            foreach ($data[1] as $key1 => $value1) {
                              if($value1['id']==$value->country_id)
                               { $val = $value1['country_name']; } } }
                              ?>
                        <h4 class="bg-light py-2 pl-2"><?php if(empty($val)){ echo "Default";}
                        else{  echo $val; }?></h4>
						</div>						
                    <div class="col-lg-12 col-md-12 mt-1">						
                        <span>Email Address</span>
					
						<h4 class="bg-light  py-2 pl-2"><?php echo $data[0][0]->email; ?></h4>
						</div>

                 				           


</div>

                        </div>
                                    </div>
                                </div>
                                <!--second tab-->
                                <!--div class="tab-pane" id="profile" role="tabpanel">
                                    <div class="card-body">
<div class="box-inn-sp">
                    <div class="row">
                        <div class="col-md-12">

                            <div class="tab-inn detail-page">

                                <h3>Q1. <span>Where did the real Boston Tea Party take place?</span></h3>
                                <div class="row ans-options">
                                    <div class="col-md-6 relative">
                                        <p class="correct">New york <i class="fa fa-check" aria-hidden="true"></i></p>
                                    </div>
                                    <div class="col-md-6 relative">
                                        <p>Washington dc</p>
                                    </div>
                                    <div class="col-md-6 relative">
                                        <p>Boston</p>
                                    </div>
                                    <div class="col-md-6 relative">
                                        <p>Philadelphia</p>
                                    </div>
                                </div>


                            </div>

                        </div>
                        <div class="col-md-12">

                            <div class="tab-inn detail-page">

                                <h3>Q2. <span>You are participating in a race. You've just passed the second person. What position are you now in?</span></h3>
                                <div class="row ans-options">
                                    <div class="col-md-6 relative">
                                        <p>1st</p>
                                    </div>
                                    <div class="col-md-6 relative">
                                        <p>2nd</p>
                                    </div>
                                    <div class="col-md-6 relative">
                                        <p class="correct">3rd <i class="fa fa-check" aria-hidden="true"></i></p>
                                    </div>
                                    <div class="col-md-6 relative">
                                        <p>4th</p>
                                    </div>
                                </div>


                            </div>

                        </div>
                        <div class="col-md-12">

                            <div class="tab-inn detail-page">

                                <h3>Q3. <span>John digs a hole that is 2 yards wide, 3 yards long, and 1 yard deep. How many cubic feet of dirt are in it?</span></h3>
                                <div class="row ans-options">
                                    <div class="col-md-6 relative">
                                        <p>0</p>
                                    </div>
                                    <div class="col-md-6 relative">
                                        <p class="wrong">2
                                            <span class="ansreport">
                                                <i class="fa fa-times" aria-hidden="true"></i> Wrong Answer
                                            </span>
                                        </p>
                                    </div>
                                    <div class="col-md-6 relative">
                                        <p class="correct">3 <span class="ansreport"><i class="fa fa-check" aria-hidden="true"></i> Correct Answer</span></p>
                                    </div>
                                    <div class="col-md-6 relative">
                                        <p>5</p>
                                    </div>
                                </div>


                            </div>

                        </div>

                        <div class="col-md-12">

                            <div class="tab-inn detail-page">

                                <h3>Q4. <span>Where did the real Boston Tea Party take place?</span></h3>
                                <div class="row ans-options">
                                    <div class="col-md-6 relative">
                                        <p class="correct">New york <i class="fa fa-check" aria-hidden="true"></i></p>
                                    </div>
                                    <div class="col-md-6 relative">
                                        <p>Washington dc</p>
                                    </div>
                                    <div class="col-md-6 relative">
                                        <p>Boston</p>
                                    </div>
                                    <div class="col-md-6 relative">
                                        <p>Philadelphia</p>
                                    </div>
                                </div>


                            </div>

                        </div>
                        <div class="col-md-12">

                            <div class="tab-inn detail-page">

                                <h3>Q5. <span>John digs a hole that is 2 yards wide, 3 yards long, and 1 yard deep. How many cubic feet of dirt are in it?</span></h3>
                                <div class="row ans-options">
                                    <div class="col-md-6 relative">
                                        <p>0</p>
                                    </div>
                                    <div class="col-md-6 relative">
                                        <p class="correct">2 
                                            <span class="ansreport">
                                                <i class="fa fa-check" aria-hidden="true"></i> Correct Answer
                                            </span>
                                        </p>
                                    </div>
                                    <div class="col-md-6 relative">
                                        <p class="wrong">3
                                            <span class="ansreport">
                                                <i class="fa fa-times" aria-hidden="true"></i> Wrong Answer
                                            </span>
                                        </p>
                                    </div>
                                    <div class="col-md-6 relative">
                                        <p>5</p>
                                    </div>
                                </div>


                            </div>

                        </div>

                        <div class="col-md-12">

                            <div class="tab-inn detail-page">

                                <h3>Q6. <span>Where did the real Boston Tea Party take place?</span></h3>
                                <div class="row ans-options">
                                    <div class="col-md-6 relative">
                                        <p class="correct">New york <i class="fa fa-check" aria-hidden="true"></i></p>
                                    </div>
                                    <div class="col-md-6 relative">
                                        <p>Washington dc</p>
                                    </div>
                                    <div class="col-md-6 relative">
                                        <p>Boston</p>
                                    </div>
                                    <div class="col-md-6 relative">
                                        <p>Philadelphia</p>
                                    </div>
                                </div>

                            </div>

                        </div>

                    </div>
                </div>
                                    </div>
                                </div-->
                          
                        </div>
                    </div>					
					
					
					
					
                    <!-- Column -->

                    </div>

            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->

            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <?php include 'footer.php'; ?>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script>
    $(function() {
        $('#myTable').DataTable();
        $(document).ready(function() {
            var table = $('#example').DataTable({
                "columnDefs": [{
                    "visible": false,
                    "targets": 2
                }],
                "order": [
                    [2, 'asc']
                ],
                "displayLength": 25,
                "drawCallback": function(settings) {
                    var api = this.api();
                    var rows = api.rows({
                        page: 'current'
                    }).nodes();
                    var last = null;
                    api.column(2, {
                        page: 'current'
                    }).data().each(function(group, i) {
                        if (last !== group) {
                            $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
                            last = group;
                        }
                    });
                }
            });
            // Order by the grouping
            $('#example tbody').on('click', 'tr.group', function() {
                var currentOrder = table.order()[0];
                if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                    table.order([2, 'desc']).draw();
                } else {
                    table.order([2, 'asc']).draw();
                }
            });
        });
    });
    $('#example23').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });
    $('.buttons-copy, .buttons-csv, .buttons-print, .buttons-pdf, .buttons-excel').addClass('btn btn-primary mr-1');
    </script>   
</body>
</html>