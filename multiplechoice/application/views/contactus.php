<!DOCTYPE html>
<html lang="en" class="no-js">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Contact Us</title>
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta name="author" content="" />
    <base href="<?php echo base_url();?>">
    <link rel="shortcut icon" href="assets/front/images/favicon.ico">
    <link rel="stylesheet" href="assets/front/css/magnific-popup.css">
    <link href="assets/front/css/owl.carousel.css" rel="stylesheet">
    <link href="assets/front/css/owl.theme.css" rel="stylesheet">
    <link href="assets/front/css/owl.transitions.css" rel="stylesheet">
    <link href="assets/front/css/mobiriseicons.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="assets/front/css/materialdesignicons.min.css" />
    <link rel="stylesheet" type="text/css" href="assets/front/css/bootstrap.min.css" />
    <link href="assets/front/css/style.css" rel="stylesheet">
</head>

    <body>

        <!-- START NAVBAR -->
        <nav class="contactnav navbar navbar-expand-lg fixed-top custom_nav_menu sticky">
            <div class="container">
                <!-- LOGO -->
                <h3 class="navbar-brand logo text_custom">
                   Multiple Choice Online
                </h3>

                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="mdi mdi-menu"></i>
                </button>
                <div class="collapse navbar-collapse" id="navbarCollapse">
                    <ul class="navbar-nav mx-auto">
                        <li class="nav-item">
                            <a href="index.html" class="nav-link">Home</a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link">Online Tests</a>
                        </li>
                        <li class="nav-item">
                            <a href="aboutus.html" class="nav-link">About Us</a>
                        </li>
                        <li class="nav-item active">
                            <a href="contactus.html" class="nav-link">Contact Us</a>
                        </li>
                     
                        <li class="nav-item">
                            <a href="pricing.html" class="nav-link">Pricing</a>
                        </li>
                        <li class="nav-item">
                            <a href="faq.html" class="nav-link">Faq</a>
                        </li>
                <li class="nav-item">
                    <a href="referral.html" class="nav-link">Referral</a>
                </li>
             
                    </ul>
                    <a href="login.html" class="btn_custom btn btn_small text-capitalize navbar-btn">Login / Sign Up</a>
                </div>
            </div>
        </nav>
        <!-- END NAVBAR -->



	<?php $path= base_url();?>	
    <div class="contactslider slider-area">
        <div class="page-title">
            <div class="single-slider slider-height slider-height-breadcrumb d-flex align-items-center" style="background-image: url(img/bg/others_bg.jpg');">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="slider-content slider-content-breadcrumb text-center">
                                <h1 class="white-color f-700">Contact Us</h1>
                                <nav class="text-center" aria-label="breadcrumb">
                                    <ol class="breadcrumb justify-content-center">
                                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                                        <li class="breadcrumb-item active" aria-current="page">Contact Us</li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>		
		
		
		
		
		
     <div class="advisors-area  pt-95 pb-70">
        <div class="container">
            <div class="row">
                <div class="col-xl-5 col-lg-6 col-md-10 offset-md-1 ml-md-auto">
                    <div class="contact-info-text">
                        <div class="section-title mb-20">
                            <div class="section-title-heading mb-10">
                                <h1>Contact Info</h1>
                            </div>
                            <div class="section-title-para">
                                <p>Lorem ipsum dolor sit amet, consecte adipisicing elit sed do eiusmod tempor incididunt.</p>
                            </div>
                        </div>
                    </div>
					
					

					
					
                    <div class="contact-info mb-50 wow fadeInRight" data-wow-delay=".3s" style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInRight;">
                        <ul>
                            <li>
                                <div class="contact-icon">
                                   <i class="mbri-mobile2 text_custom"></i>
                                </div>
                                <div class="contact-text">
                                    <h5>Call Us</h5>
                                    <span>+9850043456</span>
                                </div>
                            </li>
                            <li>
                                <div class="contact-icon">
                                  <i class="mbri-letter text_custom"></i>
                                </div>
                                <div class="contact-text">
                                    <h5>Email Us</h5>
                                    <span>exmaple@gmail.com</span>
                                </div>
                            </li>
                            <li>
                                <div class="contact-icon">
                                  <i class="mbri-pin text_custom"></i>
                                </div>
                                <div class="contact-text">
                                    <h5>Location</h5>
                                    <span>9592 Lorem Ipsum</span>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-xl-7 col-lg-6 col-md-10 offset-md-1 ml-md-auto">
                    <div class="events-details-form faq-area-form mb-30 p-0">
                        <form>
                            <div class="row">
                                <div class="col-xl-8">
                                    <div class="events-form-title mb-25">
                                        <h2>Get In Touch</h2>
                                    </div>
                                </div>
                                <div class="col-xl-6 col-lg-6 col-md-6">
                                    <input placeholder="Name :" type="text">
                                </div>
                                <div class="col-xl-6 col-lg-6 col-md-6">
                                    <input placeholder="Email :" type="text">
                                </div>
                                <div class="col-xl-6 col-lg-6 col-md-6">
                                    <input placeholder="Subject :" type="text">
                                </div>
                                <div class="col-xl-6 col-lg-6 col-md-6">
                                    <input placeholder="Phone :" type="text">
                                </div>
                                <div class="col-xl-12">
                                    <textarea cols="30" rows="10" placeholder="Message :"></textarea>
                                </div>
                                <div class="col-xl-12">
                                    <div class="faq-form-btn events-form-btn">
                                        <button class="btn m-0">submit now</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
     
	<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d27443.76230988292!2d76.67573418955078!3d30.705176750000014!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sin!4v1539340492305" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
	
	
        <!-- Start Footer -->
        <footer class="footer_detail">
            <div class="container">
                <div class="row pt-5 pb-5">
                    <div class="col-lg-12">
                        <div class="text-center footer_about">
                            <h3 class="text_custom">Multiple Choice Online</h3>
                            <p class="mx-auto mt-3 mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation </p>
                        </div>
                        <div class="text-center subcribe-newslatter mt-5">
                            <form class="mx-auto position-relative">
                                <input type="email" placeholder="Enter Your Email" required="">
                                <button type="submit" class="btn btn_custom">Subcribe</button>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="fot_bor"></div>
                <div class="row pt-3 pb-3">
                    <div class="col-lg-12">
                        <div class=" text-center">
                            <p class="text-white mb-0">2018 &copy; Multiple Choice Online. Design by Indi IT Solutions.</p>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- End Footer -->

        <!-- Back To Top Up Arrow Start -->
        <a href="#" class="back_top_angle_up" style="display: inline;"> 
            <i class="mbri-arrow-up"> </i>
        </a>
        <!-- Back To Top Up Arrow End -->

        <!-- JAVASCRIPTS -->

        <script src="<?php echo $path.'assets/front/js/jquery.min.js';?>"></script>
        <script src="<?php echo $path.'assets/front/js/popper.min.js';?>"></script>
        <script src="<?php echo $path.'assets/front/js/bootstrap.min.js';?>"></script>
        <!--TESTISLIDER JS-->
        <script src="<?php echo $path.'assets/front/js/owl.carousel.min.js';?>"></script>
        <script src="<?php echo $path.'assets/front/js/masonry.pkgd.min.js';?>"></script>
        <script src="<?php echo $path.'assets/front/js/jquery.magnific-popup.min.js';?>"></script>

        <script src="<?php echo $path.'assets/front/js/jquery.easing.min.js';?>"></script>
        <script src="<?php echo $path.'assets/front/js/custom.js';?>"></script>
    </body>
</html>