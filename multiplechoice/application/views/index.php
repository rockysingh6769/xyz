<!DOCTYPE html>
<html lang="en" class="no-js">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Index</title>
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta name="author" content="" />
    <base href="<?php echo base_url();?>">
    <link rel="shortcut icon" href="assets/front/images/favicon.ico">
    <link rel="stylesheet" href="assets/front/css/magnific-popup.css">
    <link href="assets/front/css/owl.carousel.css" rel="stylesheet">
    <link href="assets/front/css/owl.theme.css" rel="stylesheet">
    <link href="assets/front/css/owl.transitions.css" rel="stylesheet">
    <link href="assets/front/css/mobiriseicons.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="assets/front/css/materialdesignicons.min.css" />
    <link rel="stylesheet" type="text/css" href="assets/front/css/bootstrap.min.css" />
    <link href="assets/front/css/style.css" rel="stylesheet">
</head>

    <body>

        <!-- START NAVBAR -->
        <nav class="navbar navbar-expand-lg fixed-top custom_nav_menu sticky">
            <div class="container">
                <!-- LOGO -->
                <h3 class="navbar-brand logo text_custom">
                   Multiple Choice Online
                </h3>

                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="mdi mdi-menu"></i>
                </button>
                <div class="collapse navbar-collapse" id="navbarCollapse">
                    <ul class="navbar-nav mx-auto">
                        <li class="nav-item active">
                           <a href="<?php echo base_url();?>" class="nav-link">Home</a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link">Online Tests</a>
                        </li>
                        <li class="nav-item">
                            <a href="<?php echo base_url().'index.php/usercon/front_aboutus';?>" class="nav-link">About Us</a>
                        </li>
                        <li class="nav-item">
                            <a href="<?php echo base_url().'index.php/usercon/front_contactus';?>" class="nav-link">Contact Us</a>
                        </li>
                     
                        <li class="nav-item">
                            <a href="pricing.html" class="nav-link">Pricing</a>
                        </li>
                        <li class="nav-item">
                            <a href="faq.html" class="nav-link">Faq</a>
                        </li>
                <li class="nav-item">
                    <a href="referral.html" class="nav-link">Referral</a>
                </li>
             
                    </ul>
                    <?php if($this->session->userdata('id'))
                    { 
                      echo '<a href=" index.php/usercon/frontlogin" class="btn_custom btn btn_small text-capitalize navbar-btn">Profile</a><a href=" index.php/usercon/logout" class="btn_custom btn btn_small text-capitalize navbar-btn">Logout</a>'; 
                    }
                   else echo '<a href=" index.php/usercon/frontlogin" class="btn_custom btn  btn_small text-capitalize navbar-btn">Login</a><a href=" index.php/usercon/frontregister" class="btn_custom btn btn_small text-capitalize navbar-btn">Sign Up</a>'  ?>
                   
                   
                </div>
            </div>
        </nav>
        <!-- END NAVBAR -->

        <!-- Home Section Start-->
        <section class="bg_home_lan_img full_height_100vh_home" id="home">
            <div class="bg_overlay_cover_on"></div>
            <div class="home_table_cell">
                <div class="home_table_cell_center">
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-lg-12">
                                <div class="text-center">
                            
                                    <h1 class="home_title text-white mx-auto text-capitalize mb-0 pt-4"> We provides a <i class="text_custom">  platform </i>to take tests, learn, and share knowledge.</h1>
                                    <div class="home_text_details">
                                        <p class="home_subtitle mt-4 mx-auto mb-0">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua dolor sit amet.</p>
                                    </div>

                                    <div class="home_btn_manage mt-4 pt-3">
                                        <a href="#" class="btn btn_custom btn-rounded mr-3">sign up for free</a>
                                       
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Home Section End-->

        <!-- Features Start -->
        <section class="section_all" id="features">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-12">
                        <div class="section_heading text-center">
                            <h3 class="all_section_heading">How It Works</h3>
                            <div><i class="mbri-android all_section_icons  text_custom font-weight-bold"></i></div>
                            <p class="text-muted pt-2 all_section_heading_details text-center mx-auto">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                        </div>
                    </div>
                </div>

                <div class="row mt-5 vertical_content_manage">
                    <div class="col-lg-6">
                        <div class="features_box_content mt-3">
                            
                            <h4 class="font-weight-bold text-capitalize features_box_title_top">Take Tests</h4>
                            <div class="features_border_top"></div>
                            <p class="text-muted features_box_subtitle mt-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p>
                            <p class="mb-0 text-muted"><i class="mdi mdi-arrow-right-bold-circle-outline text_custom"></i>  Lorem Ipsum has been the industry's</p>
                            <p class="mb-0 text-muted"><i class="mdi mdi-arrow-right-bold-circle-outline text_custom"></i> Standard dummy text ever </p>
                            <p class="mb-0 text-muted"><i class="mdi mdi-arrow-right-bold-circle-outline text_custom"></i> Lorem ipsum dolor sit amet, consectetur</p>

                     
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="mt-3">

                            <?php $path= base_url();?>

                            <img src="<?php echo  $path.'assets/front/images/features_2.png';?>" class="img-fluid mx-auto d-block" alt="" title="features-1">
                        </div>
                    </div>
                </div>
                <div class="row mt-5 vertical_content_manage">
                    <div class="col-lg-6">
                        <div class="mt-3">
                            <img src="<?php echo $path.'assets/front/images/features_1.png';?>" class="img-fluid mx-auto d-block" alt="" title="features-2">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="features_box_content mt-3">
                            
                            <h4 class="font-weight-bold text-capitalize features_box_title_top">View Performance</h4>
                            <div class="features_border_top"></div>
                            <p class="text-muted features_box_subtitle mt-3">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words</p>
                            <p class="mb-0 text-muted"><i class="mdi mdi-arrow-right-bold-circle-outline text_custom"></i> You need to be sure</p>
                            <p class="mb-0 text-muted"><i class="mdi mdi-arrow-right-bold-circle-outline text_custom"></i> There isn't anything embarrassing</p>
                            <p class="mb-0 text-muted"><i class="mdi mdi-arrow-right-bold-circle-outline text_custom"></i> Hidden in the middle of text</p>

                           

                        </div>
                    </div>

                </div>

                <div class="row mt-5 vertical_content_manage">
                    <div class="col-lg-6">
                        <div class="features_box_content mt-3">
                            
                            <h4 class="font-weight-bold text-capitalize features_box_title_top">Share Knowledge</h4>
                            <div class="features_border_top"></div>
                            <p class="text-muted features_box_subtitle mt-3">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi</p>
                            <p class="mb-0 text-muted"><i class="mdi mdi-arrow-right-bold-circle-outline text_custom"></i>  Architecto beatae vitae dicta</p>
                            <p class="mb-0 text-muted"><i class="mdi mdi-arrow-right-bold-circle-outline text_custom"></i> Nemo enim ipsam voluptatem quia</p>
                            <p class="mb-0 text-muted"><i class="mdi mdi-arrow-right-bold-circle-outline text_custom"></i> Voluptas sit aspernatur aut odit aut fugit</p>

                  
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="mt-3">
                            <img src="<?php echo $path.'assets/front/images/features_3.png';?>" class="img-fluid mx-auto d-block" alt="" title="features-3">
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Features End -->

        <!-- Start Services -->
        <section class="section_all bg-light" id="services">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-12">
                        <div class="section_heading  text-center">
                            <h3 class="all_section_heading">All You Need to Deliver Online Learning</h3>
                            <div><i class="mbri-magic-stick all_section_icons  text_custom font-weight-bold"></i></div>
                            <p class="text-muted pt-2 all_section_heading_details text-center mx-auto">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                        </div>
                    </div>
                </div>

                <div class="row mt-5">
                    <div class="col-lg-4">
                        <div class="services_box bg-white text-center p-4 mt-3">
                            <div class="service_icon">
                                <img class="learning-icon" src="<?php echo $path.'assets/front/images/3.png';?>">
                            </div>
                            <div class="service_content mt-4">
                                <h5 class="font-weight-bold">history</h5>
                                <p class="mt-3 text-muted mb-0">quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo </p>
                                <a href="#" class="services_read_more mt-3 d-block">
                                    <i class="mdi mdi-arrow-right text_custom"></i>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4">
                        <div class="services_box bg-white text-center p-4 mt-3">
                            <div class="service_icon">
                                <img class="learning-icon" src="<?php echo $path.'assets/front/images/4.png';?>">
                            </div>
                            <div class="service_content mt-4">
                                <h5 class="font-weight-bold">Science</h5>
                                <p class="mt-3 text-muted mb-0">Duis aute irure dolor in reprehenderit in voluptate velit esse cillum </p>
                                <a href="#" class="services_read_more mt-3 d-block">
                                    <i class="mdi mdi-arrow-right text_custom"></i>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4">
                        <div class="services_box bg-white text-center p-4 mt-3">
                            <div class="service_icon">
                               <img class="learning-icon" src="<?php echo $path.'assets/front/images/2.png';?>">
                            </div>
                            <div class="service_content mt-4">
                                <h5 class="font-weight-bold">Mathematics</h5>
                                <p class="mt-3 text-muted mb-0">Excepteur sint occaecat cupidatat non proident, sunt in culpa </p>
                                <a href="#" class="services_read_more mt-3 d-block">
                                    <i class="mdi mdi-arrow-right text_custom"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

              
                
                
            </div>
        </section>
        <!-- End Services -->

        <!-- Start Cta -->
        <section class="section_all bg_video_img_cover">
            <div class="bg_overlay_cover_on"></div>
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-12">
                        <div class="cta-info  text-center">
                            <h3 class=" text-white text-capitalize font-weight-bold">Enjoy Two hours Of Access Every Week</h3>
                            <p class="cta_max_width mx-auto mt-3 text-white">Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                            <div class="text-center mt-4 pt-3">
                                <a href="#" class="btn btn_custom">Get Started</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- End Cta -->

        <!-- Video Section End -->

        <!-- Clients Start -->
     <section class="section_all bg-light" id="clients">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="section_heading text-center">
                            <h3 class="all_section_heading">Testimonials</h3>
                            <div><i class="mbri-user all_section_icons  text_custom font-weight-bold"></i></div>
                            <p class="text-muted pt-2 all_section_heading_details text-center mx-auto">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                        </div>
                    </div>
                </div>

                <div class="row mt-5">
                    <div class="col-lg-12">
                        <div id="owl-slider" class="owl-carousel mt-3">
                                        <div class="business_client_box  m-2 bg-white p-4">
                                            <div class="client_box_img mb-3">
                                                <img src="<?php echo $path.'assets/front/images/client/client-1.jpg';?>" alt="" class="img-fluid rounded-circle mx-auto d-block">
                                            </div>
                                            <div class="clients_name text-center">
                                                <h5 class="mb-0 font-weight-bold">Ronald Hall</h5>
                                                <small class="text-muted">History Student</small>
                                            </div>
                                            <p class="pt-3 text-muted text-center mx-auto"> Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt</p>
                                        </div>

                                            <div class="business_client_box  m-2 bg-white p-4">
                                                <div class="client_box_img mb-3">
                                                    <img src="<?php echo $path.'assets/front/images/client/client-2.jpg';?>" alt="" class="img-fluid rounded-circle mx-auto d-block">
                                                </div>
                                                <div class="clients_name text-center">
                                                    <h5 class="mb-0 font-weight-bold">Joseph Jorgensen</h5>
                                                    <small class="text-muted">History Student</small>
                                                </div>
                                                <p class="pt-3 text-muted text-center mx-auto"> Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt </p>
                                            </div>

                                        <div class="business_client_box  m-2 bg-white p-4">
                                            <div class="client_box_img mb-3">
                                                <img src="<?php echo $path.'assets/front/images/client/client-3.jpg';?>" alt="" class="img-fluid rounded-circle mx-auto d-block">
                                            </div>
                                            <div class="clients_name text-center">
                                                <h5 class="mb-0 font-weight-bold">Alan Mincey</h5>
                                                <small class="text-muted">History Student</small>
                                            </div>
                                            <p class="pt-3 text-muted text-center mx-auto"> Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt </p>
                                        </div>
                        </div>
                    </div>
                </div>

            </div>
        </section>

        <section class="section_all" id="contact">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="section_heading text-center">
                            <h3 class="all_section_heading">Get In Touch</h3>
                            <div><i class="mbri-pin all_section_icons  text_custom font-weight-bold"></i></div>
                            <p class="text-muted pt-2 all_section_heading_details text-center mx-auto">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                        </div>
                    </div>
                </div>

                <div class="row vertical_content_manage mt-5">
                    <div class="col-lg-6">
                        <div class="business_form_custom bg-white">
                            <form>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group mt-2">
                                            <input name="name" id="name" type="text" class="form-control" placeholder="Your name..." required="">
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group mt-2">
                                            <input name="email" id="email" type="email" class="form-control" placeholder="Your email..." required="">
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group mt-2">
                                            <input type="text" class="form-control" id="subject" placeholder="Your Subject.." required="">
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group mt-2">
                                            <textarea name="comments" id="comments" rows="4" class="form-control" placeholder="Your message..." required=""></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <input type="submit" class="btn btn_custom" value="Send Message">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="text-center mt-3">
                            <div class="contact_icon">
                                <i class="mbri-mobile2 text_custom"></i>
                            </div>
                            <div class="mt-2">
                                <p class="mb-0 font-weight-bold">Call Us</p>
                                <p class="text-muted">+9850043456</p>
                            </div>
                        </div>
                        <div class="text-center mt-3">
                            <div class="contact_icon">
                                <i class="mbri-letter text_custom"></i>
                            </div>
                            <div class="mt-2">
                                <p class="mb-0 font-weight-bold">For Support Enquiries</p>
                                <p class="text-muted">exmaple@gmail.com</p>
                            </div>
                        </div>
                        <div class="text-center mt-3">
                            <div class="contact_icon">
                                <i class="mbri-pin text_custom"></i>
                            </div>
                            <div class="mt-2">
                                <p class="mb-0 font-weight-bold">Address</p>
                                <p class="text-muted">9592 Lorem Ipsum</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Contact Us End -->

        <!-- Start Footer -->
        <footer class="footer_detail">
            <div class="container">
                <div class="row pt-5 pb-5">
                    <div class="col-lg-12">
                        <div class="text-center footer_about">
                            <h3 class="text_custom">Multiple Choice Online</h3>
                            <p class="mx-auto mt-3 mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation </p>
                        </div>
                        <div class="text-center subcribe-newslatter mt-5">
                            <form class="mx-auto position-relative">
                                <input type="email" placeholder="Enter Your Email" required="">
                                <button type="submit" class="btn btn_custom">Subcribe</button>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="fot_bor"></div>
                <div class="row pt-3 pb-3">
                    <div class="col-lg-12">
                        <div class=" text-center">
                            <p class="text-white mb-0">2018 &copy; Multiple Choice Online. Design by Indi IT Solutions.</p>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- End Footer -->

        <!-- Back To Top Up Arrow Start -->
        <a href="#" class="back_top_angle_up" style="display: inline;"> 
            <i class="mbri-arrow-up"> </i>
        </a>
        <!-- Back To Top Up Arrow End -->

        <!-- JAVASCRIPTS -->
       <script src="<?php  echo $path.'assets/front/js/jquery.min.js';?>"></script>
        <script src="<?php echo $path.'assets/front/js/popper.min.js';?>"></script>
        <script src="<?php echo $path.'assets/front/js/bootstrap.min.js';?>"></script>
        <!--TESTISLIDER JS-->
        <script src="<?php echo $path.'assets/front/js/owl.carousel.min.js';?>"></script>
        <script src="<?php echo $path.'assets/front/js/masonry.pkgd.min.js';?>"></script>
        <script src="<?php echo $path.'assets/front/js/jquery.magnific-popup.min.js';?>"></script>
       <script src="<?php  echo $path.'assets/front/js/jquery.easing.min.js';?>"></script>
        <script src="<?php echo $path.'assets/front/js/custom.js';?>"></script>
    </body>
</html>