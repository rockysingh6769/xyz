<!DOCTYPE html>
<html lang="en" class="no-js">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Login</title>
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta name="author" content="" />
    <base href="<?php echo base_url();?>">
    <link rel="shortcut icon" href="assets/front/images/favicon.ico">
    <link rel="stylesheet" href="assets/front/css/magnific-popup.css">
    <link href="assets/front/css/owl.carousel.css" rel="stylesheet">
    <link href="assets/front/css/owl.theme.css" rel="stylesheet">
    <link href="assets/front/css/owl.transitions.css" rel="stylesheet">
    <link href="assets/front/css/mobiriseicons.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="assets/front/css/materialdesignicons.min.css" />
    <link rel="stylesheet" type="text/css" href="assets/front/css/bootstrap.min.css" />
    <link href="assets/front/css/style.css" rel="stylesheet">
	<link rel="stylesheet" href="assets/front/css/font-awesome.css">
</head>
<style type="text/css">
.error{color:red !important;}
.alert-warning,.alert-success,.alert-info
{ 
	display: none;
	position: fixed;
	max-width: 50%; 
	width: 100%; 
	margin: auto;   
	top: 45%;	left: 16%;    
	z-index: 111111;     
	box-shadow: 1px 1px 8px rgba(0, 0, 0, 0.15);     
	text-align: center;     
	right: 0;    
} 
</style>

<body class="logincolor">
<div class="alert alert-warning">
<strong><center class="one"></center></strong>  
</div>
<div class="alert alert-success">
<strong><center class="two"></center></strong> 
</div>
<div class="alert alert-info">
<strong><center class="three"></center></strong> 
</div>
	
<section class="loginsection">


			<div class="container">
			       
						<div class="row">


										<div class="col-lg-10 col-md-10 col-sm-12 col-12 offset-lg-1 offset-md-1  ">			
                                     <h2 class="mobile-center text-center"><a href="index.html">Multiple Choice Online</a></h2>			
						    <div class="loginsection-in">
						<div class="row">


										<div class="col-lg-6 col-md-6 paddingleft   pr-0">
                                          <div class="login-img">
                                          <?php $path= base_url();?>
										  <img src="<?php echo $path.'assets/front/images/home-bg-3.jpg';?>">
										  <div class="login-textab text-center">
										                  <h3 class="logo text_custom">
														   Multiple Choice Online
														</h3>
														<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut </p>

														<ul class="login-home">
														
														<a href="<?php echo base_url();?>" class="btn btn-success"><i class="fa fa-home" aria-hidden="true"></i> Back To Home</a>
														</ul>
														
														</div>
										  </div> 
										</div>
										<div class="col-lg-6 col-md-6 p-0">
                                          <div class="login-text">
										  <h1>Login</h1>
										  <div class="signmrtp features_border_top ml-3"></div>
										  <p>Please enter your email and password details to access your account.</p>
									<form  method="post" id="myform" action="<?php echo base_url('index.php/usercon/frontlogin')?>">
									 <div class=" col-lg-12">
											<div class="form-group">
											 
											  <input type="text" class="form-control form-control-lg"  name="email" placeholder="Enter Your Email...">
											</div>
                                </div>	
								
									 <div class=" col-lg-12">
											<div class="form-group">
											
											  <input type="password" class="form-control form-control-lg" name="password" placeholder="Enter Your Password...">
											</div>
											

                                </div>
	                                
									 <div class=" col-lg-12 d-flex justify-content-between">
											<div class="boxshadow  custom-control custom-checkbox mb-3">
											  <input type="checkbox" class="custom-control-input" id="customCheck" name="example1">
											  <label class="custom-control-label" for="customCheck">Remember Me</label>
											</div>
											<a href="#"  data-toggle="modal" data-target="#myModal">Forget Password?</a>
									</div>
									 <div class=" col-lg-12">
											    <button type="submit"  class="btn btn-primary loginbtn">Login</button>
												</form>
												<h6 class="mt-3">Don,t have account<a href="<?php echo base_url('index.php/usercon/frontregister')?>"> Signup Here</a>.</h6>
												
														<ul class="facebookgoogle text-center">
<a href="#" class="loginBtn loginBtn--facebook">
  Login with Facebook
</a><br>
<p class="orroundedlog">or</p><br>
<a href="#" class="loginBtn loginBtn--google">
  Login with Google
</a>											</ul>
																										
												
									</div>									
									   
										  </div> 
										</div>
      
                            </div>
						</div>
                   </div>
               </div>
			</div>


</section>	
	
  <!-- The Modal -->
  <div class="loginmodal modal fade" id="myModal">
    <div class="modal-dialog modal-md">
      <div class="modal-content pt-3 pb-3">
      
        <!-- Modal Header -->
        <div class="modal-header border-0">
        <i class="fa fa-lock" aria-hidden="true"></i>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body border-0 text-center">
						<div class="sec-title">
                    
                        <h4 class="font-weight-bold text-capitalize">Forget password</h4>
						<div class="features_border_top ml-auto mr-auto"></div>
                    </div>
					
                          <form  class="mx-auto position-relative">
                                <input type="email" name="resetemail" id="resetemail" placeholder="Enter Email Address" required="">
                                <button type="button" id="resetbtn" class="btn btn_custom">Reset Password</button>
                            </form>					
					
					
					
        </div>
        

        
      </div>
    </div>
  </div>
  	
	

        <!-- JAVASCRIPTS -->
        <script src="<?php echo $path.'assets/front/js/jquery.min.js';?>"></script>
        <script src="<?php echo $path.'assets/front/js/validation.js';?>"></script>
        <script src="<?php echo $path.'assets/front/js/popper.min.js';?>"></script>
        <script src="<?php echo $path.'assets/front/js/bootstrap.min.js';?>"></script>
        <!--TESTISLIDER JS-->
        <script src="<?php echo $path.'assets/front/js/owl.carousel.min.js';?>"></script>
        <script src="<?php echo $path.'assets/front/js/masonry.pkgd.min.js';?>"></script>
        <script src="<?php echo $path.'assets/front/js/jquery.magnific-popup.min.js';?>"></script>
        <script src="<?php echo $path.'assets/front/js/jquery.easing.min.js';?>"></script>
        <script src="<?php echo $path.'assets/front/js/custom.js';?>"></script>
    


<script> 
$('#myform').validate(
{
    rules:
    {   
    	email:   { required:true, email:true},
        password:{ required:true  }
    },
    submitHandler:function(form) 
    {   
       var data = $("#myform input").serialize();
       var url='<?php echo base_url();?>index.php/usercon/frontlogin';
       $.ajax(
       {    url: url,
            data:data,
            type:'post',
            success:function(done)
            {
                 console.log(done);
	             if(done.trim()=='check')
	             {  $('.one').text('Please check your email and password');
	                $('.alert-warning').show();
	                setTimeout(function(){
	                $('.alert-warning').hide();  }, 1000); 
	             }
	             if(done.trim()=='admin_del')
	             {  $('.three').text('your account have been deleted');
	                $('.alert-info').show();
	                setTimeout(function(){
	                window.location.reload();  }, 1000); 
	             }
	             if(done.trim()=='success')
	             {  $('.two').text('successfully login');
	                $('.alert-success').show();
	                setTimeout(function(){
	                window.location= "<?php  echo base_url('index.php/usercon/user'); ?>";  },  1000);
	             }
            }
        });
    }

});

$('#resetbtn').click(function()
{
	   var data=$('#resetemail').val();
	   var url='<?php echo base_url();?>index.php/usercon/forgot';
	   alert(data); 
	   $.ajax(
	   {    url: url,
            data:{email:data},
            type:'post',
            success:function(done)
            {
                 console.log(done);
	             if(done.trim()=='notexist')
	             {  $('.three').text('your email is not exist');
	                $('.alert-info').show();
	                setTimeout(function(){
	                $('.alert-info').hide(); }, 1500); 
	             }
                 

            }
	   
	   });  

});


</script>




    </body>
</html>