<!DOCTYPE html>
<html>
<head>
  <title></title>
<style type="text/css">
form
{
margin: 100px 50px 50px 250px;
} 
#resetbtn{ margin-left: 170px; }
.error { color: red; }
.alert-success
{ 
display: none;
position: fixed;
max-width: 50%; 
width: 100%; 
margin: 20px 50px 50px 250px;   
top: 10%; left: 0%;    
z-index: 111111;     
box-shadow: 1px 1px 8px rgba(0, 0, 0, 0.15);     
text-align: center;     
right: 0;    
} 
</style>
</head>
<body>

<div class="alert alert-success">
<strong><center class="two"></center></strong> 
</div>

<base href="<?php echo base_url();?>">
<link rel="stylesheet" type="text/css" href="assets/front/css/bootstrap.min.css" />

<form method="post" id="myform" >
 <div class="form-row">
  <div class="form-group col-md-3 offset-md-2">
    <input type="password" name="password" id="password" class="form-control"  placeholder="New Password" >
   </div>
 </div>
 <div class="form-row">
  <div class="form-group col-md-3 offset-md-2">
     <input type="password" name="cpassword" class="form-control"  placeholder="Confirm password" >
     <input type="hidden" name="token" value="<?php echo $_GET['token'];?>">
  </div>
 </div>
<button type="submit" id='resetbtn' class="btn btn-primary mb-2">Reset Password</button>

</form>

</body>
</html>



 <!-- JAVASCRIPTS -->
<?php $path=base_url();?>
<script src="<?php echo $path.'assets/front/js/jquery.min.js';?>"></script>
<script src="<?php echo $path.'assets/front/js/validation.js';?>"></script>



<script> 
$('#myform').validate(
{
    rules:
    {   
        password :{ required:true,maxlength:10},
        cpassword:{ required:true,equalTo:"#password"} 
    },
    submitHandler:function(form) 
    {   
       var data = $("#myform input").serialize();
       var url='<?php echo base_url();?>index.php/usercon/resetpass';
       $.ajax(
       {    url: url,
            data:data,
            type:'post',
            success:function(done)
            {
                 console.log(done);
                 if(done.trim()=='success')
                 { $('.two').text('successfully changed password');
                   $('.alert-success').show();
                   setTimeout(function(){
                   window.location= "<?php echo base_url('index.php/usercon/frontlogin'); ?>";  },  1000); 
                  }
               
            }
       });
    }

});



</script>
