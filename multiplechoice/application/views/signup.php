<!DOCTYPE html>
<html lang="en" class="no-js">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>SignUp</title>
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta name="author" content="" />
    <base href="<?php echo base_url();?>">
    <link rel="shortcut icon" href="assets/front/images/favicon.ico">
    <link rel="stylesheet" href="assets/front/css/magnific-popup.css">
    <link href="assets/front/css/owl.carousel.css" rel="stylesheet">
    <link href="assets/front/css/owl.theme.css" rel="stylesheet">
    <link href="assets/front/css/owl.transitions.css" rel="stylesheet">
    <link href="assets/front/css/mobiriseicons.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="assets/front/css/materialdesignicons.min.css" />
    <link rel="stylesheet" type="text/css" href="assets/front/css/bootstrap.min.css" />
    <link href="assets/front/css/style.css" rel="stylesheet">
	<link rel="stylesheet" href="assets/front/css/font-awesome.css">
</head>
<style type="text/css">
.error{color:red !important;}
.alert-warning,.alert-danger,.alert-success
{ 
	display: none;
	position: fixed;
	max-width: 50%; 
	width: 100%; 
	margin: auto;   
	top: 45%;	left: 16%;    
	z-index: 111111;     
	box-shadow: 1px 1px 8px rgba(0, 0, 0, 0.15);     
	text-align: center;     
	right: 0;    
}  
#dob{color:#545151 !important; }
#status{color:#545151 !important; }
#select_data{color:#545151 !important; }     
</style>

<body class="signupbgcolor">
<div class="alert alert-warning">
<strong><center class="one"></center></strong>  
</div>

<div class="alert alert-success">
<strong><center class="two"></center></strong> 
</div>

<div class="alert alert-danger">
<strong><center class="three"></center></strong> 
</div>


	
<section class="signupsection">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-12 ">
<h2 class="mobile-center text-center"><a href="index.html">Multiple Choice Online</a></h2>							
							 <div class="signupinput">
						    <div class="signupsection-in">
						<div class="row">

										<div class="col-lg-4 col-md-5 col-sm-12 paddingleft p-0">
                                   <div class="login-img  imgsignup">
                                   	     <?php $path= base_url();?>
										  <img src="<?php echo $path.'assets/front/images/home-bg-3.jpg';?>">
										  <div class="login-textab text-center">
										                  <h3 class="logo text_custom">
														   Multiple Choice Online
														</h3>
														<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut </p>

														<ul class="login-home">
														
														<a href="<?php echo base_url();?>" class="btn btn-success"><i class="fa fa-home" aria-hidden="true"></i> Back To Home</a>
														</ul>
														
														</div>

										    </div>
							                 </div>
							              <div class="col-lg-8 col-md-7 col-sm-12 col-12 p-0">
										  <div class="signup-text">
										<h4 class="font-weight-bold text-capitalize features_box_title_top">Sign Up</h4>							 
										<div class="signmrtp features_border_top"></div>
										<p>Please enter your email and password details to access your account.</p>
									  <form  method="post" id="myform" action="<?php echo base_url('index.php/usercon/frontregister')?>">
									  <div class="row">
										<div class="form-group col-lg-6">
										  <input type="text" class="form-control form-control-lg" id="fst" name="firstname" placeholder="First Name">
										</div>
										<div class="form-group col-lg-6">
										  <input type="text" class="form-control form-control-lg" id="lst" name="lastname" placeholder="Last Name">
										</div>
										<div class="form-group col-lg-6">
										  <input type="email" class="form-control form-control-lg" id="eml" name="email" placeholder="Email Address">
										</div>
										<div class="form-group col-lg-6">
										  <input type="text" class="form-control form-control-lg" id="phone" name="mobile_no" placeholder="Phone no">
										</div>
										 
										 <div class="form-group col-lg-6">
										  <input type="date" class="form-control form-control-lg" id="dob" name="dob" >
										</div>

										  <div class="form-group col-lg-6">
										    <select  name="status" id="status"  class="form-control" style="height: 47px" > 
                                           <option disabled selected>Select/Status</option>
                                            <option  value="1">Active </option>
                                            <option  value="0">Inactive</option>
                                          </select>  
										</div>
										
										<div class="form-group col-lg-12">
								        <select id="select_data"  name="country_id"   class="form-control" style="height: 47px" > 
                                                       <option selected disabled>Select/country</option>
                                                       <?php foreach($list as $key => $value){ ?>
                                                       <option  value="<?php echo $value['id']?>">
                                                        <?php echo $value['country_name']; ?></option><?php } ?>
                                                    </select> 
											
										</div>	
										
										<div class="form-group col-lg-6">
										  <input type="password" class="form-control $('select#meal').serialize()form-control-lg" id="password" style="height: 47px" name="password" placeholder="Password">
										</div>
										<div class="form-group col-lg-6">
										  <input type="password" class="form-control form-control-lg" id="cnpsd" name="cpassword" placeholder="Confirm Password">
										</div>

												<div class="form-group col-lg-12 mt-3">
                                             <input class="btn signup" type="submit" value="Sign Up">

										 </div>
										 <h6 class="mt-2 pl-3">Already have an account?<a href="<?php echo base_url('index.php/usercon/frontlogin')?>"> Login</a>.</h6>
                                        </div> 										 
									  </form>	
									  </div>
									  </div>
									  </div>
									  </div>
							 
							 </div> 

                    </div>
                 </div>
              </div>
</section>	
	

	

        <!-- JAVASCRIPTS -->
        <script src="<?php echo $path.'assets/front/js/jquery.min.js';?>"></script>
        <script src="<?php echo $path.'assets/front/js/validation.js';?>"></script>
        <script src="<?php echo $path.'assets/front/js/popper.min.js';?>"></script>
        <script src="<?php echo $path.'assets/front/js/bootstrap.min.js';?>"></script>
        <!--TESTISLIDER JS-->
        <script src="<?php echo $path.'assets/front/js/owl.carousel.min.js';?>"></script>
        <script src="<?php echo $path.'assets/front/js/masonry.pkgd.min.js';?>"></script>
        <script src="<?php echo $path.'assets/front/js/jquery.magnific-popup.min.js';?>"></script>
        <script src="<?php echo $path.'assets/front/js/jquery.easing.min.js';?>"></script>
        <script src="<?php echo $path.'assets/front/js/custom.js';?>"></script>
    



<script> 
$('#myform').validate(
{
    rules:
    {
        firstname:{   required:true  },
        lastname: {   required:true  },
        email:    {   required:true, email:true},
        mobile_no:{   required:true  },
        password: {   required:true,maxlength:10},
        status:   {   required:true  },
        dob:      {   required:true  },
        country_id:{   required:true  },
        cpassword:{   required:true,equalTo:"#password"} 
    },
    submitHandler:function(form) 
    {   
       var data = $("#myform").serialize();
       var url='<?php echo base_url();?>index.php/usercon/frontregister';
       $.ajax(
       {    url: url,
            data:data,
            type:'post',
            success:function(done)
            {     
            	 console.log(done);
	             if(done.trim()=='already')
	             {  $('.one').text('This email is already exist');
	                $('.alert-warning').show();
	                setTimeout(function(){
	                $('.alert-warning').hide();  }, 2000); 
	             }
	             
	             if(done.trim()=='success')
	             { $('.two').text('successfully register');
	               $('.alert-success').show();
	               setTimeout(function(){
	               window.location= "<?php  echo base_url('index.php/usercon/frontlogin'); ?>";  },  1000); 
	             }

	             if(done.trim()=='please')
	             { $('.three').text('Please Fill Your Empty Filed');
	               $('.alert-danger').show();
	               setTimeout(function(){
	               $('.alert-danger').hide(); }, 2000); 
	                
	             }
	        }
       
       });
    }

});
</script>



    </body>
</html>