<!DOCTYPE html>
<html lang="en">

<head>
    <title>My Profile</title>
    <!--== META TAGS ==-->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!--== FAV ICON ==-->
    <base href="<?php echo base_url();?>">
    <link rel="shortcut icon" href="assets/user/images/fav.ico">

    <!-- GOOGLE FONTS -->
  
    <!-- FONT-AWESOME ICON CSS -->
  
    <link rel="stylesheet" href="assets/user/css/font-awesome.min.css">

    <!--== ALL CSS FILES ==-->
    <link rel="stylesheet" href="assets/user/css/style.css">
    <link rel="stylesheet" href="assets/user/css/mob.css">
    <link rel="stylesheet" href="assets/user/css/bootstrap.css">
    <link rel="stylesheet" href="assets/user/css/materialize.css" />
    <link rel="stylesheet" href="assets/user/css/dataTables.bootstrap.min.css"/>
    <link href="assets/user/css/bootstrap-glyphicons.css" rel="stylesheet"> 
    <link href="https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900&amp;subset=latin-ext" rel="stylesheet">

</head>
<style type="text/css">
.error{color: red !important;}
.alert-warning,.alert-success
{ 
    display: none;
    position: fixed;
    max-width: 50%; 
    width: 100%; 
    margin: auto;   
    top: 45%;   left: 16%;    
    z-index: 111111;     
    box-shadow: 1px 1px 8px rgba(0, 0, 0, 0.15);     
    text-align: center;     
    right: 0;    
} 
#dob{color:#545151 !important; }
#status{color:#545151 !important; }
#select_data{color:#545151 !important; }

</style>
<body>
<div class="alert alert-warning">
<strong><center class="one"></center></strong>  
</div>
<div class="alert alert-success">
<strong><center class="two"></center></strong> 
</div>
    <!-- header -->
    <?php include 'header.php'; ?>  
    <!-- // header -->

    <!--== BODY CONTNAINER ==-->
    <div class="container-fluid sb2">
        <div class="row">

            <!-- sidemenu -->
            <!--<?php //include 'sidenav.php'; ?>not remove -->
            <!-- // sidemenu -->

            <div class="sb2-2">
                <div class="sb2-2-2">
                    <ul>
                        <li><a href="<?php echo base_url().'index.php/Usercon/userindex';?>"><i class="fa fa-home" aria-hidden="true"></i> Home</a>
                        </li>
                        <li class="active-bre"><a href="#"> Admin User Add</a>
                        </li>
                        
                    </ul>
                </div>		


                <main class="pt-0 pb-0">
                    <div class="main-section trip-details">
                        <section class="companies-info tabs-friend userdetail-topspace">
                            <div class="container-fluid p-0">
                                <div class="row">
                             
                            <div class="col-lg-9">
                                     
                                <ul class="nav nav-tabs " id="myTab" role="tablist">
                                    <li class="nav-item active">
                                        <a class="nav-link" id="home-tab" data-toggle="tab" href="#my-friend" role="tab" aria-controls="my-friend" aria-selected="true" aria-expanded="true"><i class="fa fa-file-text"></i>Personal Details</a>
                                    </li>
                                </ul>
                                <div class="tab-content" id="myTabContent">
                                    <div class="tab-pane fade active in" id="my-friend" role="tabpanel" aria-labelledby="home-tab" aria-expanded="false">
                                        <div class="companies-list userdetail-topspacetab">

                                            <div class="row">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-12">

                                                    <div class="main-ws-sec profiledetail">

                                                 <form method="post" id="myform" action="<?php echo base_url('usercon/userregister')?>">

                                                          <div class="form-group col-lg-6">
                                                            <label for="fstname">First Name</label>
                                                            <input name="firstname" class="form-control" id="fstname"  type="text">
                                                        </div>

                                                        <div class="form-group col-lg-6">
                                                            <label for="lastname">Last Name</label>
                                                            <input  name="lastname" class="form-control" id="lstname" type="text">
                                                        </div>	

                                                        <div class="form-group col-lg-6">
                                                            <label for="phone">Phone No</label>
                                                            <input  name="mobile_no" class="form-control" id="phone"  type="text">
                                                        </div>	

                                                        <div class="form-group col-lg-6">
                                                            <label for="email">Email Address</label>
                                                            <input  name="email" class="form-control" id="email"  type="email">
                                                        </div>	
                                                        <div class="form-group col-lg-6">
                                                            <label for="dob">Date Of Birth</label>
                                                            <input  name="dob" class="form-control" id="dob"  type="date">
                                                        </div>	
                                                         
                                                         <div class="form-group col-lg-6">
                                                              <label>Status</label>
                                                              <select  name="status"   class="form-control" id="status" > 
                                                                <option selected disabled>Select/Status</option>
                                                            <option  value="1">Active </option>
                                                            <option  value="0">Inactive</option>
                                                              </select>  
                                                        </div> 

                                                        <div class="form-group col-lg-12">
                                                            <label>Select Country</label>
                                                    <select id="select_data"  name="country_id"   class="form-control"  > 
                                                        <option selected disabled>Select/country</option>
                                                       <?php foreach($list as $key => $value){ ?>
                                                       <option  value="<?php print_r($value['id'])?>">
                                                        <?php echo $value['country_name']; ?></option><?php } ?>
                                                    </select>  
                                                        </div>
                                                        <div class="form-group col-lg-6">
                                                            <label for="newpassword">New Password</label>
                                                            <input name="password" class="form-control" id="password" type="password">
                                                        </div>

                                                        <div class="form-group col-lg-6">
                                                            <label for="conpassword">Confirm Password</label>
                                                            <input  name="cpassword" class="form-control" id="cpassword" type="password">
                                                        </div>
                                                        <div class="form-group col-lg-12 mt-3">
                                                          <input class="btn btn-primary" type="submit" value="ADD">
                                                         
                                                        </div>
                                                    </form>





                                                </div>

                                            </div>
                                        </div>
                                     
                                        <!--process-comm end-->
                                    </div>

                                </div>
                                
                            </div>




                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--companies-info end-->

    </div>
</main>
  <?php include 'footer.php'; ?>
</div>
</div>
<!--== BOTTOM FLOAT ICON ==-->


<!--======== SCRIPT FILES =========-->
    <script>
    $(function() {
        $('#myTable').DataTable();
        $(document).ready(function() {
            var table = $('#example').DataTable({
                "columnDefs": [{
                    "visible": false,
                    "targets": 2
                }],
                "order": [
                    [2, 'asc']
                ],
                "displayLength": 25,
                "drawCallback": function(settings) {
                    var api = this.api();
                    var rows = api.rows({
                        page: 'current'
                    }).nodes();
                    var last = null;
                    api.column(2, {
                        page: 'current'
                    }).data().each(function(group, i) {
                        if (last !== group) {
                            $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
                            last = group;
                        }
                    });
                }
            });
            // Order by the grouping
            $('#example tbody').on('click', 'tr.group', function() {
                var currentOrder = table.order()[0];
                if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                    table.order([2, 'desc']).draw();
                } else {
                    table.order([2, 'asc']).draw();
                }
            });
        });
    });
    $('#example23').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });
    $('.buttons-copy, .buttons-csv, .buttons-print, .buttons-pdf, .buttons-excel').addClass('btn btn-primary mr-1');
    </script> 

 <script> 
 $(document).ready(function() {
    $('#example').DataTable();
} );

$("#password").keyup(function()
{
    var vl=$(this).val().length;
    if(vl > 0)
    $("#cpassword").attr("required", true); 
    else 
    $("#cpassword").attr("required", false);
});

$("#cpassword").keyup(function()
{ 
    var vl=$(this).val().length;
    if(vl > 0)
    $("#password").attr("required", "true");
    else
    $("#password").attr("required", false);
}); 

$('#myform').validate(
{
    rules:
    {
        firstname:{   required:true  },
        dob:      {   required:true  },
        lastname: {   required:true  },
        mobile_no:{   required:true  },
        email:    {   required:true, email:true},
        address:  {   required:true  },
        password: {   required:true,maxlength:10},
        status:   {   required:true  },
        country_id:{   required:true },
        cpassword:{   required:true, equalTo: "#password"}
        
    },
    submitHandler:function(form) 
    {   
       var data = $("#myform").serialize();
       var url='<?php echo base_url();?>index.php/usercon/adminuser_add';
       $.ajax(
       {    url: url,
            data: data,
            type:'post',
            success:function(done)
            {
                console.log(done);
                if(done.trim()=='already')
                {  $('.one').text('This email is already exist');
                   $('.alert-warning').show();
                   setTimeout(function(){
                   $('.alert-warning').hide();  },1000); 
                }
                 if(done.trim()=='success')
                 { $('.two').text('successfully Add ');
                   $('.alert-success').show();
                   setTimeout(function(){
                   window.location= "<?php  echo base_url('index.php/usercon/users'); ?>";  },  1000); 
                 }
            }
       });
    }

});



</script> 


</body>
</html>