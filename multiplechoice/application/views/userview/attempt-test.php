<!DOCTYPE html>
<html lang="en">
<head>
    <title>Attempt Test</title>
    <!--== META TAGS ==-->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <base href="<?php echo base_url(); ?>">
    <!--== FAV ICON ==-->
    <link rel="shortcut icon" href="assets/user/images/fav.ico">

    <!-- FONT-AWESOME ICON CSS -->
    <link rel="stylesheet" href="assets/user/css/font-awesome.min.css">

    <!--== ALL CSS FILES ==-->
    <link rel="stylesheet" href="assets/user/css/style.css">
    <link rel="stylesheet" href="assets/user/css/mob.css">
    <link rel="stylesheet" href="assets/user/css/bootstrap.css">
    <link rel="stylesheet" href="assets/user/css/materialize.css" />
    <link rel="stylesheet" href="assets/user/css/dataTables.bootstrap.min.css"/>
    <link href="assets/user/css/bootstrap-glyphicons.css" rel="stylesheet"> 
    <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="assets/user/css/jquery-steps.css">
    <link href="https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900&amp;subset=latin-ext" rel="stylesheet">
<style type="text/css">
#question_list
{display: none;} 
.alert-warning,.alert-danger,.alert-success
{ 
    display: none;
    position: fixed;
    max-width: 50%; 
    width: 100%; 
    margin: auto;   
    top: 45%;   left: 16%;    
    z-index: 111111;     
    box-shadow: 1px 1px 8px rgba(0, 0, 0, 0.15);     
    text-align: center;     
    right: 0;    
} 
#footer {  margin-top: 0px; margin-left: 50px; } 
#finbtn {  margin-top: -10px; line-height: 2;  }
#addstate-error   { color:red  !important; }
#addcountry-error { color:red  !important; }
#addcourse-error  { color:red  !important; }
#addgrade-error   { color:red  !important; }
#addyear-error    { color:red  !important; }
#addsubject-error { color:red  !important; }
#addchapter-error { color:red  !important; }
#stabtn           { margin-top: -123px; }
#finbtn           { margin-top: -123px; }
</style>
</head>

<body class="questionpagebgcolor">
<div class="alert alert-warning">
<strong><center class="one"></center></strong>  
</div>

<div class="alert alert-success">
<strong><center class="two"></center></strong> 
</div>

<div class="alert alert-danger">
<strong><center class="three"></center></strong> 
</div>
    <!-- header -->
        <?php include 'header.php'; ?>
    <!-- // header -->

<!--== BODY CONTNAINER ==-->
<div class="container-fluid sb2">
    <div class="row">
        <!-- sidemenu -->
            <?php include 'sidenav.php'; ?>
            <!-- // sidemenu -->

        <!--== BODY INNER CONTAINER ==-->
        <div class="sb2-2">
            <!--== breadcrumbs ==-->
            <div class="sb2-2-2">
                <ul>
                    <li><a href="index.php"><i class="fa fa-home" aria-hidden="true"></i> Home</a>
                    </li>
                    <li class="active-bre"><a href="#">Attempt Test</a>
                    </li>

                </ul>
            </div>
            <!--== DASHBOARD INFO ==-->

            <!--== User Details ==-->
            <div class="sb2-2-3">
                <div class="box-inn-sp questionsection attemptsection">
                    <div class="row">
                        <div class="tab-inn ">
                    <form id="Prioritieform" method="post">
                        <p  class="fontsize-p">Please Fill The Required Details In Order To Proceed</p>
                                <div class="col-md-4">
                                    <div class="form-group lgbtn">
                                        <select id="addcountry"  name="addcountry" class="form-control"> 
                                          <option class="abc" value="">Select/Country</option>
                                          <?php foreach($list[0] as $key => $value){ ?>
                                                <option  value="<?php print_r($value['id'])?>">
                                                <?php echo $value['country_name']; ?></option> 
                                                <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">

                                    <div class="form-group lgbtn">

                                        <select  class="form-control" name="addstate" id="addstate">
                                                <option class="abc" value="">State/Province</option>
                                            </select>
                                    </div>

                                </div>
                                <div class="col-md-4">

                                    <div class="form-group lgbtn">

                                     <select required id="addcourse"  name="addcourse" class="form-control"> 
                                          <option  selected  value="">Select/Course </option>
                                          <?php foreach($list[1] as $key => $value){ ?>
                                                <option  value="<?php print_r($value['id'])?>">
                                                <?php echo $value['course_name']; ?></option> 
                                                <?php } ?>
                                        </select>
                                    </div>

                                </div>
                                <div class="col-md-4">

                                    <div class="form-group lgbtn">
                                        <select required id="addgrade" name="addgrade" class="form-control">    <option selected  value="">Grade/Level</option>
                                                 <?php foreach($list[2] as $key => $value){ ?>
                                                 <option  value="<?php print_r($value['id'])?>">
                                                 <?php echo $value['grade_name']; ?></option> 
                                                <?php } ?>
                                          </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group lgbtn">
                                        <select required id="addyear" name="addyear" class="form-control">        <option  value="">Year/List</option>
                                                 <?php foreach($list[3] as $key => $value){ ?>
                                                 <option  value="<?php print_r($value['id'])?>">
                                                 <?php echo $value['year']; ?></option> 
                                                <?php } ?>
                                            </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group lgbtn">
                                       <select required class="form-control" name="addsubject" id="addsubject">
                                                <option  value="">Subject/List</option>
                                       </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group lgbtn">
                                         <select class="form-control" name="addchapter" id="addchapter">
                                                <option  value="">Chapter/List</option>
                                            </select>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group col-sm-12 mt-1 mb-4">
                                   <input type="submit" class="btn btn-primary" id="processbtn"  
                                    value="Proceed">
                                   <!-- <a href="javascript:void(0);" class="btn btn-primary" data-toggle="collapse" id="processbtn" data-target="#demo" aria-expanded="false">Proceed</a>-->
                                </div>
                             </form>   

                                <div class="col-md-12 alltest">

                                    <div id="demo" class="collapse">
                                        <div class="step-app">
                                           <ul class="step-steps" style="display:none">
                                                <li><a href="#tab1"><span class="number">1</span> </br>Step 1</a></li>
                                                <li><a href="#tab2"><span class="number">2</span></br> Step 2</a></li>
                                                <li><a href="#tab3"><span class="number">3</span></br> Step 3</a></li>
                                                <li><a href="#tab4"><span class="number">4</span></br> Step 4</a></li>
                                            </ul>
                                            <div class="step-content">
                                                <div class="step-tab-panel" id="tab">
                                                    <h3>1. <span> Where did the real Boston Tea Party take place?</span></h3>
                                                    <form>
                                                        <label class="container alltestwidth">New york
                                                            <input type="radio" checked="checked" name="radio">
                                                            <span class="checkmark"></span>
                                                        </label>
                                                        <label class="container alltestwidth">Washington dc
                                                            <input type="radio" name="radio">
                                                            <span class="checkmark"></span>
                                                        </label>
                                                        <label class="container alltestwidth">Boston
                                                            <input type="radio" name="radio">
                                                            <span class="checkmark"></span>
                                                        </label>
                                                        <label class="container alltestwidth">Philadelphia
                                                            <input type="radio" name="radio">
                                                            <span class="checkmark"></span>
                                                        </label>

                                                    </form>
                                                </div>
<div id="score" style="display:none">
<div class="row ans-options">
<div class="col-md-12">
<h3 class="tableheading">Your Result</h3>
<table  class="table table-border-none">
<tr>
    <th scope="col">Question No.</th>
    <th scope="col">Your Answer</th>
    <th scope="col">Correct Answer</th>
    </tr>
<tr class="mytbl">
    <td>1</td>
    <td class="correcttdlight">a. New York</td>
    <td class="correcttd">a. New York</td>
</tr>
<tfoot class="tblfoot"><tr>
<td></td><td>Attempt Questions</td><td>5/100</td></tr>
<tr>
<td></td><td>Your Score</td><td>6/100</td></tr></tfoot>
</table>
</div>
</div>
</div>
                                            </div>
                                        </div>
                                    </div>

                                          <div class="step-footer" id="footer" style="display:none">
                                            <br>
                                            
                                           
                                            <div>
                                                 <button type="button" class="btn btn-info pull-left" id="insert" style="display:none">Submit</button> 
                                             </div><br><br><br>
                                             <div>   
                                               <button type="button" id="prebtn" style="display:none" class="btn btn-primary">Previous</button>
                                               <button type="button" id="nxtbtn" style="display:none" class="btn  btn-success">Next</button>
                                                <button type="button" style="display: none; height: 42px " class="btn btn-info btn-lg submitqust" id="suggbtn" data-toggle="modal" data-target="#myModal1">Suggest Your Answer</button>
                                             </div>   
                                             <div>  
                                                <button type="button" id="stabtn" style="display:none" class="btn btn-primary btn-lg">Start Test</button>
                                             </div>
                                             <div>
                                                <button type="button" id="finbtn" class="btn btn-primary btn-lg" style="display:none">Result</button>
                                             </div>   
                                  <input type="hidden" id="question_id" name="question_id" value="0">
                                  <input type="hidden" name="prioritie_id" id="prioritie_id" value=""> 
                                  <input type="hidden" name="sugg_opt" id="sugg_opt" value="">
                                  <input type="hidden" name="sugg_text" id="sugg_text" value=""> 
                                            </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>

</div>
</div>

<!-- tab1 -->
<div class="modal fade alltestsectiomodal" id="myModal1" role="dialog">
<!--     <div class="modal-dialog">

        <div class="modal-content">
            <div class="modal-body">
               <h3>1.<span> Where did the real Boston Tea Party take place?</span></h3>
                <div class="form-group">

                    <select class="test" name="suggest_opt">
                        <optgroup>
                            <option value="1">New york</option>
                            <option value="2">Washington dc</option>
                            <option value="3">Boston</option>
                            <option value="4">Philadelphia</option>

                        </optgroup>

                    </select>
                </div>

                <div class="form-group">
                    <label for="comment">Comment:</label>
                    <textarea class="form-control" rows="5" id="comment"></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-Success">Submit</button>
            </div>
        </div>
         
    </div> -->
</div>
<?php include 'footer.php'; ?>
<?  $data=$list[4]; ?>
<script type="text/javascript">
$('#insert').click(function(){
    
     var sugg_text= $('#sugg_text').val();
     var sugg_opt= $('#sugg_opt').val();
     var v=$('#question_id').val();
     var id=$('#prioritie_id').val();
     var question = $('#quest_form'+v+'').serializeArray();
     var answer = $('#myform'+v+'').serializeArray();
     var url='<?php  echo base_url();?>usercon/insert_anwers'; 
     $.ajax(
     {
          url: url,
          data: {question,answer,id,sugg_opt,sugg_text},
          type:'post',
          success:function(done)
          {  
            console.log(done);
            $('#finbtn').hide();
          }  
      });
});
$('#stabtn').click(function()
{   
    $('.step-tab-panel').hide();
    var n=$('#question_id').val();
    var view = <?php echo json_encode($data); ?>;
    var showlist=view[n];
    var quest=showlist['question'];
    var opt_a=showlist['option_a'];
    var opt_b=showlist['option_b'];
    var opt_c=showlist['option_c'];
    var opt_d=showlist['option_d'];
    $('#aaa').val(quest);
    $('#demo').append("<div class='step-content'><form method='post' id='myform"+n+"' class='myform'><div class='step-tab-panel' id='tab"+n+"'><h3>1.<span> "+quest+"</span></h3><label class='container alltestwidth'>"+opt_a+"<input type='radio' value='option_a' class='check_option' name="+n+"><span class='checkmark'></span></label><label class='container alltestwidth'>"+opt_b+"<input  type='radio' name="+n+" value='option_b'><span class='checkmark'></span></label><label class='container alltestwidth'>"+opt_c+"<input type='radio' value='option_c' name="+n+"><span class='checkmark'></span></label><label class='container alltestwidth'>"+opt_d+"<input type='radio' name="+n+" value='option_d'><span class='checkmark'></span></label></div></form></div><div class='step-tab-panel' id='ques"+n+"'><form id='quest_form"+n+"' class='quest_form'><input type='hidden' name="+n+"  value='"+quest+"'></form></div>");
      $('#stabtn').hide(); $('#nxtbtn').show(); $('#suggbtn').show();  $('#insert').show(); $('#prebtn').hide();
});
$('#nxtbtn').click(function()
{       
        $('#sugg_text').val('');
        $('#sugg_opt').val('');
        $('.step-tab-panel').hide();
        var n=$('#question_id').val();
        $('#tab'+n+'').hide(); 
        n++; 
        $('#question_id').val(n);  
        if(n < 10 )
        { 
             var num=n+1;
             $('#prebtn').show();
             var view = <?php echo json_encode($data); ?>;
             var showlist=view[n];
             var quest=showlist['question'];
             var opt_a=showlist['option_a'];
             var opt_b=showlist['option_b'];
             var opt_c=showlist['option_c'];
             var opt_d=showlist['option_d'];
             $('#demo').append("<div class='step-content'><form method='post' id='myform"+n+"'class='myform'><div class='step-tab-panel' id='tab"+n+"'><h3>"+num+".<span> "+quest+"</span></h3><label class='container alltestwidth'>"+opt_a+"<input type='radio' class='check_option' value='option_a' name="+n+"><span class='checkmark'></span></label><label class='container alltestwidth'>"+opt_b+"<input type='radio' value='option_b' name="+n+"><span class='checkmark'></span></label><label class='container alltestwidth'>"+opt_c+"<input type='radio' value='option_c' name="+n+"><span class='checkmark'></span></label><label class='container alltestwidth'>"+opt_d+"<input type='radio' value='option_d' name="+n+"><span class='checkmark'></span></label></div></form></div><div class='step-tab-panel' id='ques"+n+"'><form class='quest_form' id='quest_form"+n+"'><input type='hidden' name="+n+"  value='"+quest+"'></form></div>");
        }else{ $('#finbtn').show(); $('#prebtn').hide(); $('#insert').hide();   $('#nxtbtn').hide(); $('#suggbtn').hide(); } 
});
$('#prebtn').click(function()
{      
        $('.step-tab-panel').hide();
        var i=$('#question_id').val();
              $('#tab'+i+'').remove();
              $('#ques'+i+'').remove();
              i--;   
        if( i< 1 )
        {  $('#tab'+i+'').show();
           $('#question_id').val(i);
           $('#prebtn').hide();
        }else
        {  $('#tab'+i+'').show();
           $('#question_id').val(i);
        }

});
$('#finbtn').click(function()
{
     $('#score').show();
     var question = $('.quest_form').serializeArray();
     var answer = $('.myform').serializeArray();
     var url='<?php  echo base_url();?>usercon/check_anwers'; 
     $.ajax(
     {
          url: url,
          data: {question,answer},
          type:'post',
          success:function(done)
          {  
               console.log(done);
               $('.mytbl').addClass('backdrop');
               $('#finbtn').hide();
               var response=done;
               var count=response.attempt; 
               if(count > 0 )
               {                     
                   $('.mytbl').remove();
                   $('.tblfoot').remove();
                   for(var i =0; i<count; i++ )
                   {   var j=i+1; 
                       var result=response[i];
                       your_option=result['your_option'];
                       correct_answer=result['correct_answer'];
                       var html =('<tr class="mytbl"><td>'+j+'</td><td class="correcttdlight">'+your_option+'</td><td class="correcttd">'+correct_answer+'</td></tr>');
                           $('table').append(html);
                    }
                     var html =('<tfoot class="tblfoot"><tr><td></td><td>Attempt  Questions</td><td>'+response. attempt+'/10</td></tr><tr><td></td><td>Your Score</td><td>'+response.score+'/10</td></tr></tfoot>');
                        $('table').append(html);
               }else
               {
                 $('.mytbl').remove();
                 $('.tblfoot').remove();
                 var html =('<tfoot class="tblfoot"><tr><td></td><td>Attempt  Questions</td><td>0/10</td></tr><tr><td></td><td>Your Score</td><td>0/10</td></tr></tfoot>');
                 $('table').append(html);
               }      
          }  
     });
});

$('#suggbtn').click(function(){
    var num=$('#question_id').val();
    var view = <?php echo json_encode($data); ?>;
    var showlist=view[num];
    var serial= num;
    serial++; 
    var quest=showlist['question'];
    var opt_a=showlist['option_a'];
    var opt_b=showlist['option_b'];
    var opt_c=showlist['option_c'];
    var opt_d=showlist['option_d'];

 $('#myModal1').html('<form id="suggform" class="suggform"><div class="modal-dialog"><div class="modal-content"><div class="modal-body"><h3>'+serial+'.<span>'+quest+'</span></h3><div class="form-group"><select required class="test" id="suggest_opt" name="suggest_opt"><optgroup><option disabled selected value="">select your suggest &nbsp; answer</option><option value="option_a">'+opt_a+'</option><option value="option_b">'+opt_b+'</option><option value="option_c">'+opt_c+'</option><option value="option_d">'+opt_d+'</option></optgroup></select><span class="errmsg" style="display:none; color:red">Choose your suggest Option</span></div><div class="form-group"><label for="comment">Comment:</label><textarea id="suggest_text" class="form-control" rows="5" id="comment"></textarea><span class="errmsg1" style="display:none; color:red">Please Fill Your Comment</span></div></div><div class="modal-footer"><button type="button" class="btn btn-default" data-dismiss="modal">Close</button><button type="button" onclick="getsuggest()" class="btn btn-Success">Submit</button></div></div></div></form>');$('.test').fSelect();

});
function getsuggest()
{  
   
     if($('#suggest_opt').val() == null)
     { $('.errmsg').show();
       setTimeout(function(){
       $('.errmsg').hide();  }, 3000); 
     }
     if($('#suggest_text').val() =='')
     {
        $('.errmsg1').show();
        setTimeout(function(){
        $('.errmsg1').hide();  }, 3000); 
     }
     if($('#suggest_opt').val() != null &&  $('#suggest_text').val() != '')
     {     

          var sugg_opt=$('#suggest_opt').val();
          var sugg_text=$('#suggest_text').val();
          $('#sugg_opt').val(sugg_opt);
          $('#sugg_text').val(sugg_text); 
          $('.check_option').attr("checked", "checked");
          $('#myModal1').hide();
          $('.modal-backdrop').remove();

     }
}
//This is for validation Priorities form //
$('#addyear').click(function(){
  if( $("#addyear option:selected").val() =='')
  alert("Please select atlease one option");
});

$('#addgrade').click(function(){
  if( $("#addgrade option:selected").val() =='')
  alert("Please select atlease one option");
});
// select country with state //
$('#addcountry').click(function()
{ 
    if( $("#addcountry option:selected").val() ==''){
      alert("Please select atlease one option");
    } 
    else 
    {     var data = $('#addcountry').val();
          var url='<?php echo base_url();?>usercon/questions'; 
          $.ajax(
          {  
                url: url,
                data:{country_id:data},
                type:'post',
                success:function(response)
                {     
                    console.log(response);
                    $('#addstate').empty();
                    $('#addstate').append('<option value="" disabled selected>Select/state</option>');
                    if(response.length > 0)
                    {  
                        var selOpts = "";
                        for (i=0;i<response.length;i++)
                        {   var id =response[i]['id'];
                            var val = response[i]['state_name'];
                            selOpts += "<option value="+id+">"+val+"</option>";
                        }
                        $('#addstate').append(selOpts);
                    }
                    else
                    {
                     $('#addstate').empty();
                    } 
                }
               
          });
    }     
});
// select cource automatic subject and chapter //
$('#addcourse').click(function()
{
    if($("#addcourse option:selected").val() ==''){
      alert("Please select atlease one option");
      $('#addsubject').empty();
      $('#addchapter').empty();
    }  
    else    
    {     var data = $('#addcourse').val();
          var url='<?php echo base_url();?>usercon/questions'; 
          $.ajax(
          {  
                url: url,
                data:{course_id:data},
                type:'post',
                success:function(response)
                {      
                    console.log(response);
                    var result1=response[0];
                    var result2=response[1];
                    

                    $('#addsubject').empty();
                    $('#addsubject').append('<option value="" disabled selected>Select/subject</option>');
                    if(result1.length > 0  )
                    { 
                        var selOpts = "";
                        for (i=0;i<result1.length;i++)
                        {   var id=result1[i]['id'];
                            var val = result1[i]['subject_name'];
                            selOpts += "<option value="+id+">"+val+"</option>";
                        }
                        $('#addsubject').append(selOpts);
                     }
                     else
                     {
                       $('#addsubject').empty();
                     }   
                    $('#addchapter').empty();
                    $('#addchapter').append('<option value="" disabled selected>Select/chapter</option>');
                     if(result2.length > 0 )
                     { 
                        var selOpts = "";
                        for (i=0;i<result2.length;i++)
                        {   var id=result2[i]['id'];
                            var val = result2[i]['chapter_name'];
                            selOpts += "<option value="+id+">"+val+"</option>";
                        }
                        $('#addchapter').append(selOpts);
                     }
                     else
                     {
                     $('#addchapter').empty();
                     }
                }     
           });
    }
});
//** submit Priorities form **// 
$('#processbtn').click(function(){
$("#Prioritieform").validate(
{    rules:
     {   addcountry:{ required: true },
         addcourse: { required: true },
         addgrade:  { required: true },
         addyear:   { required: true },
         addchapter:{ required: true },
         addstate:  { required: true },
         addsubject:{ required: true }
      },
    submitHandler: function(form) 
    {  
          var country_id  =  $('#addcountry').val();
          var course_id   =  $('#addcourse') .val();
          var grade_id    =  $('#addgrade')  .val();
          var year_id     =  $('#addyear')   .val();
          var chapter_id  =  $('#addchapter  option:selected').val(); 
          var subject_id  =  $('#addsubject  option:selected').val();
          var state_id    =  $('#addstate    option:selected').val();
       
           var url='<?php echo base_url();?>usercon/checkexist_priorities'; 
           $.ajax(
           {
              url: url,
              data:{country_id:country_id,course_id:course_id,grade_id:grade_id,
                    year_id:year_id,chapter_id:chapter_id,subject_id:subject_id,
                    state_id:state_id },
              type:'post',
               success:function(success)
               { 
                     console.log(success);
                     if(success.trim()=='not')
                     { $('.three').text('NOT Exist This Information ');
                       $('.alert-danger').show();
                       setTimeout(function(){
                       $('.alert-danger').hide(); }, 2000); 
                     }else
                     { $('#prioritie_id').val(success); 
                       $('#Prioritieform').hide();
                       $('#demo').show(); 
                       $('#footer').show();
                       $('#stabtn').show();
                      
                     }
                }  
           });
    },
});
});
</script>

    <!--======== SCRIPT FILES =========-->
 <script>
    var frmInfo = $('#frmInfo');
    var frmInfoValidator = frmInfo.validate();

    var frmLogin = $('#frmLogin');
    var frmLoginValidator = frmLogin.validate();

    var frmMobile = $('#frmMobile');
    var frmMobileValidator = frmMobile.validate();

    $('#demo').steps({
      onChange: function (currentIndex, newIndex, stepDirection) {
        console.log('onChange', currentIndex, newIndex, stepDirection);
        // tab1
        if (currentIndex === 3) {
          if (stepDirection === 'forward') {
            var valid = frmLogin.valid();
            return valid;
          }
          if (stepDirection === 'backward') {
            frmLoginValidator.resetForm();
          }
        }

        // tab2
        if (currentIndex === 1) {
          if (stepDirection === 'forward') {
            var valid = frmInfo.valid();
            return valid;
          }
          if (stepDirection === 'backward') {
            frmInfoValidator.resetForm();
          }
        }

        // tab3
        if (currentIndex === 4) {
          if (stepDirection === 'forward') {
            var valid = frmMobile.valid();
            return valid;
          }
          if (stepDirection === 'backward') {
            frmMobileValidator.resetForm();
          }
        }

        return true;

      },
      onFinish: function () {
        alert('Wizard Completed');
      }
    });
  </script>
    
<script>
(function($) {
    $(function() {
        $('.test').fSelect();
    });
})(jQuery);
</script>


</body>
</html>
