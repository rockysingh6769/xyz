<!DOCTYPE html>
<html lang="en">
<head>
    <title>Attempt Test</title>
    <!--== META TAGS ==-->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <base href="<?php echo base_url(); ?>">
    <!--== FAV ICON ==-->
    <link rel="shortcut icon" href="assets/user/images/fav.ico">

    <!-- FONT-AWESOME ICON CSS -->
    <link rel="stylesheet" href="assets/user/css/font-awesome.min.css">

    <!--== ALL CSS FILES ==-->
    <link rel="stylesheet" href="assets/user/css/style.css">
    <link rel="stylesheet" href="assets/user/css/mob.css">
    <link rel="stylesheet" href="assets/user/css/bootstrap.css">
    <link rel="stylesheet" href="assets/user/css/materialize.css" />
    <link rel="stylesheet" href="assets/user/css/dataTables.bootstrap.min.css"/>
    <link href="assets/user/css/bootstrap-glyphicons.css" rel="stylesheet"> 
    <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="assets/user/css/jquery-steps.css">
    <link href="https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900&amp;subset=latin-ext" rel="stylesheet">
<style type="text/css">
#question_list
{display: none;} 
.alert-warning,.alert-danger,.alert-success
{ 
    display: none;
    position: fixed;
    max-width: 50%; 
    width: 100%; 
    margin: auto;   
    top: 45%;   left: 16%;    
    z-index: 111111;     
    box-shadow: 1px 1px 8px rgba(0, 0, 0, 0.15);     
    text-align: center;     
    right: 0;    
} 
#footer {  margin-top: 0px; margin-left: 50px; } 
.ques   {  color: black; }
.opt    {  color: red; font-size: 20px; margin-left: 20px; }
.ans    {  color: blue;  }
.corr   {  color: green; }
.sco    {  color:green; font-size: 25px; }
.scor   {  color:black; font-size: 25px; } 
.opt_num{  margin-left: 10px; }
#finbtn {  margin-top: -10px; line-height: 2; }
</style>
</head>

<body class="questionpagebgcolor">
<div class="alert alert-warning">
<strong><center class="one"></center></strong>  
</div>

<div class="alert alert-success">
<strong><center class="two"></center></strong> 
</div>

<div class="alert alert-danger">
<strong><center class="three"></center></strong> 
</div>
    <!-- header -->
        <?php include 'header.php'; ?>
    <!-- // header -->

<!--== BODY CONTNAINER ==-->
<div class="container-fluid sb2">
    <div class="row">
        <!-- sidemenu -->
            <?php include 'sidenav.php'; ?>
            <!-- // sidemenu -->

        <!--== BODY INNER CONTAINER ==-->
        <div class="sb2-2">
            <!--== breadcrumbs ==-->
            <div class="sb2-2-2">
                <ul>
                    <li><a href="Usercon/userindex"><i class="fa fa-home" aria-hidden="true"></i> Home</a>
                    </li>
                    <li class="active-bre"><a href="#">Attempt Test</a>
                    </li>

                </ul>
            </div>
          <!--== DASHBOARD INFO ==-->
          <!--== User Details ==-->
       <div class="tab-inn detail-page">
         <div class="sb2-2-3">
                <div class="box-inn-sp questionsection attemptsection">
                    <div class="row">
                        <div class="tab-inn ">
                            <form id="Prioritieform" method="post">
                        <p  class="fontsize-p">Please Fill The Required Details In Order To Proceed</p>
                                <div class="col-md-4">
                                    <div class="form-group lgbtn">
                                         <select id="addcountry"  name="addcountry" class="form-control"  > 
                                          <option class="abc" value="">Select/Country</option>
                                          <?php foreach($list[0] as $key => $value){ ?>
                                                <option  value="<?php print_r($value['id'])?>">
                                                <?php echo $value['country_name']; ?></option> 
                                                <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">

                                    <div class="form-group lgbtn">

                                        <select  class="form-control" name="addstate" id="addstate">
                                                <option class="abc" value="">State/Province</option>
                                            </select>
                                    </div>

                                </div>
                                <div class="col-md-4">

                                    <div class="form-group lgbtn">

                                        <select required id="addcourse"  name="addcourse" class="form-control"  > 
                                          <option  selected  value="">Select/Course </option>
                                          <?php foreach($list[1] as $key => $value){ ?>
                                                <option  value="<?php print_r($value['id'])?>">
                                                <?php echo $value['course_name']; ?></option> 
                                                <?php } ?>
                                        </select>
                                    </div>

                                </div>
                                <div class="col-md-4">

                                    <div class="form-group lgbtn">

                                        <select required id="addgrade" name="addgrade" class="form-control"  > 
                                                 <option selected  value="">Grade/Level</option>
                                                 <?php foreach($list[2] as $key => $value){ ?>
                                                 <option  value="<?php print_r($value['id'])?>">
                                                 <?php echo $value['grade_name']; ?></option> 
                                                <?php } ?>
                                            </select>
                                    </div>
                                </div>
                                <div class="col-md-4">

                                    <div class="form-group lgbtn">

                                        <select required id="addyear" name="addyear" class="form-control"  > 
                                                 <option  value="">Year/List</option>
                                                 <?php foreach($list[3] as $key => $value){ ?>
                                                 <option  value="<?php print_r($value['id'])?>">
                                                 <?php echo $value['year']; ?></option> 
                                                <?php } ?>
                                            </select>
                                    </div>

                                </div>
                                <div class="col-md-4">

                                    <div class="form-group lgbtn">

                                        <select required class="form-control" name="addsubject" id="addsubject" >
                                                <option  value="">Subject/List</option>
                                                
                                            </select>
                                    </div>

                                </div>
                                <div class="col-md-4">

                                    <div class="form-group lgbtn">

                                       <select class="form-control" name="addchapter" id="addchapter">
                                                <option  value="">Chapter/List</option>
                                            </select>
                                    </div>

                                </div>
                                <div class="clearfix"></div>
                             

              
                                <div class="form-group col-sm-12 mt-1 mb-4">

                                   <input type="submit" class="btn btn-primary" data-toggle="collapse"
                                   id="processbtn" data-target="#demo" aria-expanded="false" value="Proceed">
                                   <!-- <a href="javascript:void(0);" class="btn btn-primary" data-toggle="collapse" id="processbtn" data-target="#demo" aria-expanded="false">Proceed</a>-->
                                </div>
                         </form>         
                          
                                <div class="col-md-12 alltest">

                                    <div id="demo" class="collapse" style="display:none">
                                        <div class="step-app">

                                            <ul class="step-steps" style="display:none">
                                                <li><a href="#tab1"><span class="number">1</span> </br>Step 1</a></li>
                                                <li><a href="#tab2"><span class="number">2</span></br> Step 2</a></li>
                                                <li><a href="#tab3"><span class="number">3</span></br> Step 3</a></li>
                                                <li><a href="#tab4"><span class="number">4</span></br> Step 4</a></li>
                                            </ul>
   

<div id="score" style="display:none">
<div class="row ans-options"  >
<div class="col-md-12">
<h3 class="tableheading">Your Result</h3>
<table  class="table table-border-none">
<tr>
    <th scope="col">Question No.</th>
    <th scope="col">Your Answer</th>
    <th scope="col">Correct Answer</th>
    </tr>
<tr class="mytbl">
    <td>1</td>
    <td class="correcttdlight">a. New York</td>
    <td class="correcttd">a. New York</td>
</tr>
<tfoot class="tblfoot"><tr>
<td></td><td>Attempt Questions</td><td>5/100</td></tr>
<tr>
<td></td><td>Your Score</td><td>6/100</td></tr></tfoot>
</table>
</div>
</div>
</div>

                                            <div class="step-content">
                                                <div class="step-tab-panel" id="tab0">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                             <div class="step-footer" id="footer" style="display:none">
                                                <button type="button" id="prebtn" style="display:none" class="step-btn prevoius">Previous</button>
                                                <button type="button" id="nxtbtn" style="display:none" class="step-btn next">Next</button>
                                                <button type="button" id="stabtn" style="display:none" class="step-btn next">Start Test</button>
                                                <button type="button" id="finbtn" class="step-btn finish" style="display:none">Result</button>
                                  <input type="hidden" id="question_id" name="question_id" value="1">  
                                            </div>

                                </div>
                            </div>
                       </div>
                  </div>
              </div>
         </div>
    </div>
</div>
</div>
</div>

</div>
</div>

<!-- tab1 -->
<div class="modal fade alltestsectiomodal" id="myModal1" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <!--div class="modal-header border-0">
          <button type="button" class="close" data-dismiss="modal">&times;</button>

        </div-->
            <div class="modal-body" id="result1show">
                <h3>1.<span> Where did the real Boston Tea Party take place?</span></h3>
                <div class="form-group">
                    
                    <select class="test " multiple="multiple">
                        <optgroup>
                            <option value="1">New york</option>
                            <option value="2">Washington dc</option>
                            <option value="3">Boston</option>
                            <option value="4">Philadelphia</option>

                        </optgroup>

                    </select>
                </div>

                <div class="form-group">
                    <label for="comment">Comment:</label>
                    <textarea class="form-control" rows="5" id="comment"></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-Success">Submit</button>
            </div>
        </div>

    </div>
</div>
<?  $data=$list[4]; ?>
<script src="assets/user/js/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
<script type="text/javascript">
$('#stabtn').click(function()
{   
    $('.step-tab-panel').hide();
    var n=$('#question_id').val();
    var view = <?php echo json_encode($data); ?>;
    var showlist=view[0];
    var quest=showlist['question'];
    var opt_a=showlist['option_a'];
    var opt_b=showlist['option_b'];
    var opt_c=showlist['option_c'];
    var opt_d=showlist['option_d'];
    $('#aaa').val(quest);
    $('#demo').append("<div class='step-content'><form method='post' class='myform'><div class='step-tab-panel' id='tab"+n+"'><h3>"+n+".<span> "+quest+"</span></h3><label class='container alltestwidth'>"+opt_a+"<input type='radio' value='option_a' name="+n+"><span class='checkmark'></span></label><label class='container alltestwidth'>"+opt_b+"<input type='radio' name="+n+" value='option_b'><span class='checkmark'></span></label><label class='container alltestwidth'>"+opt_c+"<input type='radio' value='option_c' name="+n+"><span class='checkmark'></span></label><label class='container alltestwidth'>"+opt_d+"<input type='radio' name="+n+" value='option_d'><span class='checkmark'></span></label></div></form></div><div class='step-tab-panel' id='ques"+n+"'><form class='quest_form'><input type='hidden' name="+n+"  value='"+quest+"'></form></div>");
      $('#stabtn').hide(); $('#nxtbtn').show(); $('#prebtn').hide();
});
$('#nxtbtn').click(function()
{       
        $('.step-tab-panel').hide();
        var n=$('#question_id').val();
        $('#tab'+n+'').hide(); 
        n++; 
        $('#question_id').val(n);  
        if(n <= 10 )
        { 
             $('#prebtn').show();
             var view = <?php echo json_encode($data); ?>;
             var showlist=view[n];
             var quest=showlist['question'];
             var opt_a=showlist['option_a'];
             var opt_b=showlist['option_b'];
             var opt_c=showlist['option_c'];
             var opt_d=showlist['option_d'];
             $('#demo').append("<div class='step-content'><form method='post' class='myform'><div class='step-tab-panel' id='tab"+n+"'><h3>"+n+".<span> "+quest+"</span></h3><label class='container alltestwidth'>"+opt_a+"<input type='radio' value='option_a' name="+n+"><span class='checkmark'></span></label><label class='container alltestwidth'>"+opt_b+"<input type='radio' value='option_b' name="+n+"><span class='checkmark'></span></label><label class='container alltestwidth'>"+opt_c+"<input type='radio' value='option_c' name="+n+"><span class='checkmark'></span></label><label class='container alltestwidth'>"+opt_d+"<input type='radio' value='option_d' name="+n+"><span class='checkmark'></span></label></div></form></div><div class='step-tab-panel' id='ques"+n+"'><form class='quest_form'><input type='hidden' name="+n+"  value='"+quest+"'></form></div>");
         }else{ $('#finbtn').show(); $('#prebtn').hide();  $('#nxtbtn').hide(); } 
                 
});
$('#prebtn').click(function()
{      
       $('.step-tab-panel').hide();
        var i=$('#question_id').val();
              $('#tab'+i+'').remove();
              $('#ques'+i+'').remove();
              i--;   
        if( i< 2 )
        {  $('#tab'+i+'').show();
           $('#question_id').val(i);
           $('#prebtn').hide();
        }else
        {  $('#tab'+i+'').show();
           $('#question_id').val(i);
        }

});
$('#finbtn').click(function(){
    
     $('#score').show();
     var question = $('.quest_form').serializeArray();
     var answer = $('.myform').serializeArray();
     var url='<?php  echo base_url();?>usercon/check_anwers'; 
     $.ajax(
     {
          url: url,
          data: {question,answer},
          type:'post',
          success:function(done)
          {  
                
                console.log(done);
                $('#finbtn').hide();
                var response=done;
                var count=response.attempt; 
               
               if(count > 0 )
               {                     
                   
                   $('.mytbl').remove();
                   $('.tblfoot').remove();
                  
                   for(var i =0; i<count; i++ )
                   {  
                       var result=response[i];
                       your_option=result['your_option'];
                       correct_answer=result['correct_answer'];
                       var html =('<tr class="mytbl"><td>'+i+'</td><td class="correcttdlight">'+your_option+'</td><td class="correcttd">'+correct_answer+'</td></tr>');
                           $('table').append(html);
                    }
                     var html =('<tfoot class="tblfoot"><tr><td></td><td>Attempt  Questions</td><td>'+response. attempt+'/10</td></tr><tr><td></td><td>Your Score</td><td>'+response.score+'/10</td></tr></tfoot>');
                        $('table').append(html);
                 }else
                 {
                    $('.mytbl').remove();
                    $('.tblfoot').remove();
                    var html =('<tfoot class="tblfoot"><tr><td></td><td>Attempt  Questions</td><td>0/10</td></tr><tr><td></td><td>Your Score</td><td>0/10</td></tr></tfoot>');
                       $('table').append(html);

                 }      
               

               
          }  
      });
});
//This is for validation Priorities form //
$('#addyear').click(function(){
  if( $("#addyear option:selected").val() =='')
  alert("Please select atlease one option");
});

$('#addgrade').click(function(){
  if( $("#addgrade option:selected").val() =='')
  alert("Please select atlease one option");
});


// select country with state //
$('#addcountry').click(function()
{ 
    if( $("#addcountry option:selected").val() ==''){
      alert("Please select atlease one option");
    } 
    else 
    {     var data = $('#addcountry').val();
          var url='<?php echo base_url();?>usercon/questions'; 
          $.ajax(
          {  
                url: url,
                data:{country_id:data},
                type:'post',
                success:function(response)
                {     
                    console.log(response);
                    $('#addstate').empty();
                     $('#addstate').append('<option value="" disabled selected>Select/state</option>');
                    if(response.length > 0)
                    {  
                        var selOpts = "";
                        for (i=0;i<response.length;i++)
                        {   var id =response[i]['id'];
                            var val = response[i]['state_name'];
                            selOpts += "<option value="+id+">"+val+"</option>";
                        }
                        $('#addstate').append(selOpts);
                       
                    }
                    else
                    {
                     $('#addstate').empty();
                    } 
                    
                }
               
           });
    }     
});

// select cource automatic subject and chapter //
$('#addcourse').click(function()
{
    if($("#addcourse option:selected").val() ==''){
      alert("Please select atlease one option");
      $('#addsubject').empty();
      $('#addchapter').empty();
    }  
    else    
    {     var data = $('#addcourse').val();
          var url='<?php echo base_url();?>usercon/questions'; 
          $.ajax(
          {  
                url: url,
                data:{course_id:data},
                type:'post',
                success:function(response)
                {      
                    console.log(response);
                    var result1=response[0];
                    var result2=response[1];
                    

                    $('#addsubject').empty();
                    $('#addsubject').append('<option value="" disabled selected>Select/subject</option>');
                    if(result1.length > 0  )
                    { 
                        var selOpts = "";
                        for (i=0;i<result1.length;i++)
                        {   var id=result1[i]['id'];
                            var val = result1[i]['subject_name'];
                            selOpts += "<option value="+id+">"+val+"</option>";
                        }
                        $('#addsubject').append(selOpts);
                     }
                     else
                     {
                       $('#addsubject').empty();
                     }   
                     $('#addchapter').empty();
                     $('#addchapter').append('<option value="" disabled selected>Select/chapter</option>');
                     if(result2.length > 0 )
                     { 
                        var selOpts = "";
                        for (i=0;i<result2.length;i++)
                        {   var id=result2[i]['id'];
                            var val = result2[i]['chapter_name'];
                            selOpts += "<option value="+id+">"+val+"</option>";
                        }
                        $('#addchapter').append(selOpts);
                     }
                     else
                     {
                     $('#addchapter').empty();
                     }
                }     
           });
    }
});
//** submit Priorities form **// 
$('#processbtn').click(function(){
$("#Prioritieform").validate(
{    rules:
     {   addcountry:{ required: true },
         addcourse: { required: true },
         addgrade:  { required: true },
         addyear:   { required: true },
         addchapter:{ required: true },
         addstate:  { required: true },
         addsubject:{ required: true }
      },
    submitHandler: function(form) 
    {  
          var country_id  =  $('#addcountry').val();
          var course_id   =  $('#addcourse') .val();
          var grade_id    =  $('#addgrade')  .val();
          var year_id     =  $('#addyear')   .val();
          var chapter_id  =  $('#addchapter  option:selected').val(); 
          var subject_id  =  $('#addsubject  option:selected').val();
          var state_id    =  $('#addstate    option:selected').val();
       
           var url='<?php echo base_url();?>usercon/checkexist_priorities'; 
           $.ajax(
           {
              url: url,
              data:{country_id:country_id,course_id:course_id,grade_id:grade_id,
                    year_id:year_id,chapter_id:chapter_id,subject_id:subject_id,
                    state_id:state_id },
              type:'post',
               success:function(success)
               { 
                     console.log(success);
                     if(success.trim()=='not')
                     { $('.three').text('NOT Exist This Information ');
                       $('.alert-danger').show();
                       setTimeout(function(){
                       $('.alert-danger').hide(); }, 2000); 
                     }else
                     { $('#hidden').val(success); 
                       $('#Prioritieform').hide();
                       $('#demo').show(); 
                       $('#footer').show();
                       $('#stabtn').show();
                     }
                }  
            });
    },
});
});


</script>
    <!--======== SCRIPT FILES =========-->
    <script src="assets/user/js/jquery.min.js"></script>
    <script src="assets/user/js/bootstrap.min.js"></script>
    <script src="assets/user/js/materialize.min.js"></script>
    <!-- <script src="js/custom.js"></script> -->
    <script src="assets/user/js/fSelect.js"></script>
    
  <script src="/multiplechoice/assets/user/js/jquery-steps.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"></script>


 <script>
    var frmInfo = $('#frmInfo');
    var frmInfoValidator = frmInfo.validate();

    var frmLogin = $('#frmLogin');
    var frmLoginValidator = frmLogin.validate();

    var frmMobile = $('#frmMobile');
    var frmMobileValidator = frmMobile.validate();

    $('#demo').steps({
      onChange: function (currentIndex, newIndex, stepDirection) {
        console.log('onChange', currentIndex, newIndex, stepDirection);
        // tab1
        if (currentIndex === 10) {
          if (stepDirection === 'forward') {
            var valid = frmLogin.valid();
            return valid;
          }
          if (stepDirection === 'backward') {
            frmLoginValidator.resetForm();
          }
        }

        // tab2
        if (currentIndex === 1) {
          if (stepDirection === 'forward') {
            var valid = frmInfo.valid();
            return valid;
          }
          if (stepDirection === 'backward') {
            frmInfoValidator.resetForm();
          }
        }

        // tab3
        if (currentIndex === 11) {
          if (stepDirection === 'forward') {
            var valid = frmMobile.valid();
            return valid;
          }
          if (stepDirection === 'backward') {
            frmMobileValidator.resetForm();
          }
        }

        return true;

      },
      onFinish: function () {
        alert('Wizard Completed');
      }
    });
  </script>
    
<script>
(function($) {
    $(function() {
        $('.test').fSelect();
    });
})(jQuery);
</script>


</body>


</html>



