<!DOCTYPE html>
<html lang="en">



<head>
    <title>Detail</title>
    <!--== META TAGS ==-->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!--== FAV ICON ==-->
    <link rel="shortcut icon" href="images/fav.ico">
    <base href="<? echo base_url(); ?>">

    <!-- FONT-AWESOME ICON CSS -->
    <link rel="stylesheet" href="assets/user/css/font-awesome.min.css">

    <!--== ALL CSS FILES ==-->
    <link rel="stylesheet" href="assets/user/css/style.css">
    <link rel="stylesheet" href="assets/user/css/mob.css">
    <link rel="stylesheet" href="assets/user/css/bootstrap.css">
    <link rel="stylesheet" href="assets/user/css/materialize.css" />
    <link href="https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900&amp;subset=latin-ext" rel="stylesheet">

</head>

<body>
<!-- header -->
            <?php include 'header.php'; ?>
        <!-- // header -->

<!--== BODY CONTNAINER ==-->
<div class="container-fluid sb2">
    <div class="row">
        
        <!-- sidemenu -->
        <?php include 'sidenav.php'; ?>
        <!-- // sidemenu -->

        <div class="sb2-2">
            <div class="sb2-2-2">
                <ul>
                    <li><a href="index.php"><i class="fa fa-home" aria-hidden="true"></i> Home</a>
                    </li>
                    <li class="active-bre"><a href="#">Detail</a>
                    </li>
                </ul>
            </div>
            <div class="sb2-2-3">
                <div class="box-inn-sp">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="tab-inn detail-page">
                                  <? $j=1; ?>
                                  <?php foreach ($list as  $value):?> 
                                <h3><? echo $j++; ?>. <span><? echo $value['question'];?></span></h3>
                                <div class="row ans-options">
                                    <div class="col-md-6 relative">
                                        <? if($value['your_answer'] == 'option_a' && $value['correct_answer']== 'option_a') 
                                         {echo '<p class="correct">'.$value['option_a']   .'<i class="fa fa-check"   
                                               aria-hidden="true"></i></p>';
                                         }else if($value['your_answer'] == 'option_a') 
                                         { echo '<p class="wrong">'.$value['option_a']   .'<i class="fa fa-times"
                                               aria-hidden="true"></i></p>';
                                         }
                                         else if($value['correct_answer']== 'option_a')
                                         {
                                          echo '<p class="correct">'.$value['option_a']   .'<i class="fa fa-check"   
                                               aria-hidden="true"></i></p>';
                                         }else
                                         {  echo '<p>'.$value['option_a'].'</p>';
                                         }?>
                                    </div>
                                      <div class="col-md-6 relative">
                                        <? if($value['your_answer'] == 'option_b' && $value['correct_answer']== 'option_b') 
                                         {echo '<p class="correct">'.$value['option_b']   .'<i class="fa fa-check"   
                                               aria-hidden="true"></i></p>';
                                         }else if($value['your_answer'] == 'option_b') 
                                         { echo '<p class="wrong">'.$value['option_b']   .'<i class="fa fa-times"
                                               aria-hidden="true"></i></p>';
                                         }
                                         else if($value['correct_answer']== 'option_b')
                                         {
                                          echo '<p class="correct">'.$value['option_b']   .'<i class="fa fa-check"   
                                               aria-hidden="true"></i></p>';
                                         }else
                                         {  echo '<p>'.$value['option_b'].'</p>';
                                         }?>
                                    </div>
                                      <div class="col-md-6 relative">
                                        <? if($value['your_answer'] == 'option_c' && $value['correct_answer']== 'option_c') 
                                         {echo '<p class="correct">'.$value['option_c']   .'<i class="fa fa-check"   
                                               aria-hidden="true"></i></p>';
                                         }else if($value['your_answer'] == 'option_c') 
                                         { echo '<p class="wrong">'.$value['option_c']   .'<i class="fa fa-times"
                                               aria-hidden="true"></i></p>';
                                         }
                                         else if($value['correct_answer']== 'option_c')
                                         {
                                          echo '<p class="correct">'.$value['option_c']   .'<i class="fa fa-check"   
                                               aria-hidden="true"></i></p>';
                                         }else
                                         {  echo '<p>'.$value['option_c'].'</p>';
                                         }?>
                                    </div>
                                       <div class="col-md-6 relative">
                                        <? if($value['your_answer'] == 'option_d' && $value['correct_answer']== 'option_d') 
                                         {echo '<p class="correct">'.$value['option_d']   .'<i class="fa fa-check"   
                                               aria-hidden="true"></i></p>';
                                         }else if($value['your_answer'] == 'option_d') 
                                         { echo '<p class="wrong">'.$value['option_d']   .'<i class="fa fa-times"
                                               aria-hidden="true"></i></p>';
                                         }
                                         else if($value['correct_answer']== 'option_d')
                                         {
                                          echo '<p class="correct">'.$value['option_d']   .'<i class="fa fa-check"   
                                               aria-hidden="true"></i></p>';
                                         }else
                                         {  echo '<p>'.$value['option_d'].'</p>';
                                         }?>
                                    </div>
                                  
                                </div>
                                <div class="suggestion">
                                    <div class="row">
                                               <? if(!empty($value['suggest_option']))
                                               { echo '<div class="col-md-6"><h4>Suggest your answer</h4><div class="form-group"><input type="text" class="form-control" value="'.$value['suggest_option'].'" disabled></div></div>';
                                               }
                                               ?> 
                                               <? if(!empty($value['comment']))
                                                  { echo '<div class="col-md-6"><h4>Comment</h4><div class="form-group"> <textarea disabled class="form-control" rows="5" id="comment">'.$value['comment'].'</textarea></div></div>';}
                                                ?> 
                                    </div>
                                </div>
                                <?php endforeach; ?>   
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
 <?php include 'footer.php'; ?>
    <!--== BOTTOM FLOAT ICON ==-->
    <!--======== SCRIPT FILES =========-->
</body>
</html>