<div class="container-fluid sb1">
    <div class="row">
        <!--== LOGO ==-->
        <div class="col-md-6 col-sm-6 col-xs-6 sb1-1">
            <a href="#" class="btn-close-menu"><i class="fa fa-times" aria-hidden="true"></i></a>
            <a href="#" class="atab-menu"><i class="fa fa-bars tab-menu" aria-hidden="true"></i></a>
            <a href="#" class="logo"><img src="/multiplechoice/assets/user/images/logo.png" alt="" />
            </a>
        </div>
        <!--== SEARCH ==-->

        <!--== NOTIFICATION ==-->

        <!--== MY ACCCOUNT ==-->
        <div class="col-md-6 col-sm-6 col-xs-6">
            <!-- Dropdown Trigger -->
            <a class='waves-effect dropdown-button top-user-pro'  data-activates='top-menu'><img src="/multiplechoice/assets/user/images/user/6.png" alt="" />My Account <i class="fa fa-angle-down" aria-hidden="true"></i>
            </a>

            <!-- Dropdown Structure -->
            <ul id='top-menu' class='dropdown-content top-menu-sty'>
            <li>
               <a href="usercon/user" class="waves-effect"><i class="fa fa-cogs" aria-hidden="true"></i>My Profile</a> 
           </li>    
                <li class="divider"></li>
                <li><a href="Usercon/logout" class="ho-dr-con-last waves-effect"><i class="fa fa-sign-in" aria-hidden="true"></i> Logout</a>
                </li>
            </ul>
            <a href="Usercon/userattempt_test" class="btn btn-primary pull-right btn-attempt">Attempt Test</a>
        </div>
    </div>
</div>