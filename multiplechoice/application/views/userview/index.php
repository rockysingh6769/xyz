<!DOCTYPE html>
<html lang="en">



<head>
    <title>Dashboard</title>
    <!--== META TAGS ==-->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!--== FAV ICON ==-->
    <link rel="shortcut icon" href="/multiplechoice/assets/user/images/fav.ico">

    <!-- FONT-AWESOME ICON CSS -->
    <link rel="stylesheet" href="/multiplechoice/assets/user/css/font-awesome.min.css">

    <!--== ALL CSS FILES ==-->
    <link rel="stylesheet" href="/multiplechoice/assets/user/css/style.css">
    <link rel="stylesheet" href="/multiplechoice/assets/user/css/mob.css">
    <link rel="stylesheet" href="/multiplechoice/assets/user/css/bootstrap.css">
    <link rel="stylesheet" href="/multiplechoice/assets/user/css/materialize.css" />
		<link rel="stylesheet" href="/multiplechoice/assets/user/css/dataTables.bootstrap.min.css"/>
		  <link href="/multiplechoice/assets/user/css/bootstrap-glyphicons.css" rel="stylesheet"> 
    <link href="https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900&amp;subset=latin-ext" rel="stylesheet">

</head>

<body>
    <!--== MAIN CONTRAINER ==-->
    <!-- header -->
            <?php include 'header.php'; ?>
        <!-- // header -->

<!--== BODY CONTNAINER ==-->
<div class="container-fluid sb2">
    <div class="row">       

        <!-- sidemenu -->
        <?php include 'sidenav.php'; ?>
        <!-- // sidemenu -->

        <!--== BODY INNER CONTAINER ==-->
        <div class="sb2-2">
            <!--== breadcrumbs ==-->
            <div class="sb2-2-2">
                <ul>
                    <li><a href="<?php echo  base_url().'Usercon/userindex';?>"><i class="fa fa-home" aria-hidden="true"></i> Home</a>
                    </li>
                    <li class="active-bre"><a href="#"> Dashboard</a>
                    </li>

                </ul>
            </div>
            <!--== DASHBOARD INFO ==-->
            <div class="ad-v2-hom-info">
                <div class="ad-v2-hom-info-inn">
                    <ul>
                        <li>
                            <div class="ad-hom-box ad-hom-box-1">
                                <span class="ad-hom-col-com ad-hom-col-1"><i class="fa fa-question-circle-o"></i></span>
                                <div class="ad-hom-view-com">
                                    <p><i class="fa  fa-arrow-up up"></i> Total Question</p>
                                      <? if(!empty($list['total_question'])) 
                                    {
                                     echo "<h3>".$list['total_question'];" </h3>";
                                    }else
                                    {echo "<h3>0</h3>"; } 
                                    ?> 
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="ad-hom-box ad-hom-box-2">
                                <span class="ad-hom-col-com ad-hom-col-2"><i class="fa fa-file-text-o"></i></span>
                                <div class="ad-hom-view-com">
                                    <p><i class="fa  fa-arrow-up up"></i> Total Test</p>
                                      <? if(!empty($list['total_test'])) 
                                    {
                                     echo "<h3>".$list['total_test'];" </h3>";
                                    }else
                                    {echo "<h3>0</h3>"; } 
                                    ?> 
                                      
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="ad-hom-box ad-hom-box-3">
                                <span class="ad-hom-col-com ad-hom-col-3"><i class="fa fa-check-square-o" aria-hidden="true"></i></span>
                                <div class="ad-hom-view-com">
                                    <p><i class="fa  fa-arrow-up up"></i>Total Score</p>
                                    <? if(!empty($list['out_of_score'])) 
                                    {
                                     echo "<h3> ".$list['out_of_score']."/".$list['total_score'];" </h3>";
                                    }else
                                    {echo "<h3>0/0</h3>"; } 
                                    ?>
                                </div>
                            </div>
                        </li>

                    </ul>
                </div>
            </div>
            <!--== User Details ==-->
            <div class="sb2-2-3">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box-inn-sp">
                            <div class="inn-title">
                                <h4>Recent Test</h4>
                       
                         </div>
                            <div class="tab-inn table-responsive ">
                                <div class="table-desi">
                                    <table class="table table-hover" id="discount1">
                                        <thead>
                                            <tr>
                                                <th>Sno.</th>
                                                <th>Course</th>
                                                <th>Chapter</th>
                                                <th>Subject</th>
                                                <th>Score</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                              <? $i=1;?>
                                              <?php foreach ($list['recent_list'] as  $value):?>
                                                <td><? echo $i++;?></td>
                                                <td><?php  echo $value['course_name']; ?></td>
                                                <td><?php  echo $value['chapter_name']; ?></td>
                                                <td><?php  echo $value['subject_name']; ?></td>
                                                <td><?php  echo $value['score']; ?></td>
                                            </tr>
                                      <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
</div>
</div>
 <?php include 'footer.php'; ?>
</div>
</div>
    <!--== BOTTOM FLOAT ICON ==-->
    <!--======== SCRIPT FILES =========-->
<script> 
 $(document).ready(function() {
    $('#discount1').DataTable();
} );
</script>
</body>



</html>