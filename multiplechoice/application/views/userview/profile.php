<!DOCTYPE html>
<html lang="en">

<head>
    <title>My Profile</title>
    <!--== META TAGS ==-->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!--== FAV ICON ==-->
    <base href="<?php echo base_url();?>">
    <link rel="shortcut icon" href="assets/user/images/fav.ico">

    <!-- GOOGLE FONTS -->
  
    <!-- FONT-AWESOME ICON CSS -->
  
    <link rel="stylesheet" href="assets/user/css/font-awesome.min.css">
    <!--== ALL CSS FILES ==-->
    <link rel="stylesheet" href="assets/user/css/style.css">
    <link rel="stylesheet" href="assets/user/css/mob.css">
    <link rel="stylesheet" href="assets/user/css/bootstrap.css">
    <link rel="stylesheet" href="assets/user/css/materialize.css" />
    <link rel="stylesheet" href="assets/user/css/dataTables.bootstrap.min.css"/>
    <link href="assets/user/css/bootstrap-glyphicons.css" rel="stylesheet"> 
    <link href="https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900&amp;subset=latin-ext" rel="stylesheet">

</head>
<style type="text/css">
.error{color: red !important;}
.alert-warning,.alert-success
{ 
    display: none;
    position: fixed;
    max-width: 50%; 
    width: 100%; 
    margin: auto;   
    top: 45%;   left: 16%;    
    z-index: 111111;     
    box-shadow: 1px 1px 8px rgba(0, 0, 0, 0.15);     
    text-align: center;     
    right: 0;    
} 
</style>
<body>
<div class="alert alert-warning">
<strong><center class="one"></center></strong>  
</div>
<div class="alert alert-success">
<strong><center class="two"></center></strong> 
</div>
    <!-- header -->
    <?php include 'header.php'; ?>  
    <!-- // header -->

    <!--== BODY CONTNAINER ==-->
    <div class="container-fluid sb2">
        <div class="row">

            <!-- sidemenu -->
            <!--<?php //include 'sidenav.php'; ?>not remove -->
            <!-- // sidemenu -->

            <div class="sb2-2">
                <div class="sb2-2-2">
                    <ul>
                        <li><a href="<?php echo base_url().'index.php/Usercon/userindex';?>"><i class="fa fa-home" aria-hidden="true"></i> Home</a>
                        </li>
                        <li class="active-bre"><a href="#"> Question</a>
                        </li>
                        
                    </ul>
                </div>		
                <main class="pt-0 pb-0">
                    <div class="main-section trip-details">
                       <section class="companies-info tabs-friend userdetail-topspace">
                            <div class="container-fluid p-0">
                                <div class="row">
                                  <div class="col-lg-3">
                                      <div class="userdetailleftimg">
                                          <div class="icn-edit">
                                           <?php $path= base_url();?>
                                           <?php if(!empty($data[0][0]->imgpath)) echo "<img src=".$path.'assets/user/images/upload/'.$data[0][0]->imgpath." class='circle' width='100' height='100'  >";   
                                               else 
                                               echo "<img src=".'assets/user/images/user1.jpg'." >" ;  ?>
                                           <label class="icn-profl" for="profl">
                                            <i class="material-icons dp48">edit</i>
                                            <input type="file" name="imgpath" id="profl">
                                        </label>
                                    </div>
                                    <h4><?php echo $data[0][0]->firstname.' '.$data[0][0]->lastname;?></h4>
                                    <p><?php  echo $data[0][0]->email;?></p>
                                    <p><?php echo $data[0][0]->mobile_no;?></p>
                                    <!--<h4>John Smith</h4>
                                    <p>admin@gmail.com</p>
                                    <p>+919811100000</p>-->
                                </div>
                            </div>
                            <div class="col-lg-9">
                                     
                                <ul class="nav nav-tabs " id="myTab" role="tablist">
                                    <li class="nav-item active">
                                        <a class="nav-link" id="home-tab" data-toggle="tab" href="#my-friend" role="tab" aria-controls="my-friend" aria-selected="true" aria-expanded="true"><i class="fa fa-file-text"></i>Personal Details</a>
                                    </li>
                                </ul>
                                <div class="tab-content" id="myTabContent">
                                    <div class="tab-pane fade active in" id="my-friend" role="tabpanel" aria-labelledby="home-tab" aria-expanded="false">
                                        <div class="companies-list userdetail-topspacetab">

                                            <div class="row">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-12">

                                                    <div class="main-ws-sec profiledetail">

                                                      <form method="post" id="myform" action="<?php echo base_url('usercon/userregister')?>">

                                                          <div class="form-group col-lg-6">
                                                            <label for="fstname">First Name</label>
                                                            <input name="firstname" class="form-control" id="fstname" value="<?php  echo $data[0][0]->firstname;?>" type="text">
                                                        </div>

                                                        <div class="form-group col-lg-6">
                                                            <label for="lastname">Last Name</label>
                                                            <input  name="lastname" class="form-control" id="lstname" value="<?php  echo $data[0][0]->lastname;?>" type="text">
                                                        </div>	

                                                        <div class="form-group col-lg-6">
                                                            <label for="phone">Phone No</label>
                                                            <input  name="mobile_no" class="form-control" id="phone" value="<?php  echo $data[0][0]->mobile_no;?>" type="text">
                                                        </div>	

                                                        <div class="form-group col-lg-6">
                                                            <label for="email">Email Address</label>
                                                            <input  name="email" class="form-control" id="email" value="<?php  echo $data[0][0]->email;?>" type="email">
                                                        </div>		
                                                              <div class="form-group col-lg-6">
                                                                <label>Date Of Birth</label>
                                                                <input type="date" class="form-control form-control-lg" id="date" name="dob" value="<?php  echo $data[0][0]->dob;?>" >
                                                               </div>
                                                                <div class="form-group col-lg-6">
                                                            <label>Select/Status</label>
                                                             <?php  if($data[0][0]->status=='1')
                                                           echo "<select class='form-control'  
                                                                 name='status'>
                                                                 <option value='1'>Active</option>
                                                                 <option value='0'>Inactive</option>
                                                               </select>";
                                                            else   
                                                            echo "<select class='form-control'  
                                                                   name='status'>
                                                                  <option value='0'>Inactive</option>
                                                                  <option value='1'>Active</option>
                                                                  </select>";
                                                         ?>
                                                        </div>
                                                       
                                                        <div class="form-group col-lg-12">
                                                        <label>Select Country</label>
                                                       <select id="select_data"  name="country_id"   class="form-control"  > 
                                                        <? foreach ($data[1] as $value1) {
                                                     if($value1['id'] == $data[0][0]->country_id) {   $key1=$value1['id'];
                                                      $val1=$value1['country_name']; }
                                                      } ?>
                                                   <option value="<? echo $key1; ?>"><? echo $val1; ?></option>
                                                       <?php foreach($data[1] as $key => $value){ ?>
                                                       <option  value="<?php echo $value['id']?>">
                                                        <?php echo $value['country_name']; ?></option><?php } ?>
                                                    </select>
                                                        </div>

                                                        <div class="form-group col-lg-6">
                                                            <label for="newpassword">New Password</label>
                                                            <input name="password" class="form-control" id="password" type="password">
                                                        </div>

                                                        <div class="form-group col-lg-6">
                                                            <label for="conpassword">Confirm Password</label>
                                                            <input  name="cpassword" class="form-control" id="cpassword" type="password">
                                                        </div>
                                                        <div class="form-group col-lg-12 mt-3">
                                                            <input class="btn btn-primary" type="submit" value="Edit">
                                                        </div>	
                                                    </form>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="process-comm">
                                            <div class="spinner">
                                                <div class="bounce1"></div>
                                                <div class="bounce2"></div>
                                                <div class="bounce3"></div>
                                            </div>
                                        </div>
                                        <!--process-comm end-->
                                    </div>

                                </div>
                                <div class="tab-pane fade" id="friend-request" role="tabpanel" aria-labelledby="home-tab" aria-expanded="false">
                                    <div class="companies-list userdetail-topspacetab">

                                        <div class="row">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-12">

                                                <div class="main-ws-sec">
                                            </div>
                                      <div class="process-comm">
                                                <div class="spinner">
                                                    <div class="bounce1"></div>
                                                    <div class="bounce2"></div>
                                                    <div class="bounce3"></div>
                                                </div>
                                            </div>
                                            <!--process-comm end-->
                                        </div>
                                    </div>
                                </div>
                            </div>




                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--companies-info end-->

    </div>
</main>
 <?php include 'footer.php'; ?>
</div>
</div>
<!--== BOTTOM FLOAT ICON ==-->


<!--======== SCRIPT FILES =========-->
<script> 
 $(document).ready(function() {
    $('#example').DataTable();
} );

$("#password").keyup(function()
{
    var vl=$(this).val().length;
    if(vl > 0)
    $("#cpassword").attr("required", true); 
    else 
    $("#cpassword").attr("required", false);
});

$("#cpassword").keyup(function()
{ 
    var vl=$(this).val().length;
    if(vl > 0)
    $("#password").attr("required", "true");
    else
    $("#password").attr("required", false);
}); 

$('#myform').validate(
{
    rules:
    {
        firstname:{   required:true  },
        lastname: {   required:true  },
        mobile_no:{   required:true  },
        email:    {   required:true, email:true},
        address:  {   required:true  },
        password: {   maxlength:10   },
        dob:      {   required:true  },
        cpassword:{   equalTo: "#password"}
        
    },
    submitHandler:function(form) 
    {   
       var data = $("#myform").serialize();
       var url='<?php echo base_url();?>index.php/usercon/userupdate';
       $.ajax(
       {    url: url,
            data: data,
            type:'post',
            success:function(done)
            {
                console.log(done);
                if(done.trim()=='already')
                {  $('.one').text('This email is already exist');
                   $('.alert-warning').show();
                   setTimeout(function(){
                   location.reload();  },1000); 
                }
                if(done.trim()=='success')
                {  $('.two').text('successfully updated');
                   $('.alert-success').show();
                   setTimeout(function(){
                   location.reload();  },1000); 
                }
            }
       });
    }

});


$('#profl').on('change', function()
{
  var url='<?php echo base_url();?>index.php/usercon/fileupload';
  var location='<?php base_url();?>assets/user/images/upload/';
  var fd = new FormData(this);
  fd.append('files',$('#profl')[0].files[0]);
    $.ajax(
    {
        method:"POST",
        url:url,    
        data:fd,
        cache: false,
        contentType: false,
        processData: false,   
        success:function(data)
        { 
           console.log(data); 
           if(data.trim()=='not')
            {  $('.one').text('your image size so big');
               $('.alert-warning').show();
               setTimeout(function(){
               window.location.reload()  },1000); 
            }
            if(data.trim()=='success')
            {  $('.two').text('your image is uploaded');
               $('.alert-success').show();
               window.location.reload()
            }
        }
                 //$("#imgsrc").attr('src', location+data);  
                 //var aba=$('#imgsrc').append('<img src="' + location+data + '" />');
    });
});
</script>
</body>
</html>