<!DOCTYPE html>
<html lang="en">
<head>
	<title>Question</title>
	<!--== META TAGS ==-->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<!--== FAV ICON ==-->
	<base href="<?php echo base_url();?>">
	<link rel="shortcut icon" href="assets/user/images/fav.ico">
    <!-- FONT-AWESOME ICON CSS -->
	<link rel="stylesheet" href="assets/user/css/font-awesome.min.css">

	<!--== ALL CSS FILES ==-->
	<link rel="stylesheet" href="assets/user/css/style.css">
	<link rel="stylesheet" href="assets/user/css/mob.css">
	<link rel="stylesheet" href="assets/user/css/bootstrap.css">
	<link rel="stylesheet" href="assets/user/css/materialize.css" />
	<link rel="stylesheet" href="assets/user/css/dataTables.bootstrap.min.css"/>
	<link href="assets/user/css/bootstrap-glyphicons.css" rel="stylesheet"> 
	<link href="https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900&amp;subset=latin-ext" rel="stylesheet">
<style type="text/css">
#addstate-error   {  color:red   !important;  }
#addcountry-error {  color:red   !important;  }
#addcourse-error  {  color:red   !important;  }
#addgrade-error   {  color:red   !important;  }
#addyear-error    {  color:red   !important;  }
#addsubject-error {  color:red   !important;  }
#addchapter-error {  color:red   !important;  }
#addcountry       {color:#545151 !important;  }
#addstate         {color:#545151 !important;  }
#addcourse        {color:#545151 !important;  }
#addgrade         {color:#545151 !important;  }
#addyear          {color:#545151 !important;  }
#addsubject       {color:#545151 !important;  }
#addchapter       {color:#545151 !important;  } 
.error            { color:red    !important;  }
</style>
</head>
<body class="questionpagebgcolor">      
	<!-- header -->
	<?php include 'header.php'; ?>
	<!-- // header -->
	<!--== BODY CONTNAINER ==-->
	<div class="container-fluid sb2">
		<div class="row">


			<!-- sidemenu -->
			<?php include 'sidenav.php'; ?>
			<!-- // sidemenu -->
			<!--== BODY INNER CONTAINER ==-->
			<div class="sb2-2">
				<!--== breadcrumbs ==-->
				<div class="sb2-2-2">
					<ul>
						<li><a href=""<?php echo base_url().'Usercon/userindex';?>"><i class="fa fa-home" aria-hidden="true"></i> Home</a>
						</li>
						<li class="active-bre"><a href="#">ADD Question</a>
						</li>

					</ul>
				</div>
				<!--== DASHBOARD INFO ==-->
                <!--== User Details ==-->
				<div class="sb2-2-3">
					<div class="box-inn-sp questionsection">
						<div class="row">
							<div class="tab-inn ">
								<p class="fontsize-p">Choose Your Priorities</p>
						<form id="prioritieform" method="post">
									<div class="col-md-4">
										<div class="form-group lgbtn">
											<select id="addcountry" required name="addcountry" class="form-control"  >  <option  value="">Select/Country</option>
	                                          <?php foreach($list[0] as $key => $value){ ?>
	                                                <option  value="<?php print_r($value['id'])?>">
	                                                <?php echo $value['country_name']; ?></option> 
	                                                <?php } ?>
	                                        </select>
										</div>
									</div>
									<div class="col-md-4">
                                         <div class="form-group lgbtn">
                                        	 <select required class="form-control" name="addstate" id="addstate">
                                                <option value="">State/Province</option>
                                            </select>
										</div>
                                    </div>
									<div class="col-md-4">
                                        <div class="form-group lgbtn">

											<select required id="addcourse" name="addcourse"  class="form-control"  > 
                                               <option value="">Select/Course </option>
                                               <?php foreach($list[1] as $key => $value){ ?>
                                                <option  value="<?php print_r($value['id'])?>">
                                                <?php echo $value['course_name']; ?></option> 
                                                <?php } ?>
                                            </select>
										</div>
                                    </div>
									<div class="col-md-4">
                                       <div class="form-group lgbtn">
                                            <select required id="addgrade" name="addgrade"  class="form-control"  > 
                                                 <option value="">Grade/Level</option>
                                                 <?php foreach($list[2] as $key => $value){ ?>
                                                 <option  value="<?php print_r($value['id'])?>">
                                                 <?php echo $value['grade_name']; ?></option> 
                                                <?php } ?>
                                            </select>
										</div>
                                    </div>
									<div class="col-md-4">
                                      <div class="form-group lgbtn">
     		                               <select required id="addyear" name="addyear"  class="form-control">      
     		                                   <option value="">Year/List</option>
                                                 <?php foreach($list[3] as $key => $value){ ?>
                                                 <option  value="<?php print_r($value['id'])?>">
                                                 <?php echo $value['year']; ?></option> 
                                                <?php } ?>
                                            </select>
									    </div>
                                    </div>
									<div class="col-md-4">
                                        <div class="form-group lgbtn">
       	                                    <select required class="form-control" name="addsubject" id="addsubject" >
                                                <option value="">Subject/List</option>
                                            </select>
										</div>

                        
									</div>
									<div class="col-md-4">
										<div class="form-group lgbtn">
											<select required class="form-control" name="addchapter" id="addchapter">
                                                <option value="">Chapter/List</option>
                                            </select>
										</div>
									</div>
								<div class="form-group col-sm-12 mt-1 mb-4">
									 <input type="submit" id="processbtn" class="btn btn-primary" value="Proceed" name="processbtn">
						             <!--<a href="javascript:void(0);"  id="processbtn" class="btn btn-primary" >Proceed</a> -->
								</div>	
                        </form>				
                                 <div class="clearfix"></div>
								    <div class="col-md-12">
										<div id="demo" class="collapse">
							<form id="addquestion" class="addquestion" method="post">
												<div class="row addquestions1">
													<h3>Add Question</h3>
													<div class="form-group col-md-12">
														<input required class="form-control" name="addquest[1]" id="addquest" type="text" value="" placeholder="Add Question">
													</div>
												
													<div class="form-group col-lg-6">
														<label for="Optiona">Option A</label>
														<input required class="form-control" name="option_a[1]" id="Optiona" type="text">
													</div>
													<div class="form-group col-lg-6">
														<label for="Optionb">Option B</label>
														<input required class="form-control" name="option_b[1]" id="Optionb" type="text">
													</div>
													<div class="form-group col-lg-6">
														<label for="Optionc">Option C</label>
														<input required class="form-control" name="option_c[1]" id="Optionc" type="text">
													</div>
													<div class="form-group col-lg-6">
														<label for="Optiond">Option D</label>
														<input required class="form-control" name="option_d[1]" id="Optiond" type="text">
													</div>
				                                    <div class="clearfix"></div>
													<h3 class="mt-5">Correct Answer</h3>
													<div class="form-group col-lg-12">
														<select class="test" name="materialExampleRadios[1]" >
															<optgroup>
																<option value="option_a">Option A</option>
																<option value="option_b" >Option B</option>
																<option value="option_c">Option C</option>
																<option value="option_d">Option D</option>
															</optgroup>

														</select>
													</div>								
										</div>
                                         <div class="form-group col-lg-12 mb-5 mt-3">
													
													  <button type="button" class="btn btn-info pull-left checkempty" id="addmore" data-toggle="collapse" data-target="#demo1">Add More</button> 
													<!--	<button class="btn btn-primary pull-right question-btn" id="addbtn" type="button">Submit</button>
														<button class="btn btn-default pull-right question-btn" type="button">Cancel</button> -->
														<input type="button" class="btn btn-primary pull-right checkvalid question-btn"  name="submit" value="submit" >
										</div>	
                                 <input type="hidden" id="hidden" name="prioritie_id" value="">
								<input type="hidden" name="validateform" id="validateform" value="1">
							</form>    					
							</div>
						</div>
					</div>
				</div>
                 <?php include 'footer.php'; ?>
			</div>
		</div>
<script type="text/javascript">
$('.checkempty').click(function(){
 if($('.addquestion').valid())
 {
    var n = $("#demo div.row").length;
     n++; $('#validateform').val(n);                              
     $(".addquestions1").append('<div class="row addquestions'+n+'"><h3>Add Question</h3><div class="form-group col-md-12"><input class="form-control" required name="addquest['+n+']"  type="text"  placeholder="Add Question"></div><div class="form-group col-lg-6"><label for="Optiona">Option A</label><input class="form-control" name="option_a['+n+']" required  type="text"></div><div class="form-group col-lg-6"><label for="Optionb">Option B</label><input class="form-control" name="option_b['+n+']" required type="text"></div><div class="form-group col-lg-6"><label for="Optionc">Option C</label><input class="form-control" name="option_c['+n+']" required  type="text"></div><div class="form-group col-lg-6"><label for="Optiond">Option D</label><input class="form-control" name="option_d['+n+']" required  type="text"></div><div class="form-group col-lg-6"><input type="button"  class="delbtn" onclick="getfunction('+n+');" value="Delete"></div><div class="clearfix"></div><h3 class="mt-5">Correct Answer</h3><div class="form-group col-lg-12"><select class="test" name="materialExampleRadios['+n+']" ><optgroup><option value="option_a">Option A</option><option value="option_b" >Option B</option><option value="option_c">Option C</option><option value="option_d">Option D</option></optgroup></select></div></div>');$('.test').fSelect();
 }
});
/* $('.delbtn').click(function(){
$('.addquestions'+n+'').remove();
}); */
function getfunction(num)
{ var a =$(".addquestions"+ num).remove(); }	


//This is for validation Priorities form //
$('#addyear').click(function(){
  if( $("#addyear option:selected").val() =='')
  alert("Please select atlease one option");
});

$('#addgrade').click(function(){
  if( $("#addgrade option:selected").val() =='')
  alert("Please select atlease one option");
});
// select country with state //
$('#addcountry').click(function()
{ 
    if( $("#addcountry option:selected").val() ==''){
      alert("Please select atlease one option");
    } 
    else 
    {     var data = $('#addcountry').val();
          var url='<?php echo base_url();?>usercon/questions'; 
          $.ajax(
          {  
                url: url,
                data:{country_id:data},
                type:'post',
                success:function(response)
                {     
                    console.log(response);
                    $('#addstate').empty();
                    $('#addstate').append('<option value="" disabled selected>Select/state</option>');
                    if(response.length > 0)
                    {  
                       var selOpts = "";
                        for (i=0;i<response.length;i++)
                        {   var id =response[i]['id'];
                            var val = response[i]['state_name'];
                            selOpts += "<option value="+id+">"+val+"</option>";
                        }
                        $('#addstate').append(selOpts);

                    }
                    else
                    {
                     $('#addstate').empty();
                    } 
                    
                }
               
           });
    }     
});
// select cource automatic subject and chapter //
$('#addcourse').click(function()
{
    if($("#addcourse option:selected").val() ==''){
      alert("Please select atlease one option");
      $('#addsubject').empty();
      $('#addchapter').empty();
    }  
    else    
    {     var data = $('#addcourse').val();
          var url='<?php echo base_url();?>usercon/questions'; 
          $.ajax(
          {  
                url: url,
                data:{course_id:data},
                type:'post',
                success:function(response)
                {      
                    console.log(response);
                    var result1=response[0];
                    var result2=response[1];
                    

                    $('#addsubject').empty();
                    $('#addsubject').append('<option value="" disabled selected>Select/subject</option>');
                    if(result1.length > 0  )
                    { 
                        var selOpts = "";
                        for (i=0;i<result1.length;i++)
                        {   var id=result1[i]['id'];
                            var val = result1[i]['subject_name'];
                            selOpts += "<option value="+id+">"+val+"</option>";
                        }
                        $('#addsubject').append(selOpts);
                     }
                     else
                     {
                       $('#addsubject').empty();
                     }   
                    $('#addchapter').empty();
                    $('#addchapter').append('<option value="" disabled selected>Select/chapter</option>');
                     if(result2.length > 0 )
                     { 
                        var selOpts = "";
                        for (i=0;i<result2.length;i++)
                        {   var id=result2[i]['id'];
                            var val = result2[i]['chapter_name'];
                            selOpts += "<option value="+id+">"+val+"</option>";
                            
                        }
                        $('#addchapter').append(selOpts);
                     }
                     else
                     {
                     $('#addchapter').empty();
                     }
                }     
           });
    }

});
$('#prioritieform').validate(
{
     rules:
     {   addcountry:{ required: true },
         addcourse: { required: true },
         addgrade:  { required: true },
         addyear:   { required: true },
         addchapter:{ required: true },
         addstate:  { required: true },
         addsubject:{ required: true }
      },
    submitHandler: function(form) 
    {
        var country_id  =  $('#addcountry').val();
		var course_id   =  $('#addcourse') .val();
		var grade_id    =  $('#addgrade')  .val();
		var year_id     =  $('#addyear')   .val();
		var chapter_id  =  $('#addchapter  option:selected').val(); 
		var subject_id  =  $('#addsubject  option:selected').val();
		var state_id    =  $('#addstate    option:selected').val();
	   
		
	   var url='<?php echo base_url();?>usercon/adminadduser_priorities'; 
	   $.ajax(
	   {
			  url: url,
			  data:{country_id:country_id,course_id:course_id,grade_id:grade_id,
					year_id:year_id,chapter_id:chapter_id,subject_id:subject_id,
					state_id:state_id },
			  type:'post',
			  success:function(success)
			  { 
				 console.log(success);
				 if(success.trim()=='please')
				 { $('.three').text('please fill All prioritie infomation ');
				   $('.alert-danger').show();
				   setTimeout(function(){
				   $('.alert-danger').hide(); }, 2000); 
				 }
				 if(success.trim()=='already')
				 {  $('.one').text('this Country, Year, Course and Grade Already Exist ');
					$('.alert-warning').show();
					setTimeout(function(){
					$('.alert-warning').hide();  }, 3000); 
				 }
				 if(success.trim()=='success')
				 {  $('.two').text('successfully add ');
					$('.alert-success').show();
					setTimeout(function(){
					$('.alert-success').hide();  }, 2000); 
				 }
				 else{ $('#hidden').val(success); 
				       $('#prioritieform').hide();
					   $('#demo').show();
					 }
               }  
        });
    }  
});
//** submit Priorities form **// 
//** submit question form **//
$('.checkvalid').click(function()
{
    if($('.addquestion').valid())
    {
        var data = $("#addquestion").serialize();
	    var url='<?php echo base_url();?>usercon/adminadduser_priorities'; 
	    $.ajax(
	    {
		    url: url,
		    data: data,
		    type:'post',
		    success:function(done)
		    { 
				console.log(done);
				if(done.trim()=='already')
				{  $('.one').text('this question and answer already exist ');
				   $('.alert-warning').show();
				   setTimeout(function(){
				   $('.alert-warning').hide();  }, 2000); 
				}
				if(done.trim()=='please')
				{  $('.one').text('please fill your question and All options ');
				   $('.alert-warning').show();
				   setTimeout(function(){
				   $('.alert-warning').hide();  }, 2000); 
				}
				if(done.trim()=='success')
				{ $('.two').text('successfully Add ');
				  $('.alert-success').show();
				  setTimeout(function(){
				  window.location= "<?php  echo base_url('usercon/questions'); ?>";  },  1000);
				}
				if(done.trim()=='notselected')
				{ $('.three').text('please select correct answer ');
				  $('.alert-danger').show();
				  setTimeout(function(){
				  $('.alert-danger').hide(); }, 2000); 
				}
				if(done.trim()=='emptyprioritie')
				{ $('.three').text('please fill your prioritie infomation ');
				  $('.alert-danger').show();
				  setTimeout(function(){
				  $('.alert-danger').hide(); }, 2000); 
				}
		    }  
        });
    }
});
</script>	
		<!-- </div> -->
		<!-- </div> -->

		<!-- </div> -->
		<!-- </div> -->

		<!--== BOTTOM FLOAT ICON ==-->


		<!--======== SCRIPT FILES =========-->
		<script>
			(function($) {
				$(function() {
					$('.test').fSelect();
				});
			})(jQuery);
		</script>
	</body>
    </html>