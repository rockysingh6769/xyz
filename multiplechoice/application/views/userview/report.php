<!DOCTYPE html>
<html lang="en">
<head>
    <title>Manage Transaction</title>
    <!--== META TAGS ==-->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!--== FAV ICON ==-->
    <base href="<? echo base_url(); ?>">
    <link rel="shortcut icon" href="assets/user/images/fav.ico">
    <!-- FONT-AWESOME ICON CSS -->
    <link rel="stylesheet" href="assets/user/css/font-awesome.min.css">
    <!--== ALL CSS FILES ==-->
    <link rel="stylesheet" href="assets/user/css/style.css">
    <link rel="stylesheet" href="assets/user/css/mob.css">
    <link rel="stylesheet" href="assets/user/css/bootstrap.css">
    <link rel="stylesheet" href="assets/user/css/materialize.css" />
    <link href="https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900&amp;subset=latin-ext" rel="stylesheet">
</head>
<body>
    <!--== MAIN CONTRAINER ==-->
        <!-- header -->
            <?php include 'header.php'; ?>
        <!-- // header -->
    <!--== BODY CONTNAINER ==-->
    <div class="container-fluid sb2">
        <div class="row">

            <!-- sidemenu -->
            <?php include 'sidenav.php'; ?>
            <!-- // sidemenu -->
            <div class="sb2-2">
                <div class="sb2-2-2">
                    <ul>
                        <li><a href="usercon/userindex"><i class="fa fa-home" aria-hidden="true"></i> Home</a>
                        </li>
                        <li class="active-bre"><a href="#">Reports</a>
                        </li>
                    </ul>
                </div>
                <div class="sb2-2-3">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="box-inn-sp">
                                <div class="inn-title">
                                    <h4>Report</h4>
                                </div>
                                <div class="tab-inn">
                                    <div class="table-responsive table-desi">
                                        <table class="table table-hover reporttable">
                                            <thead>
                                                <tr>
                                                    <th>Sno.</th>
                                                    <th>Question</th>
                                                    <th>Submitted Answer</th>
                                                    <th>Suggested Answer</th>
                                                     <th>View</th>
                                                </tr>
                                            </thead>
                                            <tbody>
											     <? $i=1; ?>
                                                 <?php foreach ($list as  $value):?>
                                                 <tr>
                                                    <td><? echo $i++; ?></td>
                                                    <td><? echo $value['total_question'] ?></td>
													<td><? echo $value['submit_answer']; ?></td>
													
                                                    <? if(!empty($value['suggest_answer']))
                                                    { 
                                                      echo "<td> ".$value['suggest_answer']."</td>";

                                                    }else{
                                                        echo "<td>0</td>";
                                                    } ?>
                                                  
                                                    <td>
                                                    <a href="usercon/userdetail?prioritie_id=<? 
                                                    echo $value['prioritie_id']; ?>"><i class="fa fa-eye" aria-hidden="true"></i></a>
													</td>
                                                 </tr>
                                                 <?php endforeach; ?> 
											</tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
           <?php include 'footer.php'; ?>
        </div>
    </div>
    <!--== BOTTOM FLOAT ICON ==-->
    <!--======== SCRIPT FILES =========-->
</body>
</html>