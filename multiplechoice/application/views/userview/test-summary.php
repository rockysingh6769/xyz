<!DOCTYPE html>
<html lang="en">

<head>
    <title>Test Summary</title>
    <!--== META TAGS ==-->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <base href="<?php echo base_url(); ?>">
    <!--== FAV ICON ==-->
    <link rel="shortcut icon" href="assets/user/images/fav.ico">


    <!-- FONT-AWESOME ICON CSS -->
    <link rel="stylesheet" href="assets/user/css/font-awesome.min.css">

    <!--== ALL CSS FILES ==-->
    <link rel="stylesheet" href="assets/user/css/style.css">
    <link rel="stylesheet" href="assets/user/css/mob.css">
    <link rel="stylesheet" href="assets/user/css/bootstrap.css">
    <link rel="stylesheet" href="assets/user/css/materialize.css" />
    <link href="https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900&amp;subset=latin-ext" rel="stylesheet">

</head>

<body>
<!-- header -->
            <?php include 'header.php'; ?>
        <!-- // header -->

<!--== BODY CONTNAINER ==-->
<div class="container-fluid sb2">
    <div class="row">
        
        <!-- sidemenu -->
        <?php include 'sidenav.php'; ?>
        <!-- // sidemenu -->

        <div class="sb2-2">
            <div class="sb2-2-2">
                <ul>
                    <li><a href="Usercon/userindex"><i class="fa fa-home" aria-hidden="true"></i> Home</a>
                    </li>
                    <li class="active-bre"><a href="#">Test Summary</a>
                    </li>
                </ul>
            </div>
            <div class="sb2-2-3">
                <div class="box-inn-sp">
                    <div class="row">
                        <div class="col-md-12">

                            <div class="tab-inn detail-page">

 
                                <div class="row ans-options">
                                    <div class="col-md-12">
									<h3 class="tableheading">Your Answer</h3>
									<table class="table table-border-none">
									  <thead>
										<tr>
										  <th scope="col">Question No.</th>
										  <th scope="col">Your Answer</th>
										  <th scope="col">Correct Answer</th>
										 
										</tr>
									  </thead>
									  <tbody>
										<tr>
										
										  <td>1</td>
										  <td class="correcttdlight">a. New York</td>
										  <td class="correcttd">a. New York</td>
										</tr>
										<tr>
										
										  <td>2</td>
										  <td class="wrongtd">a. Second Position</td>
										  <td class="correcttd">b. Third position</td>
										</tr>
										<tr>
										
										  <td>3</td>
										  <td class="correcttdlight">a. The hole is 6 cubic yards</td>
										  <td class="correcttd">a. The hole is 6 cubic yards</td>
										</tr>
										<tr>
										
										  <td>4</td>
										  <td class="wrongtd">a. Tallahassee
                                                        </td>
										  <td class="correcttd">d. Tampa bay</td>
										</tr>
										<tr>
										
										  <td>5</td>
										  <td class="correcttdlight">c. Califorinia</td>
										  <td class="correcttd">c. Dubai</td>
										</tr>
										<tr>
										
										  <td>6</td>
										  <td class="wrongtd">b. New York</td>
										  <td class="correcttd">c. Boston</td>
										</tr>
										<tr>
										
										  <td>7</td>
										  <td class="correcttdlight">d. Tallahassee</td>
										  <td class="correcttd">d. Tallahassee</td>
										</tr>
										<tr>
										
										  <td>8</td>
										  <td class="correcttdlight">a. Third Position</td>
										  <td class="correcttd">a. Third Position</td>
										</tr>
										<tr>
										
										  <td>9</td>
										  <td class="wrongtd">d. Califorinia</td>
										  <td class="correcttd">a. Dubai</td>
										</tr>

                                    </tbody>
									  <tfoot>
										<tr>
										  <td></td>
										  <td>Your Score</td>
										  <td>65/100</td>
										</tr>									  
									  </tfoot>									  
									</table>
                                </div>


                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <?php include 'footer.php'; ?>
    </div>
</div>

    <!--== BOTTOM FLOAT ICON ==-->


    <!--======== SCRIPT FILES =========-->
</body>



</html>